<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema Administrativo de Información Médica v1.0.0</title>
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="css/foundation.css">
    <style>.callout { border: none; }</style>
  </head>
  <body>
    <header>
      <div class="title-bar" data-responsive-toggle="menu" data-hide-for="medium">
        <button class="menu-icon" type="button" data-toggle="menu"></button>
        <div class="title-bar-title">Hospital Ángeles</div>
      </div>

      <div class="top-bar" id="menu">
        <div class="top-bar-left show-for-medium">
          <ul class="dropdown menu" data-dropdown-menu>
            <li class="menu-text">Hospital Ángeles</li>
          </ul>
        </div>
        <div class="top-bar-right">
          <ul class="menu">
            <li class="menu-text">Sistema Administrativo de Información Médica</li>
          </ul>
        </div>
      </div>
    </header>
    <main>
      <div class="medium callout">
        <div class="row">
          <div class="large-12 columns">
            <h3 class="text-center">INICIO DE SESIÓN</h3>
            <span id="incorrecto" class="form-error">
              El usuario y/o contraseña que ingresaste, es incorrecto.
            </span>
          </div>
        </div>

        <form id="login">
          <div class="row">
            <div class="large-3 medium-2 columns">
              <label for="username" class="text-right hide-for-small-only">Usuario</label>
              <label for="username" class="show-for-small-only">Usuario</label>
            </div>

            <div class="large-7 medium-8 columns end">
              <input id="username" name="username" type="text" placeholder="Ingresa el nombre de usuario">
              <span id="username-vacio" class="form-error">
                Por favor, ingresa tu nombre de usuario.
              </span>
            </div>
          </div>

          <div class="row">
            <div class="large-3 medium-2 columns">
              <label for="password" class="text-right hide-for-small-only">Contraseña</label>
              <label for="password" class="show-for-small-only">Contraseña</label>
            </div>

            <div class="large-7 medium-8 columns end">
              <input id="password" name="password" type="password"
                placeholder="*******">
              <span id="password-vacio" class="form-error">
                Por favor, ingresa tu contraseña.
              </span>
            </div>
          </div>

          <div class="row">
            <div class="large-8 large-offset-2 medium-8 medium-offset-2 columns end">
              <input id="entrar" type="submit" class="large button float-right" value="ENTRAR">
              <input name="accion" type="hidden" value="log-in">
            </div>
          </div>
        </form>
      </div>
    </main>
    <div id="loading" class="tiny reveal" data-reveal
      data-close-on-click="false" data-close-on-esc="false">
      <p class="lead">Espera un momento porfavor...</p>
    </div>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script>
      $(document).foundation();

      $(document).ready(function() {
        var usernameInput = $('#username');
        var passwordInput = $('#password');
        var cerrarLoading = function() {
          $('#entrar').attr('disabled', false);
          $('#loading').foundation('close');
        };

        // Trigger de eventos iniciales.
        usernameInput.focus();

        /*
        * Declaracion de EVENTOS.
        */
        $('#login').on('submit', function(evt) {
          // Quitamos clases de error.
          $('.is-invalid-input').removeClass('is-invalid-input');
          $('.form-error').removeClass('is-visible');

          // Validamos que no esten vacios los inputs.
          if ( usernameInput.val().length === 0 ) {
            usernameInput.addClass('is-invalid-input').focus();
            $('#username-vacio').addClass('is-visible');
          } else if ( $('#password').val().length === 0 ) {
            passwordInput.addClass('is-invalid-input').focus();
            $('#password-vacio').addClass('is-visible');
          } else {
            // Deshabilitamos el boton de enviar por si las moscas.
            $('#entrar').attr('disabled', true);

            // Todo correcto, mandamos datos.
            var data = $(this).serialize();

            // Abrimos loading.
            $('#loading').foundation('open');

            $.post('php/api.php', data, function(response) {
              if (response.status === 'OK') {
                window.location.href = 'views/';
              } else {
                $('#incorrecto').addClass('is-visible');
                usernameInput.focus();
              }

              cerrarLoading();
            }, 'json')
            .fail(function() {
              alert('Falló la conexión al servidor, por favor vuelve a intentarlo.');
              cerrarLoading();
            });
          }

          return evt.preventDefault();
        });
        /*  */
      });

    </script>
  </body>
</html>
