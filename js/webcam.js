(function() {
  var streaming = false,
      video        = document.querySelector('#video'),
      container    = document.querySelector('[data-thumbs]'),
      startbutton  = document.querySelector('#startbutton'),
      width = '100%',
      height = 0;

  navigator.getMedia = ( navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia ||
                         navigator.msGetUserMedia);

  navigator.getMedia(
    {
      video: true,
      audio: false
    },
    function(stream) {
      if (navigator.mozGetUserMedia) {
        video.mozSrcObject = stream;
      } else {
        var vendorURL = window.URL || window.webkitURL;
        video.src = vendorURL.createObjectURL(stream);
      }
      video.play();
    },
    function(err) {
      console.log("An error occured! " + err);
    }
  );

  video.addEventListener('canplay', function(ev){
    if (!streaming) {
      height = video.videoHeight / (video.videoWidth/width);
      video.setAttribute('width', width);
      video.setAttribute('height', height);
      // canvas.setAttribute('width', width);
      // canvas.setAttribute('height', height);
      streaming = true;
    }
  }, false);

  function takepicture() {
    var block = document.createElement('div');
    var photo = document.createElement('img');
    var canvas = document.createElement('canvas');
    var close = document.createElement('span');
    var data;

    block.className = 'column column-block';
    photo.classList.add('thumbnail');
    canvas.width = 600;
    canvas.height = 600;
    canvas.getContext('2d').drawImage(video, 0, 0, 600, 600);
    close.innerHTML = '&times;';
    close.setAttribute('data-close', 'thumbnail');
    data = canvas.toDataURL('image/jpeg', 0.75);
    photo.setAttribute('src', data);

    block.appendChild(photo);
    block.appendChild(close);
    container.appendChild(block);
  }

  startbutton.addEventListener('click', function(ev){
    document.querySelector('#data-thumbs-vacio').classList.remove('is-visible');
    takepicture();
    ev.preventDefault();
  }, false);
})();