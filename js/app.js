var mostrarMensaje = function(mensaje) {
  $('#mensaje p.lead').html(mensaje);
  $('#mensaje').foundation('open');
  return;
};

var mostrarLoading = function() {
  $('input[type="submit"]').attr('disabled', true);
  $('#loading').foundation('open');
  return;
};

var ocultarLoading = function() {
  $('input[type="submit"]').attr('disabled', false);
  $('#loading').foundation('close');
  return;
};

/*
* MENU - MOSTRAR ACTIVO
*/
var page = window.location.pathname.split('/').pop();
page = page === '' ? 'index.php' : page;

$('a[href="'+page+'"]').parent().addClass('active');
if ($('a[href="'+page+'"]').closest('.menu').parent().length !== 0) {
  $('a[href="'+page+'"]').closest('.menu').parent().addClass('active');
}
/* */

/*
* CHECAR SESSION
*/
var check = setInterval(function() {
  $.post('../php/api.php', {
    accion: 'check'
  }, function(response) {
    if (response.status === 'BYE') {
      mostrarMensaje('Tu sesión ha terminado, porfavor recarga la página.');
      clearInterval(check);
    }
  }, 'json')
  .fail(function() {
    alert('Falló la conexión al servidor, por favor vuelve a intentarlo.');
  });
}, 60000);

/*
* CERRAR SESION
*/
$('#salir').on('click', function() {
  $.post('../php/api.php', {
    accion: 'log-out'
  }, function(response) {
    if (response.status === 'OK') {
      window.location.reload();
    }
  }, 'json')
  .fail(function() {
    alert('Falló la conexión al servidor, por favor vuelve a intentarlo.');
  });
});
/* */

$(document).foundation();