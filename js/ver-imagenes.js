var verImagenes = function(table) {
  var dt = table.DataTable();
  var targetTd = $(table.data('target')).closest('td').get(0);
  var cell =  dt.cell(targetTd);
  var rowIdx = cell.index().row;
  var id = dt.row(rowIdx).data()[1];

  mostrarLoading();
  $.post('../php/api.php', {
    accion: 'obtener-imagenes',
    id: id
  }, function(response) {
    mostrarMensaje(response.msg);
  }, 'json').fail(function() {
    mostrarMensaje('Falló la conexión al servidor,' +
      ' por favor vuelve a intentarlo.');
  });
};