var editarPaciente = function(table) {
  var dt = table.DataTable();
  var targetTd = $(table.data('target')).closest('td').get(0);
  var cell =  dt.cell(targetTd);
  var rowIdx = cell.index().row;
  var id = dt.row(rowIdx).data()[1];
  var values = {
    action: 'editar',
    id: id
  };
  var form = document.createElement('form');
  var input = document.createElement('input');
  form.method = 'POST';
  form.action = 'guardar.php';
  input.name = 'data';
  input.type = 'hidden';
  input.value = JSON.stringify(values);
  form.appendChild(input);
  document.body.appendChild(form);
  form.submit();
};