var verReporte = function(table) {
  mostrarLoading();

  var id;
  if (isNaN(table)) {
    var dt = table.DataTable();
    var targetTd = $(table.data('target')).closest('td').get(0);
    var cell =  dt.cell(targetTd);
    var rowIdx = cell.index().row;
    id = dt.row(rowIdx).data()[1];
  } else {
    id = table;
  }

  var revealImagenes = $('#reveal-imagenes');
  if (revealImagenes.length === 0) {
    var reveal = document.createElement('div');
    reveal.id = 'reveal-imagenes';
    reveal.className = 'medium reveal';
    reveal.dataset.reveal = '';

    reveal.innerHTML = '<p class="lead">Elija las imágenes que desea mostrar' +
    ' en el reporte</p>' +
    '<form>' +
      '<div class="row large-up-3 medium-up-3 small-up-2" data-reveal-thumbs>' +
      '</div>' +
      '<div class="row">' +
        '<div class="large-12 columns">' +
          '<input type="submit" class="large button float-right" value="GENERAR REPORTE">' +
          '<input name="accion" type="hidden" value="obtener-reporte">' +
          '<input name="tipo" type="hidden" value="colposcopio">' +
          '<input name="id" type="hidden">' +
        '</div>' +
      '</div>' +
    '</form>' +
    '<button class="close-button" data-close aria-label="Close reveal" type="button">' +
      '<span aria-hidden="true">&times;</span>' +
    '</button>';

    $('body').append(reveal);
    revealImagenes = $('#reveal-imagenes');
    revealImagenes.find('[data-reveal-thumbs]').on('mouseenter', 'img.thumbnail', function(evt) {
      var isSelected = $(this).hasClass('selected');

      $(this).parent().find('[data-check]').attr('hidden', isSelected);
      $(this).parent().find('[data-error]').attr('hidden', !isSelected);
    });
    revealImagenes.find('[data-reveal-thumbs]').on('mouseleave', 'img.thumbnail', function(evt) {
      var isSelected = $(this).hasClass('selected');
      if (isSelected) {
        $(this).parent().find('[data-check]').attr('hidden', !isSelected);
        $(this).parent().find('[data-error]').attr('hidden', isSelected);
      } else {
        $(this).parent().find('[data-check]').attr('hidden', true);
        $(this).parent().find('[data-error]').attr('hidden', true);
      }
    });
    revealImagenes.find('[data-reveal-thumbs]').on('click', 'img.thumbnail', function(evt) {
      $(this).toggleClass('selected');
      // var isSelected = $(this).hasClass('selected');
      // $(this).parent().find('[data-check]').attr('hidden', isSelected);
      // $(this).parent().find('[data-error]').attr('hidden', !isSelected);

      if (revealImagenes.find('[data-reveal-thumbs]').find('img.selected').length > 4) {
        revealImagenes.find('[data-reveal-thumbs]')
          .find('img.selected')
          .not(this)
          .first()
          .removeClass('selected')
          .parent()
          .find('[data-check]').attr('hidden', true)
          .end()
          .find('[data-error]').attr('hidden', true);
      }
    });
    revealImagenes.find('form').on('submit', function(evt) {
      mostrarLoading();

      var data = $(this).serialize();
      $.each(revealImagenes.find('img.selected'), function(i, img) {
        data += '&imgs[]=' + img.dataset.id;
      });

      $.post('../php/api.php', data, function(response) {
        if (response.status === 'OK') {
          ocultarLoading();
          window.open(response.data, "_blank");
        } else {
          mostrarMensaje(response.msg);
        }
      }, 'json').fail(function() {
        mostrarMensaje('Falló la conexión al servidor,' +
          ' por favor vuelve a intentarlo.');
      });

      return evt.preventDefault();
    }).on('keydown', function(evt) {
      if (evt.keyCode === 13) {
        return evt.preventDefault();
      }
    });
    // revealImagenes.on('closed.zf.reveal', function() {
    //   $(this).find('[name="id"]').val('');
    // });
    revealImagenes.on('open.zf.reveal', function() {
      $(this).find('[type="submit"]').prop('disabled', false);
    });

    revealImagenes.foundation();
  }

  revealImagenes.find('[name="id"]').val(id);
  $.post('../php/api.php', {
    accion: 'obtener-imagenes',
    id: id
  }, function(response) {
    if (response.status === 'OK') {
      var thumbs = revealImagenes.find('[data-reveal-thumbs]');
      thumbs.empty();

      response.data.forEach(function(img, index) {
        thumbs.append('<div class="column column-block">' +
          '<img class="thumbnail" src="'+img.url+'" data-id='+img.id+'>' +
          '<img src="../img/check.png" data-check hidden>' +
          '<img src="../img/error.png" data-error hidden>' +
        '</div>');
      });

      revealImagenes.foundation('open');
    } else {
      mostrarMensaje(response.msg);
    }
  }, 'json').fail(function() {
    mostrarMensaje('Falló la conexión al servidor,' +
      ' por favor vuelve a intentarlo.');
  });
};