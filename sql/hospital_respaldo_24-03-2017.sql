-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-03-2017 a las 21:21:32
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hospital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alergias`
--

CREATE TABLE `alergias` (
  `alergias_id` int(11) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colposcopio`
--

CREATE TABLE `colposcopio` (
  `colposcopio_id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `procedimiento` varchar(100) NOT NULL,
  `instrumentos_id` tinyint(4) NOT NULL,
  `motivo_estudio` varchar(150) NOT NULL,
  `anestesia` tinyint(1) NOT NULL DEFAULT '0',
  `hallazgos` text NOT NULL,
  `impresion_diagnostica` text NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `colposcopio`
--

INSERT INTO `colposcopio` (`colposcopio_id`, `pacientes_id`, `numero`, `fecha`, `procedimiento`, `instrumentos_id`, `motivo_estudio`, `anestesia`, `hallazgos`, `impresion_diagnostica`, `usuarios_id`, `timestamp`) VALUES
(1, 1, NULL, '2017-02-18', 'colposcopia', 1, 'control', 0, 'cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Cervix Sano', 4, '2017-02-18 11:05:34'),
(2, 2, NULL, '2017-02-20', 'colposcopia', 1, 'control', 0, 'Cervix eutrofico, colposcopia adecuada co ZT normal con adecuada captación al lugol', 'Cervix Sano', 4, '2017-02-20 20:06:04'),
(3, 3, NULL, '2017-02-21', 'colposcopia', 1, 'Control', 0, 'Cervix atrofico, colposcopia adecuada con ZT normal se aprecia hipocaptación al lugol.', 'Atrofia Epitelial.Vaginosis Bateriana.', 4, '2017-02-21 16:48:34'),
(4, 4, NULL, '2017-02-21', 'Colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con hipocaptación al lugol periorificiaria.', 'Eversión Glandular Leve.', 4, '2017-02-21 17:25:11'),
(5, 5, NULL, '2017-02-22', 'colposcopia', 1, 'Control', 0, 'Cervix atrofico, colposcopia adecuada con ZT  anormal por presencia de area acetoblanca en el radio de las 5 a 8 con hi´pocaptación al lugol.', 'Atrofia Epitelial. Desc LIEBG.', 4, '2017-02-22 17:08:30'),
(6, 6, NULL, '2017-02-22', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT anormal presencia de area acetoblanca en el radio de las 11 y 7 con hipocaptación al lugol.', 'Desc LIEBG,IVPH.', 4, '2017-02-22 20:12:37'),
(7, 7, NULL, '2017-02-23', 'Colposcopia', 1, 'control', 0, 'Cervix eutrofico colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Vaginosis Bacteriana.', 4, '2017-02-23 17:49:39'),
(8, 8, NULL, '2017-02-23', 'colposcopia', 1, 'control', 0, 'Cervix eutrofico, colposcopica adecuada con ZT normal con adecuada captacion al lugol.', 'Eversion Glandular Leve.', 4, '2017-02-23 18:28:19'),
(9, 9, NULL, '2017-02-23', 'Colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT anormal presencia de area acetoblanca en el radio de las 11 a 9 con hipocaptación al lugol.', 'Desc LIEAG, IVPH,Eversion Glandular.', 4, '2017-02-23 18:57:13'),
(10, 10, NULL, '2017-02-23', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Cervix Sano.', 4, '2017-02-23 19:32:00'),
(11, 11, NULL, '2017-02-25', 'Colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT Normal con adecuada captacion al lugol.', 'Cervix sano.', 4, '2017-02-25 10:27:03'),
(12, 12, NULL, '2017-02-25', 'Colposcopia', 1, 'control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Vaginosis, eversión glandular leve.', 4, '2017-02-25 12:57:40'),
(13, 13, NULL, '2017-02-27', 'Colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Cervix Normal.', 4, '2017-02-27 20:37:08'),
(14, 14, NULL, '2017-02-28', 'Colposcopia', 1, 'control', 0, 'Colposcopia de cupula con hipocaptación al lugol.', 'Atrofia Epitelial.', 4, '2017-02-28 16:55:05'),
(15, 15, NULL, '2017-03-01', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Atrofia Epitelial.', 4, '2017-03-01 16:10:19'),
(16, 16, NULL, '2017-03-01', 'colposcopia', 1, 'control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT anormal presencia de area acetoblanca en el radio de 11 a 1 con hipocaptación al lugol periorificaria.', 'Eversión Glandular, Pb IVPH, LIEBG.', 4, '2017-03-01 18:13:10'),
(17, 17, NULL, '2017-03-02', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal se aprecia hipcaptación al lugol periorificiaria.', 'Eversion Glandular.', 4, '2017-03-02 19:45:05'),
(18, 18, NULL, '2017-03-03', 'Colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Cervix Sano.', 4, '2017-03-03 16:41:12'),
(19, 19, NULL, '2017-03-03', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT anormal por la presencia de area acetoblanca en el radio de las 11 a 1 con hipocaptación al lugol.', 'Eversion Glandular.Desc LIEBG, IVPH.', 4, '2017-03-03 19:08:24'),
(20, 20, NULL, '2017-03-03', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuaa con ZT anormal presencia de area acetoblanca en el radio de las 11 a 1 con hipocaptación al lugol.', 'Desc LIEBG, IVPH.', 4, '2017-03-03 20:20:41'),
(21, 21, NULL, '2017-03-06', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada se aprecia hipocaptación al lugol periorificiaria.', 'Vaginosis Bacteriana,Atrofia Epitelial.', 4, '2017-03-06 18:00:15'),
(22, 22, NULL, '2017-03-06', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Cervix Sano', 4, '2017-03-06 18:27:44'),
(23, 23, NULL, '2017-03-09', 'Colposcopia', 1, 'Control', 0, 'Cervxi eutrofico, colposcopia adecuada con ZT anormal presencia de area acetoblanca en el radio de las 11 y 5 con hipocaptacion al lugol.', 'Desc LIEBG.IVPH, ECTOPIA CERVICAL.', 4, '2017-03-09 16:10:57'),
(24, 24, NULL, '2017-03-09', 'Colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal.', 'Ectopia Cervical.', 4, '2017-03-09 17:08:21'),
(25, 25, NULL, '2017-03-09', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Ectopia Cervical leve.', 4, '2017-03-09 19:10:44'),
(26, 26, NULL, '2017-03-10', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal se aorecia hipocaptación al lugol periorificiaria.', 'Ectopia Cervical.Desc LIEAG, IVPH.', 4, '2017-03-10 17:07:46'),
(27, 27, NULL, '2017-03-10', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT anormal area acetoblanca en el radio de las 1 con hipocaptación al lugol.', 'Desc LIEBG, IVPH', 4, '2017-03-10 17:55:27'),
(28, 28, NULL, '2017-03-10', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Quiste de Naboth Se drena', 4, '2017-03-10 18:59:53'),
(29, 29, NULL, '2017-03-10', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal   se aprecia  hipocaptación al lugol.', 'Atrofia Epitelial.Vaginosis.', 4, '2017-03-10 19:34:22'),
(30, 30, NULL, '2017-03-11', 'Colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT anormal por la presencia de area acetoblanca en el radio de las 10 a 1 con hipocaptacion al lugol.', 'Ectopia Cervical,IVPH,DESC LIEBG.', 4, '2017-03-11 10:29:42'),
(31, 31, NULL, '2017-03-13', '', 1, '', 0, '', '', 4, '2017-03-13 17:24:37'),
(32, 32, NULL, '2017-03-13', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal se aprecia hipocaptación al lugol periorificiaria.', 'Ectopia Cervical.', 4, '2017-03-13 17:29:47'),
(33, 33, NULL, '2017-03-13', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Vaginosis Bacteriana.', 4, '2017-03-13 18:55:25'),
(34, 34, NULL, '2017-03-14', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Vaginosis Bacteriana.', 4, '2017-03-14 18:23:50'),
(35, 35, NULL, '2017-03-15', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Quiste de Naboth se drena.', 4, '2017-03-15 19:29:44'),
(36, 36, NULL, '2017-03-16', 'colposcopia', 1, 'Control', 0, 'Cervix atrofico, hipocaptación al lugol ', 'Atrofia Epitelial Severa.', 4, '2017-03-16 18:07:35'),
(37, 37, NULL, '2017-03-16', 'Colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT  normal con adecuada captación al lugol.', 'Vaginosis Bacteriana.', 4, '2017-03-16 18:49:47'),
(38, 38, NULL, '2017-03-17', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Vaginosis Bacteriana.', 4, '2017-03-17 19:14:17'),
(39, 39, NULL, '2017-03-17', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Cervix Sano.', 4, '2017-03-17 20:19:27'),
(40, 40, NULL, '2017-03-17', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT anormal  por la presencia de area acetoblanca en el radio de las 1 y 5 con hipocaptación al lugol.', 'Desc LIEBG, IVPH.', 4, '2017-03-17 20:51:28'),
(41, 41, NULL, '2017-03-21', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con hipocaptación al lugol.', 'Atrofia Epitelial, Vaginosis Bacteriana.', 4, '2017-03-21 16:58:51'),
(42, 42, NULL, '2017-03-21', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Cervix Normal.', 4, '2017-03-21 17:59:53'),
(43, 43, NULL, '2017-03-23', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT anormal  presencia de area acetoblanca en el radio de las 11 a 12 y 3 a 7 con hipocaptacion al lugol.', 'Desc LIEBG, IVPH.', 4, '2017-03-23 18:12:49'),
(44, 44, NULL, '2017-03-23', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'cervix sano.', 4, '2017-03-23 18:47:57'),
(45, 45, NULL, '2017-03-23', 'colposcopia', 1, 'Control', 0, 'Cervix eutrofico, colposcopia adecuada con ZT normal con adecuada captación al lugol.', 'Cervix sano.', 4, '2017-03-23 19:20:16'),
(46, 46, NULL, '2017-03-23', 'colposcopia', 1, 'Control', 0, 'Cervix atrofico, colposcopia adecuada con ZT normal con hipocaptación al lugol.', 'Atrofia Epitelial.', 4, '2017-03-23 19:46:30'),
(47, 47, NULL, '2017-03-23', 'colposcopia', 1, 'Control', 0, 'Colposcopia de cupula con hipocaptación al lugol.', 'Vaginosis Bacteriana.', 4, '2017-03-23 20:25:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colposcopio_imagenes`
--

CREATE TABLE `colposcopio_imagenes` (
  `colposcopio_imagenes_id` int(11) NOT NULL,
  `colposcopio_id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(150) NOT NULL,
  `formato` varchar(75) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `colposcopio_imagenes`
--

INSERT INTO `colposcopio_imagenes` (`colposcopio_imagenes_id`, `colposcopio_id`, `nombre`, `url`, `formato`, `usuarios_id`, `timestamp`, `borrado`) VALUES
(1, 1, '58a87edeab638.jpeg', 'http://localhost/hospital/photos/00001/58a87edeab638.jpeg', 'image/jpeg', 4, '2017-02-18 11:05:34', 0),
(2, 1, '58a87edeb0071.jpeg', 'http://localhost/hospital/photos/00001/58a87edeb0071.jpeg', 'image/jpeg', 4, '2017-02-18 11:05:34', 0),
(3, 1, '58a87edeb2399.jpeg', 'http://localhost/hospital/photos/00001/58a87edeb2399.jpeg', 'image/jpeg', 4, '2017-02-18 11:05:34', 0),
(4, 2, '58aba08c040e1.jpeg', 'http://localhost/hospital/photos/00002/58aba08c040e1.jpeg', 'image/jpeg', 4, '2017-02-20 20:06:04', 0),
(5, 2, '58aba08c0bde3.jpeg', 'http://localhost/hospital/photos/00002/58aba08c0bde3.jpeg', 'image/jpeg', 4, '2017-02-20 20:06:04', 0),
(6, 3, '58acc3c304d79.jpeg', 'http://localhost/hospital/photos/00003/58acc3c304d79.jpeg', 'image/jpeg', 4, '2017-02-21 16:48:35', 0),
(7, 3, '58acc3c30e5d3.jpeg', 'http://localhost/hospital/photos/00003/58acc3c30e5d3.jpeg', 'image/jpeg', 4, '2017-02-21 16:48:35', 0),
(8, 4, '58accc5759f49.jpeg', 'http://localhost/hospital/photos/00004/58accc5759f49.jpeg', 'image/jpeg', 4, '2017-02-21 17:25:11', 0),
(9, 4, '58accc5770e7e.jpeg', 'http://localhost/hospital/photos/00004/58accc5770e7e.jpeg', 'image/jpeg', 4, '2017-02-21 17:25:11', 0),
(10, 5, '58ae19eea22f1.jpeg', 'http://localhost/hospital/photos/00005/58ae19eea22f1.jpeg', 'image/jpeg', 4, '2017-02-22 17:08:30', 0),
(11, 5, '58ae19eed10fc.jpeg', 'http://localhost/hospital/photos/00005/58ae19eed10fc.jpeg', 'image/jpeg', 4, '2017-02-22 17:08:30', 0),
(12, 5, '58ae19eed3425.jpeg', 'http://localhost/hospital/photos/00005/58ae19eed3425.jpeg', 'image/jpeg', 4, '2017-02-22 17:08:30', 0),
(13, 6, '58ae451527951.jpeg', 'http://localhost/hospital/photos/00006/58ae451527951.jpeg', 'image/jpeg', 4, '2017-02-22 20:12:37', 0),
(14, 6, '58ae45153ec6e.jpeg', 'http://localhost/hospital/photos/00006/58ae45153ec6e.jpeg', 'image/jpeg', 4, '2017-02-22 20:12:37', 0),
(15, 6, '58ae451541f37.jpeg', 'http://localhost/hospital/photos/00006/58ae451541f37.jpeg', 'image/jpeg', 4, '2017-02-22 20:12:37', 0),
(16, 7, '58af7513991fb.jpeg', 'http://localhost/hospital/photos/00007/58af7513991fb.jpeg', 'image/jpeg', 4, '2017-02-23 17:49:39', 0),
(17, 7, '58af7513a1e9d.jpeg', 'http://localhost/hospital/photos/00007/58af7513a1e9d.jpeg', 'image/jpeg', 4, '2017-02-23 17:49:39', 0),
(18, 8, '58af7e23b84d2.jpeg', 'http://localhost/hospital/photos/00008/58af7e23b84d2.jpeg', 'image/jpeg', 4, '2017-02-23 18:28:19', 0),
(19, 8, '58af7e23d6552.jpeg', 'http://localhost/hospital/photos/00008/58af7e23d6552.jpeg', 'image/jpeg', 4, '2017-02-23 18:28:19', 0),
(20, 8, '58af7e23d9c02.jpeg', 'http://localhost/hospital/photos/00008/58af7e23d9c02.jpeg', 'image/jpeg', 4, '2017-02-23 18:28:19', 0),
(21, 9, '58af84e9c73d1.jpeg', 'http://localhost/hospital/photos/00009/58af84e9c73d1.jpeg', 'image/jpeg', 4, '2017-02-23 18:57:13', 0),
(22, 9, '58af84e9cb63a.jpeg', 'http://localhost/hospital/photos/00009/58af84e9cb63a.jpeg', 'image/jpeg', 4, '2017-02-23 18:57:13', 0),
(23, 9, '58af84e9cd192.jpeg', 'http://localhost/hospital/photos/00009/58af84e9cd192.jpeg', 'image/jpeg', 4, '2017-02-23 18:57:13', 0),
(24, 9, '58af84e9d2f54.jpeg', 'http://localhost/hospital/photos/00009/58af84e9d2f54.jpeg', 'image/jpeg', 4, '2017-02-23 18:57:13', 0),
(25, 10, '58af8d1009606.jpeg', 'http://localhost/hospital/photos/00010/58af8d1009606.jpeg', 'image/jpeg', 4, '2017-02-23 19:32:00', 0),
(26, 10, '58af8d100d86f.jpeg', 'http://localhost/hospital/photos/00010/58af8d100d86f.jpeg', 'image/jpeg', 4, '2017-02-23 19:32:00', 0),
(27, 11, '58b1b05766cb9.jpeg', 'http://localhost/hospital/photos/00011/58b1b05766cb9.jpeg', 'image/jpeg', 4, '2017-02-25 10:27:03', 0),
(28, 11, '58b1b05774395.jpeg', 'http://localhost/hospital/photos/00011/58b1b05774395.jpeg', 'image/jpeg', 4, '2017-02-25 10:27:03', 0),
(29, 12, '58b1d3a40f680.jpeg', 'http://localhost/hospital/photos/00012/58b1d3a40f680.jpeg', 'image/jpeg', 4, '2017-02-25 12:57:40', 0),
(30, 12, '58b1d3a416bb1.jpeg', 'http://localhost/hospital/photos/00012/58b1d3a416bb1.jpeg', 'image/jpeg', 4, '2017-02-25 12:57:40', 0),
(31, 13, '58b4e254af280.jpeg', 'http://localhost/hospital/photos/00013/58b4e254af280.jpeg', 'image/jpeg', 4, '2017-02-27 20:37:08', 0),
(32, 13, '58b4e254b7b3a.jpeg', 'http://localhost/hospital/photos/00013/58b4e254b7b3a.jpeg', 'image/jpeg', 4, '2017-02-27 20:37:08', 0),
(33, 14, '58b5ffc9111a6.jpeg', 'http://localhost/hospital/photos/00014/58b5ffc9111a6.jpeg', 'image/jpeg', 4, '2017-02-28 16:55:05', 0),
(34, 14, '58b5ffc91a230.jpeg', 'http://localhost/hospital/photos/00014/58b5ffc91a230.jpeg', 'image/jpeg', 4, '2017-02-28 16:55:05', 0),
(35, 15, '58b746cbc205a.jpeg', 'http://localhost/hospital/photos/00015/58b746cbc205a.jpeg', 'image/jpeg', 4, '2017-03-01 16:10:19', 0),
(36, 15, '58b746cbcc854.jpeg', 'http://localhost/hospital/photos/00015/58b746cbcc854.jpeg', 'image/jpeg', 4, '2017-03-01 16:10:19', 0),
(37, 16, '58b763964a0b3.jpeg', 'http://localhost/hospital/photos/00016/58b763964a0b3.jpeg', 'image/jpeg', 4, '2017-03-01 18:13:10', 0),
(38, 16, '58b763964eed4.jpeg', 'http://localhost/hospital/photos/00016/58b763964eed4.jpeg', 'image/jpeg', 4, '2017-03-01 18:13:10', 0),
(39, 17, '58b8caa1f3a30.jpeg', 'http://localhost/hospital/photos/00017/58b8caa1f3a30.jpeg', 'image/jpeg', 4, '2017-03-02 19:45:06', 0),
(40, 17, '58b8caa20887a.jpeg', 'http://localhost/hospital/photos/00017/58b8caa20887a.jpeg', 'image/jpeg', 4, '2017-03-02 19:45:06', 0),
(41, 18, '58b9f108eb30d.jpeg', 'http://localhost/hospital/photos/00018/58b9f108eb30d.jpeg', 'image/jpeg', 4, '2017-03-03 16:41:12', 0),
(42, 18, '58b9f108f012e.jpeg', 'http://localhost/hospital/photos/00018/58b9f108f012e.jpeg', 'image/jpeg', 4, '2017-03-03 16:41:12', 0),
(43, 19, '58ba13885d21d.jpeg', 'http://localhost/hospital/photos/00019/58ba13885d21d.jpeg', 'image/jpeg', 4, '2017-03-03 19:08:24', 0),
(44, 19, '58ba138862426.jpeg', 'http://localhost/hospital/photos/00019/58ba138862426.jpeg', 'image/jpeg', 4, '2017-03-03 19:08:24', 0),
(45, 19, '58ba13886474f.jpeg', 'http://localhost/hospital/photos/00019/58ba13886474f.jpeg', 'image/jpeg', 4, '2017-03-03 19:08:24', 0),
(46, 20, '58ba247910521.jpeg', 'http://localhost/hospital/photos/00020/58ba247910521.jpeg', 'image/jpeg', 4, '2017-03-03 20:20:41', 0),
(47, 20, '58ba24791a54c.jpeg', 'http://localhost/hospital/photos/00020/58ba24791a54c.jpeg', 'image/jpeg', 4, '2017-03-03 20:20:41', 0),
(48, 20, '58ba24791ef85.jpeg', 'http://localhost/hospital/photos/00020/58ba24791ef85.jpeg', 'image/jpeg', 4, '2017-03-03 20:20:41', 0),
(49, 21, '58bdf80f874aa.jpeg', 'http://localhost/hospital/photos/00021/58bdf80f874aa.jpeg', 'image/jpeg', 4, '2017-03-06 18:00:15', 0),
(50, 21, '58bdf80f8bee3.jpeg', 'http://localhost/hospital/photos/00021/58bdf80f8bee3.jpeg', 'image/jpeg', 4, '2017-03-06 18:00:15', 0),
(51, 22, '58bdfe809be0d.jpeg', 'http://localhost/hospital/photos/00022/58bdfe809be0d.jpeg', 'image/jpeg', 4, '2017-03-06 18:27:44', 0),
(52, 22, '58bdfe80a0846.jpeg', 'http://localhost/hospital/photos/00022/58bdfe80a0846.jpeg', 'image/jpeg', 4, '2017-03-06 18:27:44', 0),
(53, 23, '58c1d2f1522af.jpeg', 'http://localhost/hospital/photos/00023/58c1d2f1522af.jpeg', 'image/jpeg', 4, '2017-03-09 16:10:57', 0),
(54, 23, '58c1d2f159011.jpeg', 'http://localhost/hospital/photos/00023/58c1d2f159011.jpeg', 'image/jpeg', 4, '2017-03-09 16:10:57', 0),
(55, 23, '58c1d2f15af51.jpeg', 'http://localhost/hospital/photos/00023/58c1d2f15af51.jpeg', 'image/jpeg', 4, '2017-03-09 16:10:57', 0),
(56, 23, '58c1d2f15ce92.jpeg', 'http://localhost/hospital/photos/00023/58c1d2f15ce92.jpeg', 'image/jpeg', 4, '2017-03-09 16:10:57', 0),
(57, 24, '58c1e0653289f.jpeg', 'http://localhost/hospital/photos/00024/58c1e0653289f.jpeg', 'image/jpeg', 4, '2017-03-09 17:08:21', 0),
(58, 24, '58c1e06537e90.jpeg', 'http://localhost/hospital/photos/00024/58c1e06537e90.jpeg', 'image/jpeg', 4, '2017-03-09 17:08:21', 0),
(59, 25, '58c1fd14c8c0f.jpeg', 'http://localhost/hospital/photos/00025/58c1fd14c8c0f.jpeg', 'image/jpeg', 4, '2017-03-09 19:10:44', 0),
(60, 25, '58c1fd14ce9d0.jpeg', 'http://localhost/hospital/photos/00025/58c1fd14ce9d0.jpeg', 'image/jpeg', 4, '2017-03-09 19:10:44', 0),
(61, 26, '58c331c27ad4e.jpeg', 'http://localhost/hospital/photos/00026/58c331c27ad4e.jpeg', 'image/jpeg', 4, '2017-03-10 17:07:46', 0),
(62, 26, '58c331c2812df.jpeg', 'http://localhost/hospital/photos/00026/58c331c2812df.jpeg', 'image/jpeg', 4, '2017-03-10 17:07:46', 0),
(63, 27, '58c33cefc66e8.jpeg', 'http://localhost/hospital/photos/00027/58c33cefc66e8.jpeg', 'image/jpeg', 4, '2017-03-10 17:55:27', 0),
(64, 27, '58c33cefcb8f1.jpeg', 'http://localhost/hospital/photos/00027/58c33cefcb8f1.jpeg', 'image/jpeg', 4, '2017-03-10 17:55:27', 0),
(65, 28, '58c34c096f8ae.jpeg', 'http://localhost/hospital/photos/00028/58c34c096f8ae.jpeg', 'image/jpeg', 4, '2017-03-10 18:59:53', 0),
(66, 28, '58c34c097372f.jpeg', 'http://localhost/hospital/photos/00028/58c34c097372f.jpeg', 'image/jpeg', 4, '2017-03-10 18:59:53', 0),
(67, 29, '58c3541ee15cc.jpeg', 'http://localhost/hospital/photos/00029/58c3541ee15cc.jpeg', 'image/jpeg', 4, '2017-03-10 19:34:22', 0),
(68, 29, '58c3541ee4c7d.jpeg', 'http://localhost/hospital/photos/00029/58c3541ee4c7d.jpeg', 'image/jpeg', 4, '2017-03-10 19:34:22', 0),
(69, 30, '58c425f6c177c.jpeg', 'http://localhost/hospital/photos/00030/58c425f6c177c.jpeg', 'image/jpeg', 4, '2017-03-11 10:29:42', 0),
(70, 30, '58c425f6c7d0d.jpeg', 'http://localhost/hospital/photos/00030/58c425f6c7d0d.jpeg', 'image/jpeg', 4, '2017-03-11 10:29:42', 0),
(71, 30, '58c425f6c9865.jpeg', 'http://localhost/hospital/photos/00030/58c425f6c9865.jpeg', 'image/jpeg', 4, '2017-03-11 10:29:42', 0),
(72, 30, '58c425f6cb3be.jpeg', 'http://localhost/hospital/photos/00030/58c425f6cb3be.jpeg', 'image/jpeg', 4, '2017-03-11 10:29:42', 0),
(73, 32, '58c72b6b30c60.jpeg', 'http://localhost/hospital/photos/00032/58c72b6b30c60.jpeg', 'image/jpeg', 4, '2017-03-13 17:29:47', 0),
(74, 32, '58c72b6b352b1.jpeg', 'http://localhost/hospital/photos/00032/58c72b6b352b1.jpeg', 'image/jpeg', 4, '2017-03-13 17:29:47', 0),
(75, 33, '58c73f7dbf92e.jpeg', 'http://localhost/hospital/photos/00033/58c73f7dbf92e.jpeg', 'image/jpeg', 4, '2017-03-13 18:55:25', 0),
(76, 33, '58c73f7dc4750.jpeg', 'http://localhost/hospital/photos/00033/58c73f7dc4750.jpeg', 'image/jpeg', 4, '2017-03-13 18:55:25', 0),
(77, 34, '58c8899635872.jpeg', 'http://localhost/hospital/photos/00034/58c8899635872.jpeg', 'image/jpeg', 4, '2017-03-14 18:23:50', 0),
(78, 34, '58c889963d18b.jpeg', 'http://localhost/hospital/photos/00034/58c889963d18b.jpeg', 'image/jpeg', 4, '2017-03-14 18:23:50', 0),
(79, 34, '58c889964006c.jpeg', 'http://localhost/hospital/photos/00034/58c889964006c.jpeg', 'image/jpeg', 4, '2017-03-14 18:23:50', 0),
(80, 34, '58c8899641fad.jpeg', 'http://localhost/hospital/photos/00034/58c8899641fad.jpeg', 'image/jpeg', 4, '2017-03-14 18:23:50', 0),
(81, 35, '58c9ea886ee64.jpeg', 'http://localhost/hospital/photos/00035/58c9ea886ee64.jpeg', 'image/jpeg', 4, '2017-03-15 19:29:44', 0),
(82, 35, '58c9ea88730cd.jpeg', 'http://localhost/hospital/photos/00035/58c9ea88730cd.jpeg', 'image/jpeg', 4, '2017-03-15 19:29:44', 0),
(83, 35, '58c9ea887500e.jpeg', 'http://localhost/hospital/photos/00035/58c9ea887500e.jpeg', 'image/jpeg', 4, '2017-03-15 19:29:44', 0),
(84, 36, '58cb28c76f638.jpeg', 'http://localhost/hospital/photos/00036/58cb28c76f638.jpeg', 'image/jpeg', 4, '2017-03-16 18:07:35', 0),
(85, 36, '58cb28c7738a1.jpeg', 'http://localhost/hospital/photos/00036/58cb28c7738a1.jpeg', 'image/jpeg', 4, '2017-03-16 18:07:35', 0),
(86, 36, '58cb28c774c29.jpeg', 'http://localhost/hospital/photos/00036/58cb28c774c29.jpeg', 'image/jpeg', 4, '2017-03-16 18:07:35', 0),
(87, 37, '58cb32abe11ed.jpeg', 'http://localhost/hospital/photos/00037/58cb32abe11ed.jpeg', 'image/jpeg', 4, '2017-03-16 18:49:47', 0),
(88, 37, '58cb32abe5c26.jpeg', 'http://localhost/hospital/photos/00037/58cb32abe5c26.jpeg', 'image/jpeg', 4, '2017-03-16 18:49:47', 0),
(89, 37, '58cb32abe7b67.jpeg', 'http://localhost/hospital/photos/00037/58cb32abe7b67.jpeg', 'image/jpeg', 4, '2017-03-16 18:49:47', 0),
(90, 38, '58cc89e96abb9.jpeg', 'http://localhost/hospital/photos/00038/58cc89e96abb9.jpeg', 'image/jpeg', 4, '2017-03-17 19:14:17', 0),
(91, 38, '58cc89e9720eb.jpeg', 'http://localhost/hospital/photos/00038/58cc89e9720eb.jpeg', 'image/jpeg', 4, '2017-03-17 19:14:17', 0),
(92, 38, '58cc89e974413.jpeg', 'http://localhost/hospital/photos/00038/58cc89e974413.jpeg', 'image/jpeg', 4, '2017-03-17 19:14:17', 0),
(93, 39, '58cc992f4695b.jpeg', 'http://localhost/hospital/photos/00039/58cc992f4695b.jpeg', 'image/jpeg', 4, '2017-03-17 20:19:27', 0),
(94, 39, '58cc992f4b394.jpeg', 'http://localhost/hospital/photos/00039/58cc992f4b394.jpeg', 'image/jpeg', 4, '2017-03-17 20:19:27', 0),
(95, 39, '58cc992f4d2d4.jpeg', 'http://localhost/hospital/photos/00039/58cc992f4d2d4.jpeg', 'image/jpeg', 4, '2017-03-17 20:19:27', 0),
(96, 40, '58cca0b0b9329.jpeg', 'http://localhost/hospital/photos/00040/58cca0b0b9329.jpeg', 'image/jpeg', 4, '2017-03-17 20:51:28', 0),
(97, 40, '58cca0b0bd1aa.jpeg', 'http://localhost/hospital/photos/00040/58cca0b0bd1aa.jpeg', 'image/jpeg', 4, '2017-03-17 20:51:28', 0),
(98, 41, '58d1b02be3c8d.jpeg', 'http://localhost/hospital/photos/00041/58d1b02be3c8d.jpeg', 'image/jpeg', 4, '2017-03-21 16:58:51', 0),
(99, 41, '58d1b02beb5a7.jpeg', 'http://localhost/hospital/photos/00041/58d1b02beb5a7.jpeg', 'image/jpeg', 4, '2017-03-21 16:58:51', 0),
(100, 42, '58d1be79e44e0.jpeg', 'http://localhost/hospital/photos/00042/58d1be79e44e0.jpeg', 'image/jpeg', 4, '2017-03-21 17:59:53', 0),
(101, 42, '58d1be79e7b91.jpeg', 'http://localhost/hospital/photos/00042/58d1be79e7b91.jpeg', 'image/jpeg', 4, '2017-03-21 17:59:53', 0),
(102, 42, '58d1be79ea2a2.jpeg', 'http://localhost/hospital/photos/00042/58d1be79ea2a2.jpeg', 'image/jpeg', 4, '2017-03-21 17:59:53', 0),
(103, 43, '58d464819e75b.jpeg', 'http://localhost/hospital/photos/00043/58d464819e75b.jpeg', 'image/jpeg', 4, '2017-03-23 18:12:49', 0),
(104, 43, '58d46481a6c2c.jpeg', 'http://localhost/hospital/photos/00043/58d46481a6c2c.jpeg', 'image/jpeg', 4, '2017-03-23 18:12:49', 0),
(105, 43, '58d46481a8f55.jpeg', 'http://localhost/hospital/photos/00043/58d46481a8f55.jpeg', 'image/jpeg', 4, '2017-03-23 18:12:49', 0),
(106, 44, '58d46cbd1a45d.jpeg', 'http://localhost/hospital/photos/00044/58d46cbd1a45d.jpeg', 'image/jpeg', 4, '2017-03-23 18:47:57', 0),
(107, 44, '58d46cbd1ee96.jpeg', 'http://localhost/hospital/photos/00044/58d46cbd1ee96.jpeg', 'image/jpeg', 4, '2017-03-23 18:47:57', 0),
(108, 44, '58d46cbd20606.jpeg', 'http://localhost/hospital/photos/00044/58d46cbd20606.jpeg', 'image/jpeg', 4, '2017-03-23 18:47:57', 0),
(109, 45, '58d4745014fe7.jpeg', 'http://localhost/hospital/photos/00045/58d4745014fe7.jpeg', 'image/jpeg', 4, '2017-03-23 19:20:16', 0),
(110, 45, '58d4745019e08.jpeg', 'http://localhost/hospital/photos/00045/58d4745019e08.jpeg', 'image/jpeg', 4, '2017-03-23 19:20:16', 0),
(111, 45, '58d474501d0d1.jpeg', 'http://localhost/hospital/photos/00045/58d474501d0d1.jpeg', 'image/jpeg', 4, '2017-03-23 19:20:16', 0),
(112, 46, '58d47a7618e40.jpeg', 'http://localhost/hospital/photos/00046/58d47a7618e40.jpeg', 'image/jpeg', 4, '2017-03-23 19:46:30', 0),
(113, 46, '58d47a7649f74.jpeg', 'http://localhost/hospital/photos/00046/58d47a7649f74.jpeg', 'image/jpeg', 4, '2017-03-23 19:46:30', 0),
(114, 46, '58d47a764ca6c.jpeg', 'http://localhost/hospital/photos/00046/58d47a764ca6c.jpeg', 'image/jpeg', 4, '2017-03-23 19:46:30', 0),
(115, 46, '58d47a764f94d.jpeg', 'http://localhost/hospital/photos/00046/58d47a764f94d.jpeg', 'image/jpeg', 4, '2017-03-23 19:46:30', 0),
(116, 47, '58d48381456d5.jpeg', 'http://localhost/hospital/photos/00047/58d48381456d5.jpeg', 'image/jpeg', 4, '2017-03-23 20:25:05', 0),
(117, 47, '58d4838153580.jpeg', 'http://localhost/hospital/photos/00047/58d4838153580.jpeg', 'image/jpeg', 4, '2017-03-23 20:25:05', 0),
(118, 47, '58d48381550d9.jpeg', 'http://localhost/hospital/photos/00047/58d48381550d9.jpeg', 'image/jpeg', 4, '2017-03-23 20:25:05', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilios`
--

CREATE TABLE `domicilios` (
  `domicilios_id` int(11) NOT NULL,
  `calle` varchar(100) NOT NULL,
  `colonia` varchar(100) NOT NULL,
  `ciudad` varchar(75) NOT NULL,
  `estado` varchar(75) NOT NULL,
  `pais` varchar(30) NOT NULL,
  `codigo_postal` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `dt_colposcopio`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `dt_colposcopio` (
`consecutivo` int(1)
,`id` int(11)
,`expediente` int(5) unsigned zerofill
,`fecha` varchar(10)
,`nombre` varchar(277)
,`fechaNacimiento` varchar(10)
,`procedimiento` varchar(100)
,`instrumento` varchar(50)
,`motivoEstudio` varchar(150)
,`anestesia` varchar(2)
,`hallazgos` text
,`impresionDiagnostica` text
,`acciones` varchar(222)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfermedades`
--

CREATE TABLE `enfermedades` (
  `enfermedades_id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_civiles`
--

CREATE TABLE `estados_civiles` (
  `estados_civiles_id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instrumentos`
--

CREATE TABLE `instrumentos` (
  `instrumentos_id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `instrumentos`
--

INSERT INTO `instrumentos` (`instrumentos_id`, `nombre`, `borrado`) VALUES
(1, 'Colposcopio Vasconcellos', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_colposcopio`
--

CREATE TABLE `log_colposcopio` (
  `id` int(11) NOT NULL,
  `accion` varchar(10) NOT NULL,
  `colposcopio_id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `procedimiento` varchar(100) NOT NULL,
  `instrumento` varchar(50) NOT NULL DEFAULT 'COLPOSCOPIO',
  `motivo_estudio` varchar(150) NOT NULL,
  `anestesia` tinyint(1) NOT NULL DEFAULT '0',
  `hallazgos` text NOT NULL,
  `impresion_diagnostica` text NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_colposcopio_imagenes`
--

CREATE TABLE `log_colposcopio_imagenes` (
  `id` int(11) NOT NULL,
  `accion` varchar(10) NOT NULL,
  `colposcopio_imagenes_id` int(11) NOT NULL,
  `colposcopio_id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(150) NOT NULL,
  `formato` varchar(75) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_pacientes`
--

CREATE TABLE `log_pacientes` (
  `id` int(11) NOT NULL,
  `accion` varchar(10) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ocupaciones`
--

CREATE TABLE `ocupaciones` (
  `ocupaciones_id` int(11) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `pacientes_id` int(11) NOT NULL,
  `expediente` int(5) UNSIGNED ZEROFILL NOT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `nombre` varchar(75) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `peso` float DEFAULT NULL,
  `estatura` float DEFAULT NULL,
  `sexo` varchar(10) DEFAULT NULL,
  `telefono_1` varchar(10) DEFAULT NULL,
  `telefono_2` varchar(10) DEFAULT NULL,
  `celular` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url_foto` varchar(150) DEFAULT NULL,
  `curp` varchar(15) DEFAULT NULL,
  `rfc` varchar(15) DEFAULT NULL,
  `estados_civiles_id` tinyint(11) DEFAULT NULL,
  `ocupaciones_id` int(11) DEFAULT NULL,
  `domicilios_id` int(11) DEFAULT NULL,
  `enf_padre` varchar(75) DEFAULT NULL,
  `enf_madre` varchar(75) DEFAULT NULL,
  `enf_hnos` varchar(75) DEFAULT NULL,
  `deporte` varchar(50) DEFAULT NULL,
  `otras_actividades` varchar(50) DEFAULT NULL,
  `sanguineos_tipos_id` int(11) DEFAULT NULL,
  `observaciones` text,
  `menarquia` varchar(20) DEFAULT NULL,
  `duracion` varchar(40) DEFAULT NULL,
  `flujo` tinyint(4) DEFAULT NULL,
  `activa` tinyint(4) DEFAULT NULL,
  `partos` tinyint(4) DEFAULT NULL,
  `cantidad_p` varchar(15) DEFAULT NULL,
  `cesaria` varchar(15) DEFAULT NULL,
  `natural` varchar(15) DEFAULT NULL,
  `abortos` tinyint(4) DEFAULT NULL,
  `cantidad_a` varchar(20) DEFAULT NULL,
  `menopausia` tinyint(4) DEFAULT NULL,
  `info` varchar(50) DEFAULT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`pacientes_id`, `expediente`, `fecha_ingreso`, `nombre`, `apellido_paterno`, `apellido_materno`, `fecha_nacimiento`, `peso`, `estatura`, `sexo`, `telefono_1`, `telefono_2`, `celular`, `email`, `url_foto`, `curp`, `rfc`, `estados_civiles_id`, `ocupaciones_id`, `domicilios_id`, `enf_padre`, `enf_madre`, `enf_hnos`, `deporte`, `otras_actividades`, `sanguineos_tipos_id`, `observaciones`, `menarquia`, `duracion`, `flujo`, `activa`, `partos`, `cantidad_p`, `cesaria`, `natural`, `abortos`, `cantidad_a`, `menopausia`, `info`, `usuarios_id`, `timestamp`, `borrado`) VALUES
(1, 00001, NULL, 'ANA DENISE', 'RUIZ', 'GONZALEZ', '1984-07-19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-18 11:05:34', 0),
(2, 00002, NULL, 'ERIKA', 'BOTELLO', 'NIETO', '1978-01-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-20 20:06:03', 0),
(3, 00003, NULL, 'MARISELA', 'SANCHEZ', 'RAMIREZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-21 16:48:34', 0),
(4, 00004, NULL, 'ZURITH YESENIA ', 'MAYA', 'RAMIREZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-21 17:25:11', 0),
(5, 00005, NULL, 'TERESA', 'GONZALEZ', 'DEL ANGEL', '2062-07-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-22 17:08:30', 0),
(6, 00006, NULL, 'JESSICA ', 'VEGA', 'GARCIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-22 20:12:37', 0),
(7, 00007, NULL, 'YARA ', 'DE OLIVEIRA ', 'MAGALHAES', '1980-09-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-23 17:49:39', 0),
(8, 00008, NULL, 'KATHYA', 'HERNANDEZ', 'GONZALEZ', '1991-11-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-23 18:28:19', 0),
(9, 00009, NULL, 'VALENTINA', 'OROSCO', 'CASTRO', '1970-02-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-23 18:57:13', 0),
(10, 00010, NULL, 'CLAUDIA', 'ALVARADO', 'SANCHEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-23 19:32:00', 0),
(11, 00011, NULL, 'GUADALUPE', 'HERNANDEZ ', 'MORALES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-25 10:27:03', 0),
(12, 00012, NULL, 'AYDE', 'LAUREANO', 'GARCIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-25 12:57:40', 0),
(13, 00013, NULL, 'JENNY ', 'CONTRERAS ', 'GARCIA.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-27 20:37:08', 0),
(14, 00014, NULL, 'MARISELA', 'JUAREZ', 'JUAREZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-02-28 16:55:05', 0),
(15, 00015, NULL, 'MARIA DEL CONSUELO', ' MORALES ', 'HDZ.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-01 16:10:19', 0),
(16, 00016, NULL, 'ADDY MABEL ', ' HERNANDEZ', 'SIERRA.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-01 18:13:10', 0),
(17, 00017, NULL, 'TANIA', 'GONZALEZ', 'RANGEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-02 19:45:05', 0),
(18, 00018, NULL, 'BEATRIZ', 'SANCHEZ', 'MARTINEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-03 16:41:12', 0),
(19, 00019, NULL, 'ARCELIA', 'ESPINOZA', 'AMADOR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-03 19:08:24', 0),
(20, 00020, NULL, 'TANIA ', 'CABANE ', 'SANCHEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-03 20:20:41', 0),
(21, 00021, NULL, 'MARTHA', 'LOZANO', 'MACIAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-06 18:00:15', 0),
(22, 00022, NULL, 'DANIELA', 'VILLEGAS', 'LOZANO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-06 18:27:44', 0),
(23, 00023, NULL, 'DIANA', 'PANCARDO', 'MORA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-09 16:10:57', 0),
(24, 00024, NULL, 'NORMA ALICIA', 'IZAGUIRRE', 'SOLANO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-09 17:08:21', 0),
(25, 00025, NULL, 'BRENDA GUADALUPE ', 'ANDRADE', 'CRUZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-09 19:10:44', 0),
(26, 00026, NULL, 'ORQUIDEA', 'ARIAS', 'GAONA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-10 17:07:46', 0),
(27, 00027, NULL, 'CARMEN ARIANA', 'VELAZQUEZ', 'HERNANDEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-10 17:55:27', 0),
(28, 00028, NULL, 'GLORIA CRISTINA', 'PANDO ', 'ARGUELLES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-10 18:59:53', 0),
(29, 00029, NULL, 'ASUNCION A', 'MONTALVO ', 'NIÑO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-10 19:34:22', 0),
(30, 00030, NULL, 'ROSA PATRICIA', 'SAUCEDO', 'LOPEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-11 10:29:42', 0),
(31, 00031, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-13 17:24:37', 0),
(32, 00032, NULL, 'ORALIA', 'HERNANDEZ', 'SAN MARTIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-13 17:29:47', 0),
(33, 00033, NULL, 'ERIKA ', 'CASTELAN', 'HUERTA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-13 18:55:25', 0),
(34, 00034, NULL, 'MAYRA', 'GARCES', 'HERNANDEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-14 18:23:50', 0),
(35, 00035, NULL, 'SILVIA', 'VAZQUEZ', 'MEDINA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-15 19:29:44', 0),
(36, 00036, NULL, 'DORA NELLY', 'NAVA', 'TORRES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-16 18:07:35', 0),
(37, 00037, NULL, 'MONICA', 'BERRIOS', 'SANCHEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-16 18:49:47', 0),
(38, 00038, NULL, 'MARIANA', 'GONZALEZ', 'CARRANZA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-17 19:14:17', 0),
(39, 00039, NULL, 'SILVIA ROSA', 'PADILLA', 'GARCIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-17 20:19:27', 0),
(40, 00040, NULL, 'ISAURA PAULINA', 'ALDAMA', 'DEANTES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-17 20:51:28', 0),
(41, 00041, NULL, 'LOURDES', 'REGALADO', 'GARZA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-21 16:58:51', 0),
(42, 00042, NULL, 'ADRIANA', 'RAMIREZ', 'DOMINGUEZ.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-21 17:59:53', 0),
(43, 00043, NULL, 'LUCERO', 'MIJES ', 'MACHUCA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-23 18:12:49', 0),
(44, 00044, NULL, 'NORIKO SCARLETT ', 'DAN ', 'RUIZ.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-23 18:47:57', 0),
(45, 00045, NULL, 'DORLY', 'GOMEZ', 'JUAREZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-23 19:20:16', 0),
(46, 00046, NULL, 'TEODULA', 'JUAREZ', 'DEL ANGEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-23 19:46:30', 0),
(47, 00047, NULL, 'WALQUIRIA NALLELI ', 'SALAS', ' ZIMBRON.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2017-03-23 20:25:05', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes_alergias`
--

CREATE TABLE `pacientes_alergias` (
  `id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `alergias_id` int(11) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes_enfermedades`
--

CREATE TABLE `pacientes_enfermedades` (
  `id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `enfermedades_id` int(11) NOT NULL,
  `observaciones` text NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes_vacunas`
--

CREATE TABLE `pacientes_vacunas` (
  `id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `vacunas_id` int(11) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sanguineos_tipos`
--

CREATE TABLE `sanguineos_tipos` (
  `sanguineos_tipos_id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuarios_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `usuarios_niveles_id` tinyint(4) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuarios_id`, `username`, `password`, `usuarios_niveles_id`, `borrado`) VALUES
(1, 'demo', 'admin', 1, 0),
(2, 'demo', 'cliente', 2, 0),
(3, 'demo', 'consultor', 3, 0),
(4, 'blanca.robles', 'b10950Re', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_accesos`
--

CREATE TABLE `usuarios_accesos` (
  `usuarios_accesos_id` tinyint(4) NOT NULL,
  `clave` varchar(6) NOT NULL,
  `descripcion` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_accesos`
--

INSERT INTO `usuarios_accesos` (`usuarios_accesos_id`, `clave`, `descripcion`) VALUES
(1, 'SUD01', 'Acceso a todo.'),
(2, 'ADM01', 'Acceso a módulo \'Inicio\'.'),
(3, 'ADM02', 'Acceso a módulo \'Guardar\'.'),
(4, 'ADM03', 'Acceso a módulo \'Consultar\'.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_niveles`
--

CREATE TABLE `usuarios_niveles` (
  `usuarios_niveles_id` tinyint(4) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_niveles`
--

INSERT INTO `usuarios_niveles` (`usuarios_niveles_id`, `nombre`, `borrado`) VALUES
(1, 'SUDO', 0),
(2, 'Administrador', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_permisos`
--

CREATE TABLE `usuarios_permisos` (
  `usuarios_permisos_id` int(11) NOT NULL,
  `usuarios_niveles_id` tinyint(4) NOT NULL,
  `usuarios_accesos_id` tinyint(4) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_permisos`
--

INSERT INTO `usuarios_permisos` (`usuarios_permisos_id`, `usuarios_niveles_id`, `usuarios_accesos_id`, `borrado`) VALUES
(1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vacunas`
--

CREATE TABLE `vacunas` (
  `vacunas_id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura para la vista `dt_colposcopio`
--
DROP TABLE IF EXISTS `dt_colposcopio`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dt_colposcopio`  AS  select 1 AS `consecutivo`,`c`.`colposcopio_id` AS `id`,`p`.`expediente` AS `expediente`,date_format(`c`.`fecha`,'%d/%m/%Y') AS `fecha`,concat(`p`.`nombre`,' ',`p`.`apellido_paterno`,' ',`p`.`apellido_materno`) AS `nombre`,date_format(`p`.`fecha_nacimiento`,'%d/%m/%Y') AS `fechaNacimiento`,`c`.`procedimiento` AS `procedimiento`,`i`.`nombre` AS `instrumento`,`c`.`motivo_estudio` AS `motivoEstudio`,if((`c`.`anestesia` = 1),'SI','NO') AS `anestesia`,`c`.`hallazgos` AS `hallazgos`,`c`.`impresion_diagnostica` AS `impresionDiagnostica`,concat('<ul class="dropdown menu navigation" data-dropdown-menu data-closing-time=50>','<li>','<a href="#acciones">Da clic para...</a>','<ul class="menu">','<li><a href="#status" data-action="ver-imagenes">VER IMAGENES</a></li>','</ul>','</li></ul>') AS `acciones` from ((`pacientes` `p` join `colposcopio` `c` on((`c`.`pacientes_id` = `p`.`pacientes_id`))) join `instrumentos` `i` on((`i`.`instrumentos_id` = `c`.`instrumentos_id`))) where (`p`.`borrado` = 0) order by `p`.`pacientes_id` ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alergias`
--
ALTER TABLE `alergias`
  ADD PRIMARY KEY (`alergias_id`);

--
-- Indices de la tabla `colposcopio`
--
ALTER TABLE `colposcopio`
  ADD PRIMARY KEY (`colposcopio_id`);

--
-- Indices de la tabla `colposcopio_imagenes`
--
ALTER TABLE `colposcopio_imagenes`
  ADD PRIMARY KEY (`colposcopio_imagenes_id`);

--
-- Indices de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  ADD PRIMARY KEY (`domicilios_id`);

--
-- Indices de la tabla `enfermedades`
--
ALTER TABLE `enfermedades`
  ADD PRIMARY KEY (`enfermedades_id`);

--
-- Indices de la tabla `estados_civiles`
--
ALTER TABLE `estados_civiles`
  ADD PRIMARY KEY (`estados_civiles_id`);

--
-- Indices de la tabla `instrumentos`
--
ALTER TABLE `instrumentos`
  ADD PRIMARY KEY (`instrumentos_id`);

--
-- Indices de la tabla `log_colposcopio`
--
ALTER TABLE `log_colposcopio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `log_colposcopio_imagenes`
--
ALTER TABLE `log_colposcopio_imagenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `log_pacientes`
--
ALTER TABLE `log_pacientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ocupaciones`
--
ALTER TABLE `ocupaciones`
  ADD PRIMARY KEY (`ocupaciones_id`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`pacientes_id`);

--
-- Indices de la tabla `pacientes_alergias`
--
ALTER TABLE `pacientes_alergias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pacientes_enfermedades`
--
ALTER TABLE `pacientes_enfermedades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pacientes_vacunas`
--
ALTER TABLE `pacientes_vacunas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sanguineos_tipos`
--
ALTER TABLE `sanguineos_tipos`
  ADD PRIMARY KEY (`sanguineos_tipos_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuarios_id`);

--
-- Indices de la tabla `usuarios_accesos`
--
ALTER TABLE `usuarios_accesos`
  ADD PRIMARY KEY (`usuarios_accesos_id`);

--
-- Indices de la tabla `usuarios_niveles`
--
ALTER TABLE `usuarios_niveles`
  ADD PRIMARY KEY (`usuarios_niveles_id`);

--
-- Indices de la tabla `usuarios_permisos`
--
ALTER TABLE `usuarios_permisos`
  ADD PRIMARY KEY (`usuarios_permisos_id`);

--
-- Indices de la tabla `vacunas`
--
ALTER TABLE `vacunas`
  ADD PRIMARY KEY (`vacunas_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alergias`
--
ALTER TABLE `alergias`
  MODIFY `alergias_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `colposcopio`
--
ALTER TABLE `colposcopio`
  MODIFY `colposcopio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `colposcopio_imagenes`
--
ALTER TABLE `colposcopio_imagenes`
  MODIFY `colposcopio_imagenes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  MODIFY `domicilios_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `enfermedades`
--
ALTER TABLE `enfermedades`
  MODIFY `enfermedades_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estados_civiles`
--
ALTER TABLE `estados_civiles`
  MODIFY `estados_civiles_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `instrumentos`
--
ALTER TABLE `instrumentos`
  MODIFY `instrumentos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `log_colposcopio`
--
ALTER TABLE `log_colposcopio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `log_colposcopio_imagenes`
--
ALTER TABLE `log_colposcopio_imagenes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `log_pacientes`
--
ALTER TABLE `log_pacientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ocupaciones`
--
ALTER TABLE `ocupaciones`
  MODIFY `ocupaciones_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `pacientes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `pacientes_alergias`
--
ALTER TABLE `pacientes_alergias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pacientes_enfermedades`
--
ALTER TABLE `pacientes_enfermedades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pacientes_vacunas`
--
ALTER TABLE `pacientes_vacunas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sanguineos_tipos`
--
ALTER TABLE `sanguineos_tipos`
  MODIFY `sanguineos_tipos_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuarios_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuarios_accesos`
--
ALTER TABLE `usuarios_accesos`
  MODIFY `usuarios_accesos_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuarios_niveles`
--
ALTER TABLE `usuarios_niveles`
  MODIFY `usuarios_niveles_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios_permisos`
--
ALTER TABLE `usuarios_permisos`
  MODIFY `usuarios_permisos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `vacunas`
--
ALTER TABLE `vacunas`
  MODIFY `vacunas_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
