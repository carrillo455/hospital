-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-03-2017 a las 19:47:27
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hospital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adicciones`
--

CREATE TABLE `adicciones` (
  `adicciones_id` tinyint(4) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `borrado` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adicciones`
--

INSERT INTO `adicciones` (`adicciones_id`, `nombre`, `borrado`) VALUES
(1, 'Alcoholismo', 0),
(2, 'Tabaquismo', 0),
(3, 'Drogas', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alergias`
--

CREATE TABLE `alergias` (
  `alergias_id` int(11) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alergias`
--

INSERT INTO `alergias` (`alergias_id`, `nombre`, `borrado`) VALUES
(1, 'Medicamentos', 0),
(2, 'Alimentos', 0),
(3, 'Ambientales', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colposcopio`
--

CREATE TABLE `colposcopio` (
  `colposcopio_id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `procedimiento` varchar(100) NOT NULL,
  `instrumentos_id` tinyint(4) NOT NULL,
  `motivo_estudio` varchar(150) NOT NULL,
  `anestesia` tinyint(1) NOT NULL DEFAULT '0',
  `hallazgos` text NOT NULL,
  `impresion_diagnostica` text NOT NULL,
  `plan` text NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colposcopio_imagenes`
--

CREATE TABLE `colposcopio_imagenes` (
  `colposcopio_imagenes_id` int(11) NOT NULL,
  `colposcopio_id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(150) NOT NULL,
  `formato` varchar(75) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilios`
--

CREATE TABLE `domicilios` (
  `domicilios_id` int(11) NOT NULL,
  `calle` varchar(100) NOT NULL,
  `colonia` varchar(100) NOT NULL,
  `ciudad` varchar(75) NOT NULL,
  `estado` varchar(75) NOT NULL,
  `pais` varchar(30) NOT NULL,
  `codigo_postal` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `dt_colposcopio`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `dt_colposcopio` (
`consecutivo` int(1)
,`id` int(11)
,`expediente` int(5) unsigned zerofill
,`fecha` varchar(10)
,`nombre` varchar(277)
,`fechaNacimiento` varchar(10)
,`procedimiento` varchar(100)
,`instrumento` varchar(50)
,`motivoEstudio` varchar(150)
,`anestesia` varchar(2)
,`hallazgos` text
,`impresionDiagnostica` text
,`plan` text
,`acciones` varchar(291)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `dt_pacientes`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `dt_pacientes` (
`consecutivo` int(1)
,`id` int(11)
,`expediente` int(5) unsigned zerofill
,`fechaIngreso` varchar(10)
,`nombre` varchar(277)
,`fechaNacimiento` varchar(10)
,`sexo` varchar(10)
,`peso` float
,`estatura` float
,`telefono` varchar(10)
,`celular` varchar(10)
,`email` varchar(50)
,`nombreOcupacion` varchar(125)
,`nombreSanguineoTipo` varchar(30)
,`padecimientoActual` varchar(150)
,`exploracionFisica` varchar(150)
,`ta` varchar(150)
,`fr` varchar(150)
,`fc` varchar(150)
,`menarca` varchar(100)
,`gesta` varchar(150)
,`fum` varchar(150)
,`ivsa` varchar(150)
,`para` varchar(150)
,`abortos` tinyint(4)
,`cesareas` tinyint(4)
,`pSexuales` varchar(150)
,`otrasActividades` varchar(400)
,`observaciones` text
,`antecedentesFamiliares` varchar(255)
,`antecedentesPatologicos` varchar(255)
,`antecedentesQuirurgicos` varchar(255)
,`antecedentesGinecoobstetricos` varchar(255)
,`alergias` text
,`adicciones` text
,`vacunas` text
,`acciones` varchar(219)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfermedades`
--

CREATE TABLE `enfermedades` (
  `enfermedades_id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_civiles`
--

CREATE TABLE `estados_civiles` (
  `estados_civiles_id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instrumentos`
--

CREATE TABLE `instrumentos` (
  `instrumentos_id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `instrumentos`
--

INSERT INTO `instrumentos` (`instrumentos_id`, `nombre`, `borrado`) VALUES
(1, 'Colposcopio Vasconcellos', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_colposcopio`
--

CREATE TABLE `log_colposcopio` (
  `id` int(11) NOT NULL,
  `accion` varchar(10) NOT NULL,
  `colposcopio_id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `procedimiento` varchar(100) NOT NULL,
  `instrumento` varchar(50) NOT NULL DEFAULT 'COLPOSCOPIO',
  `motivo_estudio` varchar(150) NOT NULL,
  `anestesia` tinyint(1) NOT NULL DEFAULT '0',
  `hallazgos` text NOT NULL,
  `impresion_diagnostica` text NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_colposcopio_imagenes`
--

CREATE TABLE `log_colposcopio_imagenes` (
  `id` int(11) NOT NULL,
  `accion` varchar(10) NOT NULL,
  `colposcopio_imagenes_id` int(11) NOT NULL,
  `colposcopio_id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(150) NOT NULL,
  `formato` varchar(75) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_pacientes`
--

CREATE TABLE `log_pacientes` (
  `id` int(11) NOT NULL,
  `accion` varchar(10) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ocupaciones`
--

CREATE TABLE `ocupaciones` (
  `ocupaciones_id` int(11) NOT NULL,
  `nombre` varchar(125) DEFAULT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `pacientes_id` int(11) NOT NULL,
  `expediente` int(5) UNSIGNED ZEROFILL NOT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `nombre` varchar(75) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `peso` float DEFAULT NULL,
  `estatura` float DEFAULT NULL,
  `sexo` varchar(10) DEFAULT 'femenino',
  `telefono_1` varchar(10) DEFAULT NULL,
  `telefono_2` varchar(10) DEFAULT NULL,
  `celular` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url_foto` varchar(150) DEFAULT NULL,
  `curp` varchar(15) DEFAULT NULL,
  `rfc` varchar(15) DEFAULT NULL,
  `estados_civiles_id` tinyint(11) DEFAULT NULL,
  `ocupaciones_id` int(11) DEFAULT NULL,
  `domicilios_id` int(11) DEFAULT NULL,
  `antecedentes_familiares` varchar(255) DEFAULT NULL,
  `antecedentes_patologicos` varchar(255) DEFAULT NULL,
  `antecedentes_quirurgicos` varchar(255) DEFAULT NULL,
  `antecedentes_ginecoobstetricos` varchar(255) DEFAULT NULL,
  `deporte` varchar(50) DEFAULT NULL,
  `otras_actividades` varchar(400) DEFAULT NULL,
  `sanguineos_tipos_id` int(11) DEFAULT NULL,
  `observaciones` text,
  `menarca` varchar(100) DEFAULT NULL,
  `gesta` varchar(150) DEFAULT NULL,
  `fum` varchar(150) DEFAULT NULL,
  `ivsa` varchar(150) DEFAULT NULL,
  `para` varchar(150) DEFAULT NULL,
  `poses_sexuales` varchar(150) DEFAULT NULL,
  `padecimiento_actual` varchar(150) DEFAULT NULL,
  `exploracion_fisica` varchar(150) DEFAULT NULL,
  `t_a` varchar(150) DEFAULT NULL,
  `f_r` varchar(150) DEFAULT NULL,
  `f_c` varchar(150) DEFAULT NULL,
  `duracion` varchar(40) DEFAULT NULL,
  `flujo` tinyint(4) DEFAULT NULL,
  `activa` tinyint(4) DEFAULT NULL,
  `partos` tinyint(4) DEFAULT NULL,
  `cantidad_p` varchar(15) DEFAULT NULL,
  `cesareas` tinyint(4) DEFAULT NULL,
  `natural` tinyint(4) DEFAULT NULL,
  `abortos` tinyint(4) DEFAULT NULL,
  `cantidad_a` varchar(20) DEFAULT NULL,
  `menopausia` tinyint(4) DEFAULT NULL,
  `info` varchar(50) DEFAULT NULL,
  `usuarios_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes_adicciones`
--

CREATE TABLE `pacientes_adicciones` (
  `id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `adicciones_id` tinyint(4) NOT NULL,
  `tiene` tinyint(1) NOT NULL DEFAULT '0',
  `observaciones` text,
  `borrado` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes_alergias`
--

CREATE TABLE `pacientes_alergias` (
  `id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `alergias_id` tinyint(4) NOT NULL,
  `tiene` tinyint(1) NOT NULL DEFAULT '0',
  `observaciones` text NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes_enfermedades`
--

CREATE TABLE `pacientes_enfermedades` (
  `id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `enfermedades_id` int(11) NOT NULL,
  `tiene` tinyint(1) NOT NULL DEFAULT '0',
  `observaciones` text NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes_vacunas`
--

CREATE TABLE `pacientes_vacunas` (
  `id` int(11) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `vacunas_id` tinyint(4) NOT NULL,
  `tiene` tinyint(1) NOT NULL DEFAULT '0',
  `observaciones` text NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sanguineos_tipos`
--

CREATE TABLE `sanguineos_tipos` (
  `sanguineos_tipos_id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sanguineos_tipos`
--

INSERT INTO `sanguineos_tipos` (`sanguineos_tipos_id`, `nombre`, `borrado`) VALUES
(1, 'Por confirmar', 0),
(2, 'O+', 0),
(3, 'O-', 0),
(4, 'A+', 0),
(5, 'A-', 0),
(6, 'AB+', 0),
(7, 'AB-', 0),
(8, 'B+', 0),
(9, 'B-', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuarios_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `usuarios_niveles_id` tinyint(4) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuarios_id`, `username`, `password`, `usuarios_niveles_id`, `borrado`) VALUES
(1, 'demo', 'admin', 1, 0),
(2, 'demo', 'cliente', 2, 0),
(3, 'demo', 'consultor', 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_accesos`
--

CREATE TABLE `usuarios_accesos` (
  `usuarios_accesos_id` tinyint(4) NOT NULL,
  `clave` varchar(6) NOT NULL,
  `descripcion` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_accesos`
--

INSERT INTO `usuarios_accesos` (`usuarios_accesos_id`, `clave`, `descripcion`) VALUES
(1, 'SUD01', 'Acceso a todo.'),
(2, 'ADM01', 'Acceso a módulo \'Inicio\'.'),
(3, 'ADM02', 'Acceso a módulo \'Guardar\'.'),
(4, 'ADM03', 'Acceso a módulo \'Consultar\'.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_niveles`
--

CREATE TABLE `usuarios_niveles` (
  `usuarios_niveles_id` tinyint(4) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_niveles`
--

INSERT INTO `usuarios_niveles` (`usuarios_niveles_id`, `nombre`, `borrado`) VALUES
(1, 'SUDO', 0),
(2, 'Administrador', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_permisos`
--

CREATE TABLE `usuarios_permisos` (
  `usuarios_permisos_id` int(11) NOT NULL,
  `usuarios_niveles_id` tinyint(4) NOT NULL,
  `usuarios_accesos_id` tinyint(4) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_permisos`
--

INSERT INTO `usuarios_permisos` (`usuarios_permisos_id`, `usuarios_niveles_id`, `usuarios_accesos_id`, `borrado`) VALUES
(1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vacunas`
--

CREATE TABLE `vacunas` (
  `vacunas_id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vacunas`
--

INSERT INTO `vacunas` (`vacunas_id`, `nombre`, `borrado`) VALUES
(1, 'Poliomielitis', 0),
(2, 'Tétanos', 0),
(3, 'Sarampión', 0),
(4, 'Difteria', 0),
(5, 'Tosferina', 0),
(6, 'Tuberculosis', 0),
(7, 'Influenza', 0),
(8, 'Influenza B', 0),
(9, 'Hepatitis B', 0),
(10, 'Paroditis', 0),
(11, 'Rubéola', 0),
(12, 'Antigripe', 0);

-- --------------------------------------------------------

--
-- Estructura para la vista `dt_colposcopio`
--
DROP TABLE IF EXISTS `dt_colposcopio`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dt_colposcopio`  AS  select 1 AS `consecutivo`,`c`.`colposcopio_id` AS `id`,`p`.`expediente` AS `expediente`,date_format(`c`.`fecha`,'%d/%m/%Y') AS `fecha`,concat(`p`.`nombre`,' ',`p`.`apellido_paterno`,' ',`p`.`apellido_materno`) AS `nombre`,date_format(`p`.`fecha_nacimiento`,'%d/%m/%Y') AS `fechaNacimiento`,`c`.`procedimiento` AS `procedimiento`,`i`.`nombre` AS `instrumento`,`c`.`motivo_estudio` AS `motivoEstudio`,if((`c`.`anestesia` = 1),'SI','NO') AS `anestesia`,if((char_length(`c`.`hallazgos`) > 52),concat(substr(`c`.`hallazgos`,1,52),'...'),`c`.`hallazgos`) AS `hallazgos`,if((char_length(`c`.`impresion_diagnostica`) > 52),concat(substr(`c`.`impresion_diagnostica`,1,52),'...'),`c`.`impresion_diagnostica`) AS `impresionDiagnostica`,if((char_length(`c`.`plan`) > 52),concat(substr(`c`.`plan`,1,52),'...'),`c`.`plan`) AS `plan`,concat('<ul class="dropdown menu navigation" data-dropdown-menu data-closing-time=50>','<li>','<a href="#acciones">Da clic para...</a>','<ul class="menu">','<li><a href="#reporte" data-action="ver-reporte">VER REPORTE</a></li>','<li><a href="#status" data-action="ver-imagenes">VER IMAGENES</a></li>','</ul>','</li></ul>') AS `acciones` from ((`pacientes` `p` join `colposcopio` `c` on((`c`.`pacientes_id` = `p`.`pacientes_id`))) join `instrumentos` `i` on((`i`.`instrumentos_id` = `c`.`instrumentos_id`))) where (`p`.`borrado` = 0) order by `p`.`pacientes_id` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `dt_pacientes`
--
DROP TABLE IF EXISTS `dt_pacientes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dt_pacientes`  AS  select 1 AS `consecutivo`,`p`.`pacientes_id` AS `id`,`p`.`expediente` AS `expediente`,date_format(`p`.`fecha_ingreso`,'%d/%m/%Y') AS `fechaIngreso`,concat(`p`.`nombre`,' ',`p`.`apellido_paterno`,' ',`p`.`apellido_materno`) AS `nombre`,date_format(`p`.`fecha_nacimiento`,'%d/%m/%Y') AS `fechaNacimiento`,`p`.`sexo` AS `sexo`,`p`.`peso` AS `peso`,`p`.`estatura` AS `estatura`,`p`.`telefono_1` AS `telefono`,`p`.`celular` AS `celular`,`p`.`email` AS `email`,`o`.`nombre` AS `nombreOcupacion`,`st`.`nombre` AS `nombreSanguineoTipo`,`p`.`padecimiento_actual` AS `padecimientoActual`,`p`.`exploracion_fisica` AS `exploracionFisica`,`p`.`t_a` AS `ta`,`p`.`f_r` AS `fr`,`p`.`f_c` AS `fc`,`p`.`menarca` AS `menarca`,`p`.`gesta` AS `gesta`,`p`.`fum` AS `fum`,`p`.`ivsa` AS `ivsa`,`p`.`para` AS `para`,`p`.`abortos` AS `abortos`,`p`.`cesareas` AS `cesareas`,`p`.`poses_sexuales` AS `pSexuales`,`p`.`otras_actividades` AS `otrasActividades`,`p`.`observaciones` AS `observaciones`,`p`.`antecedentes_familiares` AS `antecedentesFamiliares`,`p`.`antecedentes_patologicos` AS `antecedentesPatologicos`,`p`.`antecedentes_quirurgicos` AS `antecedentesQuirurgicos`,`p`.`antecedentes_ginecoobstetricos` AS `antecedentesGinecoobstetricos`,(select group_concat(`a`.`nombre` separator '<br>') from (`pacientes_alergias` `pa` join `alergias` `a` on((`a`.`alergias_id` = `pa`.`alergias_id`))) where ((`pa`.`tiene` = 1) and (`pa`.`borrado` = 0) and (`pa`.`pacientes_id` = `p`.`pacientes_id`))) AS `alergias`,(select group_concat(`a`.`nombre` separator '<br>') from (`pacientes_adicciones` `pa` join `adicciones` `a` on((`a`.`adicciones_id` = `pa`.`adicciones_id`))) where ((`pa`.`tiene` = 1) and (`pa`.`borrado` = 0) and (`pa`.`pacientes_id` = `p`.`pacientes_id`))) AS `adicciones`,(select group_concat(`v`.`nombre` separator '<br>') from (`pacientes_vacunas` `pv` join `vacunas` `v` on((`v`.`vacunas_id` = `pv`.`vacunas_id`))) where ((`pv`.`tiene` = 1) and (`pv`.`borrado` = 0) and (`pv`.`pacientes_id` = `p`.`pacientes_id`))) AS `vacunas`,concat('<ul class="dropdown menu navigation" data-dropdown-menu data-closing-time=50>','<li>','<a href="#acciones">Da clic para...</a>','<ul class="menu">','<li><a href="#editar" data-action="editar-paciente">EDITAR</a></li></ul>','</li></ul>') AS `acciones` from ((`pacientes` `p` left join `ocupaciones` `o` on((`p`.`ocupaciones_id` = `o`.`ocupaciones_id`))) left join `sanguineos_tipos` `st` on((`p`.`sanguineos_tipos_id` = `st`.`sanguineos_tipos_id`))) where (`p`.`borrado` = 0) order by `p`.`pacientes_id` ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adicciones`
--
ALTER TABLE `adicciones`
  ADD PRIMARY KEY (`adicciones_id`);

--
-- Indices de la tabla `alergias`
--
ALTER TABLE `alergias`
  ADD PRIMARY KEY (`alergias_id`);

--
-- Indices de la tabla `colposcopio`
--
ALTER TABLE `colposcopio`
  ADD PRIMARY KEY (`colposcopio_id`);

--
-- Indices de la tabla `colposcopio_imagenes`
--
ALTER TABLE `colposcopio_imagenes`
  ADD PRIMARY KEY (`colposcopio_imagenes_id`);

--
-- Indices de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  ADD PRIMARY KEY (`domicilios_id`);

--
-- Indices de la tabla `enfermedades`
--
ALTER TABLE `enfermedades`
  ADD PRIMARY KEY (`enfermedades_id`);

--
-- Indices de la tabla `estados_civiles`
--
ALTER TABLE `estados_civiles`
  ADD PRIMARY KEY (`estados_civiles_id`);

--
-- Indices de la tabla `instrumentos`
--
ALTER TABLE `instrumentos`
  ADD PRIMARY KEY (`instrumentos_id`);

--
-- Indices de la tabla `log_colposcopio`
--
ALTER TABLE `log_colposcopio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `log_colposcopio_imagenes`
--
ALTER TABLE `log_colposcopio_imagenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `log_pacientes`
--
ALTER TABLE `log_pacientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ocupaciones`
--
ALTER TABLE `ocupaciones`
  ADD PRIMARY KEY (`ocupaciones_id`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`pacientes_id`);

--
-- Indices de la tabla `pacientes_adicciones`
--
ALTER TABLE `pacientes_adicciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pacientes_adicciones` (`adicciones_id`);

--
-- Indices de la tabla `pacientes_alergias`
--
ALTER TABLE `pacientes_alergias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pacientes_enfermedades`
--
ALTER TABLE `pacientes_enfermedades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pacientes_vacunas`
--
ALTER TABLE `pacientes_vacunas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sanguineos_tipos`
--
ALTER TABLE `sanguineos_tipos`
  ADD PRIMARY KEY (`sanguineos_tipos_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuarios_id`);

--
-- Indices de la tabla `usuarios_accesos`
--
ALTER TABLE `usuarios_accesos`
  ADD PRIMARY KEY (`usuarios_accesos_id`);

--
-- Indices de la tabla `usuarios_niveles`
--
ALTER TABLE `usuarios_niveles`
  ADD PRIMARY KEY (`usuarios_niveles_id`);

--
-- Indices de la tabla `usuarios_permisos`
--
ALTER TABLE `usuarios_permisos`
  ADD PRIMARY KEY (`usuarios_permisos_id`);

--
-- Indices de la tabla `vacunas`
--
ALTER TABLE `vacunas`
  ADD PRIMARY KEY (`vacunas_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alergias`
--
ALTER TABLE `alergias`
  MODIFY `alergias_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `colposcopio`
--
ALTER TABLE `colposcopio`
  MODIFY `colposcopio_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `colposcopio_imagenes`
--
ALTER TABLE `colposcopio_imagenes`
  MODIFY `colposcopio_imagenes_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  MODIFY `domicilios_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `enfermedades`
--
ALTER TABLE `enfermedades`
  MODIFY `enfermedades_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estados_civiles`
--
ALTER TABLE `estados_civiles`
  MODIFY `estados_civiles_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `instrumentos`
--
ALTER TABLE `instrumentos`
  MODIFY `instrumentos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `log_colposcopio`
--
ALTER TABLE `log_colposcopio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `log_colposcopio_imagenes`
--
ALTER TABLE `log_colposcopio_imagenes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `log_pacientes`
--
ALTER TABLE `log_pacientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ocupaciones`
--
ALTER TABLE `ocupaciones`
  MODIFY `ocupaciones_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `pacientes_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pacientes_adicciones`
--
ALTER TABLE `pacientes_adicciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pacientes_alergias`
--
ALTER TABLE `pacientes_alergias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pacientes_enfermedades`
--
ALTER TABLE `pacientes_enfermedades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pacientes_vacunas`
--
ALTER TABLE `pacientes_vacunas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sanguineos_tipos`
--
ALTER TABLE `sanguineos_tipos`
  MODIFY `sanguineos_tipos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuarios_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuarios_accesos`
--
ALTER TABLE `usuarios_accesos`
  MODIFY `usuarios_accesos_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuarios_niveles`
--
ALTER TABLE `usuarios_niveles`
  MODIFY `usuarios_niveles_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios_permisos`
--
ALTER TABLE `usuarios_permisos`
  MODIFY `usuarios_permisos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `vacunas`
--
ALTER TABLE `vacunas`
  MODIFY `vacunas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pacientes_adicciones`
--
ALTER TABLE `pacientes_adicciones`
  ADD CONSTRAINT `fk_pacientes_adicciones` FOREIGN KEY (`adicciones_id`) REFERENCES `adicciones` (`adicciones_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
