/*pacientes_adicciones table*/

CREATE TABLE IF NOT EXISTS pacientes_adicciones(
  id                INT NOT NULL,
  pacientes_id      INT(11) NOT NULL,
  adicciones_id     TINYINT(1) NOT NULL,
  observaciones     TEXT DEFAULT NULL,
  borrado           TINYINT(1) DEFAULT 0,
  PRIMARY KEY(id)
) ENGINE=InnoDB;


/*adicciones_pacientes alter*/

ALTER TABLE pacientes_adicciones ADD CONSTRAINT fk_pacientes_adicciones FOREIGN KEY (adicciones_id) REFERENCES adicciones(adicciones_id);