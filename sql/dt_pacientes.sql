ALTER VIEW dt_pacientes AS
SELECT 1 AS consecutivo, p.pacientes_id AS id, p.expediente,
  DATE_FORMAT(p.fecha_ingreso, '%d/%m/%Y') AS fechaIngreso,
  CONCAT(p.nombre, ' ', p.apellido_paterno, ' ', p.apellido_materno) AS nombre,
  DATE_FORMAT(p.fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento, p.sexo,
  p.peso, p.estatura, p.telefono_1 AS telefono, -- p.telefono_2,
  p.celular, p.email, -- p.url_foto, p.curp, p.rfc, p.estados_civiles_id,
  -- ec.nombre AS nombreEstadoCivil, p.ocupaciones_id,
  o.nombre AS nombreOcupacion, st.nombre AS nombreSanguineoTipo,
  p.padecimiento_actual AS padecimientoActual, p.exploracion_fisica AS exploracionFisica,
  p.t_a AS ta, p.f_r AS fr, p.f_c AS fc, p.menarca, p.gesta, p.fum, p.ivsa, p.para,
  p.abortos, p.cesareas, p.poses_sexuales AS pSexuales,
  p.otras_actividades AS otrasActividades, p.observaciones,
  -- p.domicilios_id, d.calle, d.colonia, d.ciudad, d.estado, d.pais, d.codigo_postal,
  p.antecedentes_familiares AS antecedentesFamiliares,
  p.antecedentes_patologicos AS antecedentesPatologicos,
  p.antecedentes_quirurgicos AS antecedentesQuirurgicos,
  p.antecedentes_ginecoobstetricos AS antecedentesGinecoobstetricos,
  -- (SELECT GROUP_CONCAT(CONCAT('<p>', a.nombre, '</p>', '<small>', pa.observaciones, '</small>') SEPARATOR '<br>')
  --   FROM pacientes_alergias pa
  --   INNER JOIN alergias a ON a.alergias_id = pa.alergias_id
  --   WHERE pa.borrado = 0 AND pa.pacientes_id = p.pacientes_id) AS alergias,
  -- (SELECT GROUP_CONCAT(CONCAT('<p>', a.nombre, '</p>', '<small>', pa.observaciones, '</small>') SEPARATOR '<br>')
  --   FROM pacientes_adicciones pa
  --   INNER JOIN adicciones a ON a.adicciones_id = pa.adicciones_id
  --   WHERE pa.borrado = 0 AND pa.pacientes_id = p.pacientes_id) AS adicciones,
  -- (SELECT GROUP_CONCAT(CONCAT('<p>', a.nombre, '</p>', '<small>', pv.observaciones, '</small>') SEPARATOR '<br>')
  --   FROM pacientes_vacunas pv
  --   INNER JOIN vacunas a ON a.vacunas_id = pv.vacunas_id
  --   WHERE pv.borrado = 0 AND pv.pacientes_id = p.pacientes_id) AS vacunas,
  (SELECT GROUP_CONCAT(a.nombre SEPARATOR '<br>')
    FROM pacientes_alergias pa
    INNER JOIN alergias a ON a.alergias_id = pa.alergias_id
    WHERE pa.tiene = 1
      AND pa.borrado = 0
      AND pa.pacientes_id = p.pacientes_id) AS alergias,
  (SELECT GROUP_CONCAT(a.nombre SEPARATOR '<br>')
    FROM pacientes_adicciones pa
    INNER JOIN adicciones a ON a.adicciones_id = pa.adicciones_id
    WHERE pa.tiene = 1
      AND pa.borrado = 0
      AND pa.pacientes_id = p.pacientes_id) AS adicciones,
  (SELECT GROUP_CONCAT(v.nombre SEPARATOR '<br>')
    FROM pacientes_vacunas pv
    INNER JOIN vacunas v ON v.vacunas_id = pv.vacunas_id
    WHERE pv.tiene = 1
      AND pv.borrado = 0
      AND pv.pacientes_id = p.pacientes_id) AS vacunas,
  CONCAT('<ul class="dropdown menu navigation" data-dropdown-menu data-closing-time=50>',
    '<li>',
      '<a href="#acciones">Da clic para...</a>',
      '<ul class="menu">',
        '<li><a href="#editar" data-action="editar-paciente">EDITAR</a></li>'
      '</ul>',
    '</li>'
  '</ul>') AS acciones
  -- p.deporte,
  -- p.duracion,
  -- p.flujo, p.activa, p.partos, p.cantidad_p, p.natural,
  -- p.cantidad_a, p.menopausia, p.info
  -- p.usuarios_id, u.username, p.timestamp, p.borrado
FROM pacientes p
-- LEFT JOIN estados_civiles ec ON p.estados_civiles_id = ec.estados_civiles_id
LEFT JOIN ocupaciones o ON p.ocupaciones_id = o.ocupaciones_id
-- INNER JOIN domicilios d ON p.domicilios_id = d.domicilios_id
LEFT JOIN sanguineos_tipos st ON p.sanguineos_tipos_id = st.sanguineos_tipos_id
-- INNER JOIN usuarios_id u ON p.usuarios_id = u.usuarios_id
WHERE p.borrado = 0
ORDER BY p.pacientes_id;