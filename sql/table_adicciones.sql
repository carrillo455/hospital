/*adicciones table*/

CREATE TABLE IF NOT EXISTS adicciones(
  adicciones_id     TINYINT NOT NULL,
  nombre            VARCHAR(255) DEFAULT NULL,
  borrado           TINYINT(1) DEFAULT 0,
  PRIMARY KEY(adicciones_id)
) ENGINE=InnoDB;

/*adicciones inserts*/

INSERT INTO adicciones(adicciones_id, nombre, borrado)
  VALUES (1, 'Alcoholismo', 0);

INSERT INTO adicciones(adicciones_id, nombre, borrado)
  VALUES (2, 'Tabaquismo', 0);

INSERT INTO adicciones(adicciones_id, nombre, borrado)
  VALUES (3, 'Drogas', 0);