<?php
  session_start();
  if (!isset($_SESSION['usuario'])) {
    header('Location: ../index.php');
  } else {
    $claves = $_SESSION['usuario']['claves'];
    if (!in_array('SUD01', $claves) && !in_array('ADM02', $claves)) {
      header('Location: ../index.php');
    }
  }
?>
<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema Administrativo de Información Médica v1.0.0</title>
    <link rel="icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/foundation.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../css/app.css">
  </head>
  <body>
    <?php require_once 'header.php'; ?>

    <main>
      <div class="medium callout">
        <div class="row">
          <div class="large-12 columns">
            <h4 class="text-center">Bienvenido
              <strong><?php echo $_SESSION['usuario']['username']; ?></strong>
            </h4>
          </div>
        </div>

        <form id="formulario">
          <div class="row">
            <div class="large-12 columns">
              <h3 class="text-center">CONSULTA MÉDICA</h3>
            </div>
          </div>

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="buscar-paciente" class="text-right hide-for-small-only">Buscar Paciente</label>
              <label for="buscar-paciente" class="show-for-small-only">Buscar Paciente</label>
            </div>

            <div class="large-8 medium-8 columns">
              <input id="buscar-paciente" type="text" placeholder="Por Nombre, Apellidos, Num. de Paciente">
              <input id="id-paciente" name="id-paciente" type="hidden">
            </div>

            <div class="large-2 medium-2 columns">
              <a id="nuevo-paciente" class="small expanded button">NUEVO</a>
            </div>
          </div>

          <div class="row">
            <hr>
            <h5 class="text-center">INFORMACIÓN DE LA CONSULTA</h5>
          </div>

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="expediente" class="text-right hide-for-small-only">Expediente</label>
              <label for="expediente" class="show-for-small-only">Expediente</label>
            </div>

            <div class="large-4 medium-4 columns">
              <input id="expediente" type="text" disabled>
              <!-- <input name="expediente" type="hidden"> -->
            </div>

            <div class="large-2 large-offset-2 medium-2 columns">
              <label for="fecha" class="text-right hide-for-small-only">Fecha</label>
              <label for="fecha" class="show-for-small-only">Fecha</label>
            </div>

            <div class="large-2 medium-4 columns">
              <input id="fecha" type="text" disabled>
              <!-- <input name="fecha" type="hidden"> -->
            </div>
          </div>

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="nombre" class="text-right hide-for-small-only">Nombre del Paciente</label>
              <label for="nombre" class="show-for-small-only">Nombre del Paciente</label>
            </div>

            <div class="large-4 medium-4 columns">
              <input id="nombre" name="nombre" type="text" placeholder="Nombre(s)">
            </div>

            <div class="large-3 medium-3 columns">
              <input id="apellido-paterno" name="apellido-paterno" type="text" placeholder="Apellido Paterno">
            </div>

            <div class="large-3 medium-3 columns">
              <input id="apellido-materno" name="apellido-materno" type="text" placeholder="Apellido Materno">
            </div>
          </div>

          <!-- <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="apellido-paterno" class="text-right hide-for-small-only">Apellido Paterno</label>
              <label for="apellido-paterno" class="show-for-small-only">Apellido Paterno</label>
            </div>

            <div class="large-4 medium-4 columns">
              <input id="apellido-paterno" name="apellido-paterno" type="text" placeholder="Apellido Paterno">
            </div>

            <div class="large-2 medium-2 columns">
              <label for="apellido-materno" class="text-right hide-for-small-only">Apellido Materno</label>
              <label for="apellido-materno" class="show-for-small-only">Apellido Materno</label>
            </div>

            <div class="large-4 medium-4 columns">
              <input id="apellido-materno" name="apellido-materno" type="text" placeholder="Apellido Materno">
            </div>
          </div> -->

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="fecha-nacimiento" class="text-right hide-for-small-only">Fecha de Nacimiento</label>
              <label for="fecha-nacimiento" class="show-for-small-only">Fecha de Nacimiento</label>
            </div>

            <div class="large-4 medium-4 columns">
              <input id="fecha-nacimiento" name="fecha-nacimiento" type="text" placeholder="dd/mm/aaaa">
              <span id="fecha-nacimiento-incorrecta" class="form-error">
                Favor de revisar la fecha de nacimiento, el formato adecuado es "<i>dd/mm/aaaa</i>".
              </span>
            </div>

            <div class="large-2 medium-2 columns">
              <label for="edad" class="text-right hide-for-small-only">Edad</label>
              <label for="edad" class="show-for-small-only">Edad</label>
            </div>

            <div class="large-4 medium-4 columns">
              <input id="edad" name="edad" type="text" placeholder="Edad" disabled>
            </div>
          </div>

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="procedimiento" class="text-right hide-for-small-only">Procedimiento</label>
              <label for="procedimiento" class="show-for-small-only">Procedimiento</label>
            </div>

            <div class="large-4 medium-4 columns">
              <input id="procedimiento" name="procedimiento" type="text" data-default
                placeholder="Procedimiento" value="Colposcopía">
            </div>

            <div class="large-2 medium-2 columns">
              <label for="instrumentos" class="text-right hide-for-small-only">Instrumento</label>
              <label for="instrumentos" class="show-for-small-only">Instrumento</label>
            </div>

            <div class="large-4 medium-4 columns">
              <select id="instrumentos" name="instrumento"></select>
            </div>
          </div>

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="motivo-estudio" class="text-right hide-for-small-only">Motivo de Estudio</label>
              <label for="motivo-estudio" class="show-for-small-only">Motivo de Estudio</label>
            </div>

            <div class="large-6 medium-6 columns">
              <input id="motivo-estudio" name="motivo-estudio" type="text" data-default
                placeholder="Motivo de Estudio" value="Control">
            </div>

            <div class="large-2 medium-2 columns">
              <label for="anestesia" class="text-right hide-for-small-only">Anestesia</label>
              <label for="anestesia" class="show-for-small-only">Anestesia</label>
            </div>

            <div class="large-2 medium-2 columns">
              <select id="anestesia" name="anestesia">
                <option value="no">NO</option>
                <option value="si">SI</option>
              </select>
            </div>
          </div>

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="hallazgos" class="text-right hide-for-small-only">Hallazgos</label>
              <label for="hallazgos" class="show-for-small-only">Hallazgos</label>
            </div>

            <div class="large-10 medium-10 columns">
              <textarea id="hallazgos" name="hallazgos" rows="5"></textarea>
            </div>
          </div>

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="impresion-diagnostica" class="text-right hide-for-small-only">Impresión Diagnóstica</label>
              <label for="impresion-diagnostica" class="show-for-small-only">Impresión Diagnóstica</label>
            </div>

            <div class="large-10 medium-10 columns">
              <textarea id="impresion-diagnostica" name="impresion-diagnostica" rows="5"></textarea>
            </div>
          </div>

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="plan" class="text-right hide-for-small-only">Plan</label>
              <label for="plan" class="show-for-small-only">Plan</label>
            </div>

            <div class="large-10 medium-10 columns">
              <textarea id="plan" name="plan" rows="5"></textarea>
            </div>
          </div>

          <div class="row">
            <div class="large-6 medium-6 columns text-center">
              <video id="video"></video>
              <!-- <div class="expanded button-group" style="margin:0;">
                <a id="startbutton" class="medium button">CAPTURAR IMAGEN</a>
                <a id="elegir-imagen-colposcopio" class="file-upload medium button">
                  CARGAR IMAGEN EXTERNA
                  <input id="imagen-colposcopio" type="file" class="file-input" accept=".jpeg, .jpg, .png">
                </a>
              </div> -->
              <a id="startbutton" class="medium expanded button" style="margin: 2px 0;">CAPTURAR IMAGEN</a>
              <a id="borrar-imagenes" class="medium expanded alert button">BORRAR IMÁGENES</a>
              <span id="data-thumbs-vacio" class="form-error">
                * Es necesario capturar por lo menos una fotografía.
              </span>
            </div>

            <div class="large-6 medium-6 columns">
              <div class="row large-up-3 medium-up-2 small-up-3" data-thumbs></div>
            </div>
          </div>

          <div class="row">
            <div class="large-4 large-offset-8 columns end">
              <input id="guardar" type="submit" class="large expanded button float-right" value="GUARDAR">
              <input name="accion" type="hidden" value="guardar-colposcopio">
            </div>
          </div>
        </form>
      </div>
    </main>

    <?php require_once 'footer.php'; ?>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.min.js"></script>
    <script src="../js/vendor/jquery-ui.min.js"></script>
    <script src="../js/vendor/jquery.mask.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/webcam.js"></script>
    <script src="../js/ver-reporte.js"></script>
    <script>
      $(document).ready(function() {
        var datos = [
          {
            id: 'expediente',
            accion: 'obtener-expediente',
            input: 'text'
          },
          {
            id: 'instrumentos',
            accion: 'obtener-instrumentos',
            input: 'select'
          }
        ];
        var datosCargados = 0;
        var limpiarFormulario = function() {
          return $('#formulario')
            .find('.is-invalid-input')
            .removeClass('is-invalid-input')
            .end()
            .find('.form-error')
            .removeClass('is-visible')
            .end()
            .find('[data-thumbs]')
            .empty()
            .end()
            .find('select option:nth-child(1)')
            .prop('selected', true)
            .end()
            .find('[readonly]')
            .removeAttr('readonly')
            .end()
            .find(':input')
            .not('select')
            .not('[data-default]')
            // .not(':disabled')
            .not(':submit')
            .not('[name=accion]')
            .val('');
        };
        var calcularEdad = function(rawDate) {
          var formattedDate = rawDate.split('/').reverse().join('-');
          var birthDate = new Date(formattedDate);
          var diffDate = new Date - birthDate;
          var ageDate = new Date(diffDate);
          ageDate.setHours(ageDate.getHours() - 24); // Hack.. no era exacto.
          var age = Math.abs(ageDate.getUTCFullYear() - 1970);
          if (isNaN(age)) {
            return false;
          }

          return age;
        };
        var pad = function(n, width=3, z=0) {
          return (String(z).repeat(width) + String(n)).slice(String(n).length)
        };
        var obtenerFechaHoy = function() {
          var date = new Date();
          var dateFormatHuman = pad(date.getDate(), 2) + '/' +
            pad((date.getMonth() + 1), 2) + '/' +
            date.getFullYear();

          return dateFormatHuman;
        };

        // Valores iniciales.
        (function() {
          var date = new Date();
          var _pad = function(n, width=3, z=0) {
            return (String(z).repeat(width) + String(n)).slice(String(n).length)
          };
          var dateFormatHuman = _pad(date.getDate(), 2) + '/' +
            _pad((date.getMonth() + 1), 2) + '/' +
            date.getFullYear();
          // var dateFormatSql = date.getFullYear() + '-' +
          //   _pad((date.getMonth() + 1), 2) + '-' +
          //   _pad(date.getDate(), 2);

          $('#fecha').val(dateFormatHuman);
          // $('[name="fecha"]').val(dateFormatSql);
        })();

        // Cargar datos.
        (function() {
          mostrarLoading();

          datos.forEach(function(dato, index) {
            var id = dato.id;
            var accion = dato.accion;
            var input = dato.input;
            var extra = dato.extra;

            $.post('../php/api.php', {
              accion: accion
            }, function(response) {
              if (response.status === 'OK') {
                var data = response.data;

                // Revisar a que tipo de input se le daran los valores.
                switch (input) {
                  case 'text':
                    $('#' + id).val(data);
                    $('[name="' + id + '"]').val(data);
                    if (extra) {
                      $('#' + id).attr('data-' + extra, data);
                    }
                  break;

                  case 'select':
                    for (var i = 0; i < data.length; i++) {
                      $('#' + id).append('<option value=' +
                        data[i].id + '>' +
                        data[i].nombre + '</option>');

                      if (extra) {
                        $('#' + id).find(':last')
                          .attr('data-' + extra, data[i][extra]);
                      }
                    }
                  break;
                }

                // Si tiene un extra, mandar a llamar su evento onchange.
                if (extra) {
                  $('#' + id).trigger('change');
                }

                datosCargados += 1;
              } else {
                mostrarMensaje(response.msg);
              }

              // Ya termino de cargar todos los datos.
              if (datosCargados === datos.length) {
                ocultarLoading();
                $('#formulario').find(':input:first').focus();
              }
            }, 'json').fail(function() {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            });
          });
        })();

        $('#buscar-paciente').autocomplete({
          minLength: 2,
          autoFocus: true,
          source: function (request, response) {
            request.accion = 'obtener-pacientes';
            return $.post("../php/api.php", request, function(_response) {
              return response(_response.data);
            }, 'json');
          },
          change: function( event, ui ) {
            if (ui.item === null) {
            }
          },
          search: function( event, ui ) {
          },
          select: function( event, ui ) {
            $('#id-paciente').val(ui.item.id);
            $('#expediente').val(ui.item.expediente);
            // $('[name="expediente"]').val(ui.item.expediente);
            $('#nombre').val(ui.item.nombre).attr('readonly', true);
            $('#apellido-paterno').val(ui.item.apellidoPaterno).attr('readonly', true);
            $('#apellido-materno').val(ui.item.apellidoMaterno).attr('readonly', true);
            $('#fecha-nacimiento').val(ui.item.fechaNacimiento).attr('readonly', true);
            $('#edad').val(function() {
              var edad = calcularEdad(ui.item.fechaNacimiento);
              if (!edad) {
                return '';
              }

              return edad;
            })

            $('#formulario input[type=text]')
              .not('.ui-autocomplete-input, :disabled, [readonly]')
              .first()
              .focus();
            // $('#procedimiento').val(ui.item.procedimiento);
            // $('#instrumentos').val(ui.item.instrumento);
            // $('#motivo-estudio').val(ui.item.motivoEstudio);
            // $('#anestesia').val(ui.item.anestesia);
            // $('#hallazgos').val(ui.item.hallazgos);
            // $('#impresion-diagnostica').val(ui.item.impresionDiagnostica);
            // $('[data-thumbs]').empty();
            // ui.item.imgs.split(',').forEach(function(src, index) {
            //   var block = document.createElement('div');
            //   var photo = document.createElement('img');
            //   var close = document.createElement('span');

            //   block.className = 'column column-block';
            //   photo.classList.add('thumbnail');
            //   photo.setAttribute('src', src);
            //   close.innerHTML = '&times;';
            //   close.setAttribute('data-close', 'thumbnail');

            //   block.appendChild(photo);
            //   block.appendChild(close);
            //   $('[data-thumbs]').append(block);
            // });
            return true;
          }
        }).autocomplete( "instance" )._renderItem = function( ul, item ) {
          return $( "<li style='border-bottom:1px solid grey'>" )
          .append( "<a>" + item.label + "</a>" )
          .appendTo( ul );
        };

        $('#nuevo-paciente').on('click', function(evt) {
          mostrarLoading();
          limpiarFormulario();
          $('#fecha').val(obtenerFechaHoy);

          return $.post('../php/api.php', {
              accion: 'obtener-expediente'
            }, function(response) {
              if (response.status === 'OK') {
                var data = response.data;
                $('#expediente').val(data);
                // $('[name="expediente"]').val(data);
                ocultarLoading();

                $('#nombre').focus();
              } else {
                mostrarMensaje(response.msg);
              }
            }, 'json');
        });

        $('#borrar-imagenes').on('click', function(evt) {
          return $('[data-thumbs]').empty();
        });

        $('[data-thumbs]').on('click', '[data-close="thumbnail"]', function(evt) {
          return this.parentNode.remove();
        });

        $('#formulario').on('submit', function(evt) {
          // Validación por el nombre vacio.
          if ($('#nombre').val() === '') {
            $('#nombre').focus();
            return evt.preventDefault();
          }

          // Validación por la fecha de nacimiento.
          if ($('#fecha-nacimiento').val() === ''
            || $('#fecha-nacimiento').hasClass('is-invalid-input')
            || $('#fecha-nacimiento-incorrecta').hasClass('is-visible')) {
            $('#fecha-nacimiento').focus();
            return evt.preventDefault();
          }

          // Validación por lo menos una foto.
          if ($('[data-thumbs]').find('.thumbnail').length === 0) {
            $('#startbutton').focus();
            $('#data-thumbs-vacio').addClass('is-visible');
            return evt.preventDefault();
          }

          // Quitamos clases de error.
          $('.is-invalid-input').removeClass('is-invalid-input');
          $('.form-error').removeClass('is-visible');

          mostrarLoading();

          var formData = new FormData(this);
          var thumbnails = $('.thumbnail');
          $.each(thumbnails, function(index, thumbnail) {
            formData.append('imgs[]', thumbnail.src);
          });

          $.ajax({
            url: '../php/api.php',
            method: 'POST',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
              if (response.status === 'OK') {
                $('#nuevo-paciente').trigger('click');
                verReporte(response.data);
              }

              ocultarLoading();
              mostrarMensaje(response.msg);
            },
            error: function() {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            }
          });

          return evt.preventDefault();
        });

        // Evento para deshabilitar el 'enter' para que no se ejecute
        // el submit del formulario.
        $(document).on('keydown', function(evt) {
          if (evt.keyCode === 13 && evt.target.tagName !== 'TEXTAREA') {
            return evt.preventDefault();
          }

          return;
        });
        /*  */

        // Evento para dar focus al cerrar el mensaje.
        $('#mensaje').on('closed.zf.reveal', function(evt) {
          $('#formulario').find(':input:first').focus();
        });

        // Evento para agregar mascara a la fecha de nacimiento.
        $('#fecha-nacimiento').mask('00/00/0000', {
          onComplete: function(cep) {
            var edad = calcularEdad(cep);
            if (!edad) {
              $('#edad').val('');
              return;
            }

            $('#edad').val(edad);
            $('#fecha-nacimiento').removeClass('is-invalid-input');
            $('#fecha-nacimiento-incorrecta').removeClass('is-visible');
          },
          onChange: function(cep) {
            $('#fecha-nacimiento').addClass('is-invalid-input');
            $('#fecha-nacimiento-incorrecta').addClass('is-visible');
          }
        });
      });
    </script>
  </body>
</html>