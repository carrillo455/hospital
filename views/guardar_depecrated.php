<?php
  session_start();
  if (!isset($_SESSION['usuario'])) {
    header('Location: ../index.php');
  } else {
    $claves = $_SESSION['usuario']['claves'];
    if (!in_array('SUD01', $claves) && !in_array('ADM02', $claves)) {
      header('Location: ../index.php');
    }
  }
?>
<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema Administrativo de Información Médica v1.0.0</title>
    <link rel="icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/foundation.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../css/app.css">
  </head>
  <body>
    <?php require_once 'header.php'; ?>

    <main>
      <div class="medium callout">
        <div class="row">
          <div class="medium-12 columns">
            <h4 class="text-center">GUARDAR INFORMACIÓN DEL PACIENTE</h4>
          </div>
        </div>

        <form id="formulario">
          <!-- SECCION: BASE -->
          <div class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">INFORMACIÓN BASE</p>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="fecha-ingreso" class="text-right hide-for-small-only">Fecha Ingreso</label>
                <label for="fecha-ingreso" class="show-for-small-only">Fecha Ingreso</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="fecha-ingreso" name="fecha-ingreso" type="date">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="nombre" class="text-right hide-for-small-only">Nombre</label>
                <label for="nombre" class="show-for-small-only">Nombre</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="nombre" name="nombre" type="text" placeholder="Nombre del Paciente">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="apellido-paterno" class="text-right hide-for-small-only">Apellido Paterno</label>
                <label for="apellido-paterno" class="show-for-small-only">Apellido Paterno</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="apellido-paterno" name="apellido-paterno" type="text"
                  placeholder="Aplleido Paterno del Paciente">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="apellido-materno" class="text-right hide-for-small-only">Apellido Materno</label>
                <label for="apellido-materno" class="show-for-small-only">Apellido Materno</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="apellido-materno" name="apellido-materno" type="text"
                  placeholder="Apellido Materno del Paciente">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="fecha-nacimiento" class="text-right hide-for-small-only">Fecha de Nacimiento</label>
                <label for="fecha-nacimiento" class="show-for-small-only">Fecha de Nacimiento</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="fecha-nacimiento" name="fecha-nacimiento" type="date">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="sexo" class="text-right hide-for-small-only">Sexo</label>
                <label for="sexo" class="show-for-small-only">Sexo</label>
              </div>

              <div class="large-4 medium-4 columns">
                <select id="sexo" name="sexo">
                  <option value="masculino">Masculino</option>
                  <option value="femenino">Femenino</option>
                </select>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="peso" class="text-right hide-for-small-only">Peso</label>
                <label for="peso" class="show-for-small-only">Peso</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="peso" name="peso" type="text" placeholder="0.00">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="estatura" class="text-right hide-for-small-only">Estatura</label>
                <label for="estatura" class="show-for-small-only">Estatura</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="estatura" name="estatura" type="text" placeholder="0.00">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="telefono-1" class="text-right hide-for-small-only">Teléfono 1</label>
                <label for="telefono-1" class="show-for-small-only">Teléfono 1</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="telefono-1" name="telefono-1" type="text" placeholder="">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="telefono-2" class="text-right hide-for-small-only">Teléfono 2</label>
                <label for="telefono-2" class="show-for-small-only">Teléfono 2</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="telefono-2" name="telefono-2" type="text" placeholder="">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="celular" class="text-right hide-for-small-only">Celular</label>
                <label for="celular" class="show-for-small-only">Celular</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="celular" name="celular" type="text" placeholder="">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="email" class="text-right hide-for-small-only">Email</label>
                <label for="email" class="show-for-small-only">Email</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="email" name="email" type="text" placeholder="">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="fotografia" class="text-right hide-for-small-only">Fotografía</label>
                <label for="fotografia" class="show-for-small-only">Fotografía</label>
              </div>

              <div class="large-4 medium-4 columns">
                <img src="" alt="Fotografía del Paciente" width="100%">
              </div>

              <div class="large-6 medium-6 columns">
                <a id="elegir-fotografia" type="button" class="file-upload button expanded">
                  ELEGIR FOTOGRAFIA
                  <input id="fotografia" type="file" class="file-input" accept=".jpeg, .jpg, .png">
                </a>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="curp" class="text-right hide-for-small-only">CURP</label>
                <label for="curp" class="show-for-small-only">CURP</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="curp" name="curp" type="text" placeholder="">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="rfc" class="text-right hide-for-small-only">RFC</label>
                <label for="rfc" class="show-for-small-only">RFC</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="rfc" name="rfc" type="text" placeholder="">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="nombre-ocupacion" class="text-right hide-for-small-only">Ocupación</label>
                <label for="nombre-ocupacion" class="show-for-small-only">Ocupación</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="nombre-cupacion" name="nombre-ocupacion" type="text">
                <input name="id-ocupacion" type="hidden">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="calle" class="text-right hide-for-small-only">Calle</label>
                <label for="calle" class="show-for-small-only">Calle</label>
              </div>

              <div class="large-4 medium-10 columns">
                <input id="calle" name="calle" type="text" placeholder="Calle">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="colonia" class="text-right hide-for-small-only">Colonia</label>
                <label for="colonia" class="show-for-small-only">Colonia</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="colonia" name="colonia" type="text" placeholder="Colonia">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="ciudad" class="text-right hide-for-small-only">Ciudad</label>
                <label for="ciudad" class="show-for-small-only">Ciudad</label>
              </div>

              <div class="large-4 medium-10 columns">
                <input id="ciudad" name="ciudad" type="text" placeholder="Ciudad">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="estado" class="text-right hide-for-small-only">Estado</label>
                <label for="estado" class="show-for-small-only">Estado</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="estado" name="estado" type="text" placeholder="Estado">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="pais" class="text-right hide-for-small-only">País</label>
                <label for="pais" class="show-for-small-only">País</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="pais" name="pais" type="text" placeholder="País">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="codigo-postal" class="text-right hide-for-small-only">Código Postal</label>
                <label for="codigo-postal" class="show-for-small-only">Código Postal</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="codigo-postal" name="codigo-postal" type="text" placeholder="Código Postal">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="enf-padre" class="text-right hide-for-small-only">Enf. Padre</label>
                <label for="enf-padre" class="show-for-small-only">Enf. Padre</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="enf-padre" name="enf-padre" rows="3"></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="enf-madre" class="text-right hide-for-small-only">Enf. Madre</label>
                <label for="enf-madre" class="show-for-small-only">Enf. Madre</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="enf-madre" name="enf-madre" rows="3"></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="enf-hermanos" class="text-right hide-for-small-only">Enf. Hermanos</label>
                <label for="enf-hermanos" class="show-for-small-only">Enf. Hermanos</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="enf-hermanos" name="enf-hermanos" rows="3"></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="deporte" class="text-right hide-for-small-only">Deporte</label>
                <label for="deporte" class="show-for-small-only">Deporte</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="deporte" name="deporte" type="text">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="otras-actividades" class="text-right hide-for-small-only">Otras Actividades</label>
                <label for="otras-actividades" class="show-for-small-only">Otras Actividades</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="otras-actividades" name="otras-actividades" rows="3"></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="tipo-sanguineo" class="text-right hide-for-small-only">Tipo Sanguíneo</label>
                <label for="tipo-sanguineo" class="show-for-small-only">Tipo Sanguíneo</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="tipo-sanguineo" name="tipo-sanguineo" type="text">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="observaciones" class="text-right hide-for-small-only">Observaciones</label>
                <label for="observaciones" class="show-for-small-only">Observaciones</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="observaciones" name="observaciones" rows="3"></textarea>
              </div>
            </div>
          </div>

          <div class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">ADICCIONES</p>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="adiccion-alcoholismo" class="text-right hide-for-small-only">¿Padece de Alcoholismo?</label>
                <label for="adiccion-alcoholismo" class="show-for-small-only">¿Padece de Alcoholismo?</label>
              </div>

              <div class="large-2 medium-2 columns">
                <select id="adiccion-alcoholismo" name="adiccion-alcoholismo">
                  <option value="no">NO</option>
                  <option value="si">SI</option>
                </select>
              </div>

              <div class="large-2 medium-2 columns">
                <label for="observaciones-alcoholismo" class="text-right hide-for-small-only">Observaciones (Alcoholismo)</label>
                <label for="observaciones-alcoholismo" class="show-for-small-only">Observaciones (Alcoholismo)</label>
              </div>

              <div class="large-6 medium-6 columns">
                <textarea id="observaciones-alcoholismo" name="observaciones-alcoholismo" rows="3"></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="adiccion-tabaquismo" class="text-right hide-for-small-only">¿Padece de Tabaquismo?</label>
                <label for="adiccion-tabaquismo" class="show-for-small-only">¿Padece de Tabaquismo?</label>
              </div>

              <div class="large-2 medium-2 columns">
                <select id="adiccion-tabaquismo" name="adiccion-tabaquismo">
                  <option value="no">NO</option>
                  <option value="si">SI</option>
                </select>
              </div>

              <div class="large-2 medium-2 columns">
                <label for="observaciones-tabaquismo" class="text-right hide-for-small-only">Observaciones (Tabaquismo)</label>
                <label for="observaciones-tabaquismo" class="show-for-small-only">Observaciones (Tabaquismo)</label>
              </div>

              <div class="large-6 medium-6 columns">
                <textarea id="observaciones-tabaquismo" name="observaciones-tabaquismo" rows="3"></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="padece-drogas" class="text-right hide-for-small-only">¿Consume Drogas?</label>
                <label for="padece-drogas" class="show-for-small-only">¿Consume Drogas?</label>
              </div>

              <div class="large-2 medium-2 columns">
                <select id="padece-drogas" name="padece-drogas">
                  <option value="no">NO</option>
                  <option value="si">SI</option>
                </select>
              </div>

              <div class="large-2 medium-2 columns">
                <label for="observaciones-drogas" class="text-right hide-for-small-only">Observaciones (Drogas)</label>
                <label for="observaciones-drogas" class="show-for-small-only">Observaciones (Drogas)</label>
              </div>

              <div class="large-6 medium-6 columns">
                <textarea id="observaciones-drogas" name="observaciones-drogas" rows="3"></textarea>
              </div>
            </div>
          </div>

          <div class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">VACUNAS</p>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="vacuna-poliomielitis" class="text-right hide-for-small-only">Vacuna Poliomielitis</label>
                <label for="vacuna-poliomielitis" class="show-for-small-only">Vacuna Poliomielitis</label>
              </div>

              <div class="large-4 medium-4 columns">
                <select id="vacuna-poliomielitis" name="vacuna-poliomielitis">
                  <option value="no">NO</option>
                  <option value="si">SI</option>
                </select>
              </div>

              <div class="large-2 medium-2 columns">
                <label for="vacuna-tetanos" class="text-right hide-for-small-only">Vacuna Tetanos</label>
                <label for="vacuna-tetanos" class="show-for-small-only">Vacuna Tetanos</label>
              </div>

              <div class="large-4 medium-4 columns">
                <select id="vacuna-tetanos" name="vacuna-tetanos">
                  <option value="no">NO</option>
                  <option value="si">SI</option>
                </select>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="vacuna-sarampion" class="text-right hide-for-small-only">Vacuna Sarampion</label>
                <label for="vacuna-sarampion" class="show-for-small-only">Vacuna Sarampion</label>
              </div>

              <div class="large-4 medium-4 columns">
                <select id="vacuna-sarampion" name="vacuna-sarampion">
                  <option value="no">NO</option>
                  <option value="si">SI</option>
                </select>
              </div>

              <div class="large-2 medium-2 columns">
                <label for="vacuna-difteria" class="text-right hide-for-small-only">Vacuna Difteria</label>
                <label for="vacuna-difteria" class="show-for-small-only">Vacuna Difteria</label>
              </div>

              <div class="large-4 medium-4 columns">
                <select id="vacuna-difteria" name="vacuna-difteria">
                  <option value="no">NO</option>
                  <option value="si">SI</option>
                </select>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="vacuna-tosferina" class="text-right hide-for-small-only">Vacuna Tosferina</label>
                <label for="vacuna-tosferina" class="show-for-small-only">Vacuna Tosferina</label>
              </div>

              <div class="large-4 medium-4 columns">
                <select id="vacuna-tosferina" name="vacuna-tosferina">
                  <option value="no">NO</option>
                  <option value="si">SI</option>
                </select>
              </div>

              <div class="large-2 medium-2 columns">
                <label for="vacuna-tuberculosis" class="text-right hide-for-small-only">Vacuna Tuberculosis</label>
                <label for="vacuna-tuberculosis" class="show-for-small-only">Vacuna Tuberculosis</label>
              </div>

              <div class="large-4 medium-4 columns">
                <select id="vacuna-tuberculosis" name="vacuna-tuberculosis">
                  <option value="no">NO</option>
                  <option value="si">SI</option>
                </select>
              </div>
            </div>
          </div>

          <div class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">IMAGENES DEL COLPOSCOPIO</p>
                <button class="medium button float-right" onclick="return false;">DESCARGAR IMAGENES</button>
                <a id="elegir-imagen-colposopio" type="button" class="file-upload button float-left">
                  ELEGIR IMAGEN PARA SUBIR
                  <input id="imagen-colposopio" type="file" class="file-input" accept=".jpeg, .jpg, .png">
                </a>
              </div>
            </div>

            <div class="row small-up-2 medium-up-3 large-up-4">
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="large-12 columns end">
              <input id="guardar" type="submit" class="large button float-right" value="GUARDAR">
              <input name="accion" type="hidden" value="guardar-paciente">
            </div>
          </div>
        </form>
      </div>
    </main>

    <?php require_once 'footer.php'; ?>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.min.js"></script>
    <script src="../js/vendor/jquery-ui.min.js"></script>
    <script src="../js/app.js"></script>
    <script>
      $(document).ready(function() {
        var datos = [
          {
            id: 'tal1',
            accion: 'obtener-tal1'
          },
          {
            id: 'tal2',
            accion: 'obtener-tal2'
          },
          {
            id: 'tal3',
            accion: 'obtener-tal3',
            extra: 'otro'
          }
        ];
        var datosCargados = 0;

        // Cargar datos.
        (function() {
          mostrarLoading();
          datos.forEach(function(dato, index) {
            var id = dato.id;
            var extra = dato.extra;

            $.post('../php/api.php', {
              accion: dato.accion
            }, function(response) {
              if (response.status === 'OK') {
                for (var i = 0; i < response.data.length; i++) {
                  $('#' + id).append('<option value=' +
                    response.data[i].id + '>' +
                    response.data[i].nombre + '</option>');

                  if (extra) {
                    $('#' + id).find(':last')
                      .attr('data-' + extra, response.data[i][extra]);
                  }
                }

                // Si tiene un extra, mandar a llamar su evento onchange.
                if (extra) {
                  $('#' + id).trigger('change');
                }

                datosCargados += 1;
              } else {
                mostrarMensaje(response.msg);
              }

              // Ya termino de cargar todos los datos.
              if (datosCargados === datos.length) {
                ocultarLoading();

                // Revisar si mandaron datos.
                (function() {
                  var data = <?php echo isset($_POST['data'])
                    ? $_POST['data'] : 0; echo ';' ?>

                  if (data) {
                    switch (data.action) {
                      case 'agregar':
                      break;

                      case 'editar':
                        mostrarLoading();
                        $.post('../php/api.php', {
                          id: data.id,
                          accion: 'obtener-paciente'
                        }, function(response) {
                          if (response.status === 'OK') {

                            $('#formulario').append('<input type=hidden name=id value=' +
                              data.id + '>');
                          }

                          ocultarLoading();
                        }, 'json').fail(function() {
                          mostrarMensaje('Falló la conexión al servidor,' +
                            ' por favor vuelve a intentarlo.');
                        });
                      break;
                    }
                  }
                })();
              }
            }, 'json').fail(function() {
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            });
          });
        })();

        $('#formulario').on('submit', function(evt) {
          mostrarLoading();

          var data = $(this).serialize();

          $.post('../php/api.php', data, function(response) {
            ocultarLoading();
            mostrarMensaje(response.msg);
          }, 'json').fail(function() {
            mostrarMensaje('Falló la conexión al servidor,' +
              ' por favor vuelve a intentarlo.');
          });

          return evt.preventDefault();
        });

        // Evento para deshabilitar el 'enter' para que no se ejecute
        // el submit del formulario.
        $(document).on('keydown', function(evt) {
          if (evt.keyCode === 13) {
            return evt.preventDefault();
          }
        });
        /*  */

        // Trigger de eventos iniciales.
        $('#formulario').find(':input:first').focus();
      });
    </script>
  </body>
</html>