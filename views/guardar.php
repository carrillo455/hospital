<?php
  session_start();
  if (!isset($_SESSION['usuario'])) {
    header('Location: ../index.php');
  } else {
    $claves = $_SESSION['usuario']['claves'];
    if (!in_array('SUD01', $claves) && !in_array('ADM02', $claves)) {
      header('Location: ../index.php');
    }
  }
?>
<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema Administrativo de Información Médica v1.0.0</title>
    <link rel="icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/foundation.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../css/app.css">
  </head>
  <body>
    <?php require_once 'header.php'; ?>

    <main>
      <div class="medium callout">
        <div class="row">
          <div class="medium-12 columns">
            <h4 class="text-center">GUARDAR INFORMACIÓN DEL PACIENTE</h4>
          </div>
        </div>

        <form id="formulario">
          <!-- SECCION: BASE -->
          <div class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">INFORMACIÓN BASE</p>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="expediente" class="text-right hide-for-small-only">Expediente</label>
                <label for="expediente" class="show-for-small-only">Expediente</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="expediente" type="text" disabled>
              </div>

              <div class="large-2 large-offset-2 medium-2 columns">
                <label for="fecha-ingreso" class="text-right hide-for-small-only">Fecha de Ingreso</label>
                <label for="fecha-ingreso" class="show-for-small-only">Fecha de Ingreso</label>
              </div>

              <div class="large-2 medium-4 columns">
                <input name="fecha-ingreso" id="fecha-ingreso" type="text" placeholder="dd/mm/aaaa" data-date>
                <span id="fecha-ingreso-incorrecta" class="form-error">
                  Favor de revisar la fecha de ingreso, el formato adecuado es "<i>dd/mm/aaaa</i>".
                </span>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="nombre" class="text-right hide-for-small-only">Nombre del Paciente</label>
                <label for="nombre" class="show-for-small-only">Nombre del Paciente</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="nombre" name="nombre" type="text" placeholder="Nombre">
              </div>

              <div class="large-3 medium-3 columns">
                <input id="apellido-paterno" name="apellido-paterno" type="text"
                  placeholder="Apellido Paterno">
              </div>

              <div class="large-3 medium-3 columns">
                <input id="apellido-materno" name="apellido-materno" type="text"
                  placeholder="Apellido Materno">
              </div>
            </div>

            <!-- <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="apellido-paterno" class="text-right hide-for-small-only">Apellido Paterno</label>
                <label for="apellido-paterno" class="show-for-small-only">Apellido Paterno</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="apellido-paterno" name="apellido-paterno" type="text"
                  placeholder="Apellido Paterno del Paciente">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="apellido-materno" class="text-right hide-for-small-only">Apellido Materno</label>
                <label for="apellido-materno" class="show-for-small-only">Apellido Materno</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="apellido-materno" name="apellido-materno" type="text"
                  placeholder="Apellido Materno del Paciente">
              </div>
            </div> -->

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="fecha-nacimiento" class="text-right hide-for-small-only">Fecha de Nacimiento</label>
                <label for="fecha-nacimiento" class="show-for-small-only">Fecha de Nacimiento</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="fecha-nacimiento" name="fecha-nacimiento" type="text" placeholder="dd/mm/aaaa" data-date>
                <span id="fecha-nacimiento-incorrecta" class="form-error">
                  Favor de revisar la fecha de nacimiento, el formato adecuado es "<i>dd/mm/aaaa</i>".
                </span>
              </div>

              <div class="large-2 medium-2 columns">
                <label for="edad" class="text-right hide-for-small-only">Edad</label>
                <label for="edad" class="show-for-small-only">Edad</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="edad" name="edad" type="text" placeholder="Edad" disabled>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="sexo" class="text-right hide-for-small-only">Sexo</label>
                <label for="sexo" class="show-for-small-only">Sexo</label>
              </div>

              <div class="large-10 medium-10 columns">
                <select id="sexo" name="sexo" data-default>
                  <option value="masculino">Masculino</option>
                  <option value="femenino" selected>Femenino</option>
                </select>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="peso" class="text-right hide-for-small-only">Peso (kg)</label>
                <label for="peso" class="show-for-small-only">Peso (kg)</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="peso" name="peso" type="text" placeholder="0.00" data-float="999.99">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="estatura" class="text-right hide-for-small-only">Estatura (metros)</label>
                <label for="estatura" class="show-for-small-only">Estatura (metros)</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="estatura" name="estatura" type="text" placeholder="0.00" data-float="9.99">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="telefono" class="text-right hide-for-small-only">Teléfono</label>
                <label for="telefono" class="show-for-small-only">Teléfono</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="telefono" name="telefono" type="text" placeholder="0123456789" data-tel>
              </div>

              <div class="large-2 medium-2 columns">
                <label for="celular" class="text-right hide-for-small-only">Celular</label>
                <label for="celular" class="show-for-small-only">Celular</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="celular" name="celular" type="text" placeholder="0123456789" data-tel>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="email" class="text-right hide-for-small-only">Email</label>
                <label for="email" class="show-for-small-only">Email</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="email" name="email" type="text" placeholder="paciente@email.com">
              </div>
            </div>

            <!-- <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="fotografia" class="text-right hide-for-small-only">Fotografía</label>
                <label for="fotografia" class="show-for-small-only">Fotografía</label>
              </div>

              <div class="large-4 medium-4 columns">
                <img src="" alt="Fotografía del Paciente" width="100%">
              </div>

              <div class="large-6 medium-6 columns">
                <a id="elegir-fotografia" type="button" class="file-upload button expanded">
                  ELEGIR FOTOGRAFIA
                  <input id="fotografia" type="file" class="file-input" accept=".jpeg, .jpg, .png">
                </a>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="curp" class="text-right hide-for-small-only">CURP</label>
                <label for="curp" class="show-for-small-only">CURP</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="curp" name="curp" type="text" placeholder="">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="rfc" class="text-right hide-for-small-only">RFC</label>
                <label for="rfc" class="show-for-small-only">RFC</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="rfc" name="rfc" type="text" placeholder="">
              </div>
            </div> -->

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="nombre-ocupacion" class="text-right hide-for-small-only">Ocupación</label>
                <label for="nombre-ocupacion" class="show-for-small-only">Ocupación</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="nombre-ocupacion" name="nombre-ocupacion" type="text" placeholder="Ocupacion">
                <input name="id-ocupacion" type="hidden">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="tipos-sanguineos" class="text-right hide-for-small-only">Tipo Sanguíneo</label>
                <label for="tipos-sanguineos" class="show-for-small-only">Tipo Sanguíneo</label>
              </div>

              <div class="large-10 medium-10 columns">
                <select id="tipos-sanguineos" name="tipo-sanguineo"></select>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="padecimiento-actual" class="text-right hide-for-small-only">Padecimiento Actual</label>
                <label for="padecimiento-actual" class="show-for-small-only">Padecimiento Actual</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="padecimiento-actual" name="padecimiento-actual" type="text" placeholder="Padecimiento Actual">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="exploracion-fisica" class="text-right hide-for-small-only">Exploración Física</label>
                <label for="exploracion-fisica" class="show-for-small-only">Exploración Física</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="exploracion-fisica" name="exploracion-fisica" type="text" placeholder="Exploración Física">
              </div>
            </div>

            <div class="row">
              <div class="large-1 medium-1 columns">
                <label for="t-a" class="text-right hide-for-small-only">T.A.</label>
                <label for="t-a" class="show-for-small-only">T.A.</label>
              </div>

              <div class="large-3 medium-3 columns">
                <input id="t-a" name="t-a" type="text" placeholder="T.A.">
              </div>

              <div class="large-1 medium-1 columns">
                <label for="f-r" class="text-right hide-for-small-only">F.R.</label>
                <label for="f-r" class="show-for-small-only">F.R.</label>
              </div>

              <div class="large-3 medium-3 columns">
                <input id="f-r" name="f-r" type="text" placeholder="F.R.">
              </div>

              <div class="large-1 medium-1 columns">
                <label for="f-c" class="text-right hide-for-small-only">F.C.</label>
                <label for="f-c" class="show-for-small-only">F.C.</label>
              </div>

              <div class="large-3 medium-3 columns">
                <input id="f-c" name="f-c" type="text" placeholder="F.C.">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="menarca" class="text-right hide-for-small-only">Menarca</label>
                <label for="menarca" class="show-for-small-only">Menarca</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="menarca" name="menarca" type="text" placeholder="Menarca">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="gesta" class="text-right hide-for-small-only">Gesta</label>
                <label for="gesta" class="show-for-small-only">Gesta</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="gesta" name="gesta" type="text" placeholder="Gesta">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="fum" class="text-right hide-for-small-only">Fum</label>
                <label for="fum" class="show-for-small-only">Fum</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="fum" name="fum" type="text" placeholder="Fum">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="ivsa" class="text-right hide-for-small-only">IVSA</label>
                <label for="ivsa" class="show-for-small-only">IVSA</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="ivsa" name="ivsa" type="text" placeholder="IVSA">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="para" class="text-right hide-for-small-only">Para</label>
                <label for="para" class="show-for-small-only">Para</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="para" name="para" type="text" placeholder="Para">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="abortos" class="text-right hide-for-small-only">Abortos</label>
                <label for="abortos" class="show-for-small-only">Abortos</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="abortos" name="abortos" type="text" placeholder="0" data-num>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="cesareas" class="text-right hide-for-small-only">Cesáreas</label>
                <label for="cesareas" class="show-for-small-only">Cesáreas</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="cesareas" name="cesareas" type="text" placeholder="0" data-num>
              </div>

              <div class="large-2 medium-2 columns">
                <label for="p-sexuales" class="text-right hide-for-small-only">P. Sexuales</label>
                <label for="p-sexuales" class="show-for-small-only">P. Sexuales</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="p-sexuales" name="p-sexuales" type="text" placeholder="P. Sexuales">
              </div>
            </div>

            <!-- <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="calle" class="text-right hide-for-small-only">Calle</label>
                <label for="calle" class="show-for-small-only">Calle</label>
              </div>

              <div class="large-4 medium-10 columns">
                <input id="calle" name="calle" type="text" placeholder="Calle">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="colonia" class="text-right hide-for-small-only">Colonia</label>
                <label for="colonia" class="show-for-small-only">Colonia</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="colonia" name="colonia" type="text" placeholder="Colonia">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="ciudad" class="text-right hide-for-small-only">Ciudad</label>
                <label for="ciudad" class="show-for-small-only">Ciudad</label>
              </div>

              <div class="large-4 medium-10 columns">
                <input id="ciudad" name="ciudad" type="text" placeholder="Ciudad">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="estado" class="text-right hide-for-small-only">Estado</label>
                <label for="estado" class="show-for-small-only">Estado</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="estado" name="estado" type="text" placeholder="Estado">
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="pais" class="text-right hide-for-small-only">País</label>
                <label for="pais" class="show-for-small-only">País</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="pais" name="pais" type="text" placeholder="País">
              </div>

              <div class="large-2 medium-2 columns">
                <label for="codigo-postal" class="text-right hide-for-small-only">Código Postal</label>
                <label for="codigo-postal" class="show-for-small-only">Código Postal</label>
              </div>

              <div class="large-4 medium-4 columns">
                <input id="codigo-postal" name="codigo-postal" type="text" placeholder="Código Postal">
              </div>
            </div> -->

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="otras-actividades" class="text-right hide-for-small-only">Otras Actividades</label>
                <label for="otras-actividades" class="show-for-small-only">Otras Actividades</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="otras-actividades" name="otras-actividades" rows="3" placeholder="Actividades, Deportes, etc..."></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="observaciones" class="text-right hide-for-small-only">Observaciones</label>
                <label for="observaciones" class="show-for-small-only">Observaciones</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="observaciones" name="observaciones" rows="3" placeholder="Observaciones..."></textarea>
              </div>
            </div>
          </div>

          <!-- ANTECEDENTES -->
          <div class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">ANTECEDENTES</p>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="antecedentes-familiares" class="text-right hide-for-small-only">Antecedentes Familiares</label>
                <label for="antecedentes-familiares" class="show-for-small-only">Antecedentes Familiares</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="antecedentes-familiares" name="antecedentes-familiares" rows="3"></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="antecedentes-patologicos" class="text-right hide-for-small-only">Antecedentes Patológicos</label>
                <label for="antecedentes-patologicos" class="show-for-small-only">Antecedentes Patológicos</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="antecedentes-patologicos" name="antecedentes-patologicos" rows="3"></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="antecedentes-quirurgicos" class="text-right hide-for-small-only">Antecedentes Quirúrgicos</label>
                <label for="antecedentes-quirurgicos" class="show-for-small-only">Antecedentes Quirúrgicos</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="antecedentes-quirurgicos" name="antecedentes-quirurgicos" rows="3"></textarea>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="antecedentes-ginecoobstetricos" class="text-right hide-for-small-only">Antecedentes Gineco Obstétricos</label>
                <label for="antecedentes-ginecoobstetricos" class="show-for-small-only">Antecedentes Gineco Obstétricos</label>
              </div>

              <div class="large-10 medium-10 columns">
                <textarea id="antecedentes-ginecoobstetricos" name="antecedentes-ginecoobstetricos" rows="3"></textarea>
              </div>
            </div>
          </div>

          <!-- ALERGIAS -->
          <div id="alergias" class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">ALERGIAS</p>
              </div>
            </div>
          </div>

          <!-- ADICCIONES -->
          <div id="adicciones" class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">ADICCIONES</p>
              </div>
            </div>
          </div>

          <!-- VACUNAS -->
          <div id="vacunas" class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">VACUNAS</p>
              </div>
            </div>
          </div>

          <!-- div class="medium callout">
            <div class="row">
              <div class="large-12 columns">
                <p class="lead text-center">IMAGENES DEL COLPOSCOPIO</p>
                <button class="medium button float-right" onclick="return false;">DESCARGAR IMAGENES</button>
                <a id="elegir-imagen-colposopio" type="button" class="file-upload button float-left">
                  ELEGIR IMAGEN PARA SUBIR
                  <input id="imagen-colposopio" type="file" class="file-input" accept=".jpeg, .jpg, .png">
                </a>
              </div>
            </div>

            <div class="row small-up-2 medium-up-3 large-up-4">
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
              <div class="column column-block">
                <img src="../img/600x600.png" class="thumbnail" alt="">
              </div>
            </div>
          </div> -->

          <div class="row">
            <div class="large-4 large-offset-8 columns end">
              <input id="guardar" type="submit" class="large expanded button float-right" value="GUARDAR">
              <input name="accion" type="hidden" value="guardar-paciente">
            </div>
          </div>
        </form>
      </div>
    </main>

    <?php require_once 'footer.php'; ?>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.min.js"></script>
    <script src="../js/vendor/jquery-ui.min.js"></script>
    <script src="../js/vendor/jquery.mask.min.js"></script>
    <script src="../js/app.js"></script>
    <script>
      $(document).ready(function() {
        var datos = [
          {
            id: 'expediente',
            accion: 'obtener-expediente',
            input: 'text'
          },
          {
            id: 'tipos-sanguineos',
            accion: 'obtener-sanguineos-tipos',
            input: 'select'
          },
          {
            id: 'alergias',
            accion: 'obtener-alergias',
            input: 'custom-1'
          },
          {
            id: 'adicciones',
            accion: 'obtener-adicciones',
            input: 'custom-1'
          },
          {
            id: 'vacunas',
            accion: 'obtener-vacunas',
            input: 'custom-1'
          }
        ];
        var datosCargados = 0;
        var calcularEdad = function(rawDate) {
          var formattedDate = rawDate.split('/').reverse().join('-');
          var birthDate = new Date(formattedDate);
          var diffDate = new Date - birthDate;
          var ageDate = new Date(diffDate);
          ageDate.setHours(ageDate.getHours() - 24); // Hack.. no era exacto.
          var age = Math.abs(ageDate.getUTCFullYear() - 1970);
          if (isNaN(age)) {
            return false;
          }

          return age;
        };
        var pad = function(n, width=3, z=0) {
          return (String(z).repeat(width) + String(n)).slice(String(n).length)
        };
        var obtenerFechaHoy = function() {
          var date = new Date();
          var dateFormatHuman = pad(date.getDate(), 2) + '/' +
            pad((date.getMonth() + 1), 2) + '/' +
            date.getFullYear();

          return dateFormatHuman;
        };
        var limpiarFormulario = function() {
          return $('#formulario')
            .find('select:not([data-default]) option:nth-child(1)')
            .prop('selected', true)
            .end()
            .find('[readonly]')
            .removeAttr('readonly')
            .end()
            .find(':input')
            .not('select')
            // .not(':disabled')
            .not(':submit')
            .not('[name=accion]')
            .val('');
        };
        var refresh = function() {
          mostrarLoading();
          limpiarFormulario();
          $('#fecha-ingreso').val(obtenerFechaHoy);

          return $.post('../php/api.php', {
              accion: 'obtener-expediente'
            }, function(response) {
              if (response.status === 'OK') {
                var data = response.data;
                $('#expediente').val(data);
                // $('[name="expediente"]').val(data);
                ocultarLoading();

                $('#nombre').focus();
              } else {
                mostrarMensaje(response.msg);
              }
            }, 'json');
        };

        // Valores iniciales.
        (function() {
          var date = new Date();
          var _pad = function(n, width=3, z=0) {
            return (String(z).repeat(width) + String(n)).slice(String(n).length)
          };
          var dateFormatHuman = _pad(date.getDate(), 2) + '/' +
            _pad((date.getMonth() + 1), 2) + '/' +
            date.getFullYear();
          // var dateFormatSql = date.getFullYear() + '-' +
          //   _pad((date.getMonth() + 1), 2) + '-' +
          //   _pad(date.getDate(), 2);

          $('#fecha-ingreso').val(dateFormatHuman);
          // $('[name="fecha"]').val(dateFormatSql);
        })();

        // Cargar datos.
        (function() {
          mostrarLoading();

          datos.forEach(function(dato, index) {
            var id = dato.id;
            var accion = dato.accion;
            var input = dato.input;
            var _for = dato.for;
            var extra = dato.extra;

            $.post('../php/api.php', {
              accion: accion
            }, function(response) {
              if (response.status === 'OK') {
                var data = response.data;

                // Revisar a que tipo de input se le daran los valores.
                switch (input) {
                  case 'text':
                    $('#' + id).val(data);
                    $('[name="' + id + '"]').val(data);
                    if (extra) {
                      $('#' + id).attr('data-' + extra, data);
                    }
                  break;

                  case 'select':
                    for (var i = 0; i < data.length; i++) {
                      $('#' + id).append('<option value=' +
                        data[i].id + '>' +
                        data[i].nombre + '</option>');

                      if (extra) {
                        $('#' + id).find(':last')
                          .attr('data-' + extra, data[i][extra]);
                      }
                    }
                  break;

                  case 'custom-1':
                    data.forEach(function(d, i) {
                      $('#' + id).append('<div class="row">' +
                        '<div class="large-2 medium-2 columns">' +
                          '<label class="text-right hide-for-small-only">' +
                            d.nombre +
                          '</label>' +
                          '<label class="show-for-small-only">' +
                            d.nombre +
                          '</label>' +
                        '</div>' +

                        '<div class="large-2 medium-2 columns">' +
                          '<select name="' + id + '-tiene[]">' +
                            '<option value="no">NO</option>' +
                            '<option value="si">SI</option>' +
                          '</select>' +
                          '<input name="' + id + '-ids[]" type="hidden"' +
                            ' value="' + d.id + '">' +
                        '</div>' +

                        '<div class="large-2 medium-2 columns">' +
                          '<label class="text-right hide-for-small-only">' +
                            'Observaciones ' + d.nombre +
                          '</label>'+
                          '<label class="show-for-small-only">' +
                            'Observaciones ' + d.nombre +
                          '</label>' +
                        '</div>' +

                        '<div class="large-6 medium-6 columns">' +
                          '<textarea name="' + id + '-observaciones[]" rows="3"></textarea>' +
                        '</div>'
                      );
                    });
                  break;
                }

                // Si tiene un extra, mandar a llamar su evento onchange.
                if (extra) {
                  $('#' + id).trigger('change');
                }

                datosCargados += 1;
              } else {
                mostrarMensaje(response.msg);
              }

              // Ya termino de cargar todos los datos.
              if (datosCargados === datos.length) {
                ocultarLoading();

                // Revisar si mandaron datos.
                (function() {
                  var data = <?php echo isset($_POST['data'])
                    ? $_POST['data'] : 0; echo ';' ?>

                  if (data) {
                    switch (data.action) {
                      case 'agregar':
                      break;

                      case 'editar':
                        // Viene del listado de ACTIVIDADES.
                        mostrarLoading();
                        $.post('../php/api.php', {
                          id: data.id,
                          accion: 'obtener-paciente'
                        }, function(response) {
                          if (response.status === 'OK') {
                            var data = response.data;
                            var base = data.base;
                            var alergias = data.alergias;
                            var adicciones = data.adicciones;
                            var vacunas = data.vacunas;

                            // BASE
                            $('#expediente').val(base.expediente);
                            $('#fecha-ingreso').val(base.fechaIngreso);
                            $('#nombre').val(base.nombre);
                            $('#apellido-paterno').val(base.apellidoPaterno);
                            $('#apellido-materno').val(base.apellidoMaterno);
                            $('#fecha-nacimiento').val(base.fechaNacimiento);
                            $('#sexo').val(base.sexo);
                            $('#peso').val(base.peso);
                            $('#estatura').val(base.estatura);
                            $('#telefono').val(base.telefono);
                            $('#celular').val(base.celular);
                            $('#email').val(base.email);
                            $('#nombre-ocupacion').val(base.nombreOcupacion);
                            $('#id-ocupacion').val(base.idOcupacion);
                            $('#tipos-sanguineos').val(base.tipoSanguineo);
                            $('#padecimiento-actual').val(base.padecimientoActual);
                            $('#exploracion-fisica').val(base.exploracionFisica);
                            $('#t-a').val(base.ta);
                            $('#f-r').val(base.fr);
                            $('#f-c').val(base.fc);
                            $('#menarca').val(base.menarca);
                            $('#gesta').val(base.gesta);
                            $('#fum').val(base.fum);
                            $('#ivsa').val(base.ivsa);
                            $('#para').val(base.para);
                            $('#abortos').val(base.abortos);
                            $('#cesareas').val(base.cesareas);
                            $('#p-sexuales').val(base.pSexuales);
                            $('#otras-actividades').val(base.otrasActividades);
                            $('#observaciones').val(base.observaciones);
                            $('#antecedentes-familiares').val(base.antecedentesFamiliares);
                            $('#antecedentes-patologicos').val(base.antecedentesPatologicos);
                            $('#antecedentes-quirurgicos').val(base.antecedentesQuirurgicos);
                            $('#antecedentes-ginecoobstetricos').val(base.antecedentesGinecoobstetricos);

                            alergias.forEach(function(alergia, index) {
                              var input = $('[name="alergias-ids[]"][value='+alergia.id+']');
                              var row = input.closest('.row');
                              var tiene = row.find('[name="alergias-tiene[]"]');
                              var observaciones = row.find('[name="alergias-observaciones[]"]');

                              tiene.val(alergia.tiene);
                              observaciones.val(alergia.observaciones);
                            });

                            adicciones.forEach(function(adiccion, index) {
                              var input = $('[name="adicciones-ids[]"][value='+adiccion.id+']');
                              var row = input.closest('.row');
                              var tiene = row.find('[name="adicciones-tiene[]"]');
                              var observaciones = row.find('[name="adicciones-observaciones[]"]');

                              tiene.val(adiccion.tiene);
                              observaciones.val(adiccion.observaciones);
                            });

                            vacunas.forEach(function(vacuna, index) {
                              var input = $('[name="vacunas-ids[]"][value='+vacuna.id+']');
                              var row = input.closest('.row');
                              var tiene = row.find('[name="vacunas-tiene[]"]');
                              var observaciones = row.find('[name="vacunas-observaciones[]"]');

                              tiene.val(vacuna.tiene);
                              observaciones.val(vacuna.observaciones);
                            });

                            $('#formulario').append('<input type=hidden' +
                              ' name="id-paciente" value=' + base.id + '>');
                          }

                          ocultarLoading();
                        }, 'json').fail(function() {
                          ocultarLoading();
                          mostrarMensaje('Falló la conexión al servidor,' +
                            ' por favor vuelve a intentarlo.');
                        });
                      break;
                    }
                  }
                })();

                $('#formulario').find(':input:first').focus();
              }
            }, 'json').fail(function() {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            });
          });
        })();

        $('#nombre-ocupacion').autocomplete({
          minLength: 2,
          autoFocus: true,
          source: function (request, response) {
            request.accion = 'obtener-ocupaciones';
            return $.post("../php/api.php", request, function(_response) {
              return response(_response.data);
            }, 'json');
          },
          change: function( event, ui ) {
            if (ui.item === null) {
              $('[name="id-ocupacion"]').val('');
            }
          },
          search: function( event, ui ) {
          },
          select: function( event, ui ) {
            $('[name="id-ocupacion"]').val(ui.item.id);
            return true;
          }
        }).autocomplete( "instance" )._renderItem = function( ul, item ) {
          return $( "<li style='border-bottom:1px solid grey'>" )
          .append( "<a>" + item.label + "</a>" )
          .appendTo( ul );
        };

        $('#formulario').on('submit', function(evt) {
          // Validación por el nombre vacio.
          if ($('#nombre').val() === '') {
            $('#nombre').focus();
            return evt.preventDefault();
          }

          // Validacion por los campos de fecha.
          var dateInput;
          $.each($('[data-date]'), function(i, input) {
            if (input.value === '') {
              dateInput = input;
              return;
            }
          });

          if (dateInput) {
            $(dateInput).focus();
            return evt.preventDefault();
          }

          // Validacion si hay algun error aún.
          if ($('.is-invalid-input').length > 0
            || $('.form-error.is-visible').length > 0) {
            $('.is-invalid-input').first().parent().find(':input').focus();
            return evt.preventDefault();
          }

          // Quitamos clases de error.
          $('.is-invalid-input').removeClass('is-invalid-input');
          $('.form-error').removeClass('is-visible');

          mostrarLoading();

          var data = $(this).serialize();

          $.post('../php/api.php', data, function(response) {
            if (response.status === 'OK') {
              refresh();
            }

            ocultarLoading();
            mostrarMensaje(response.msg);
          }, 'json').fail(function() {
            ocultarLoading();
            mostrarMensaje('Falló la conexión al servidor,' +
              ' por favor vuelve a intentarlo.');
          });

          return evt.preventDefault();
        });

        // Evento para deshabilitar el 'enter' para que no se ejecute
        // el submit del formulario.
        $(document).on('keydown', function(evt) {
          if (evt.keyCode === 13 && evt.target.tagName !== 'TEXTAREA') {
            return evt.preventDefault();
          }

          return;
        });
        /*  */

        // Trigger de eventos iniciales.
        $('#formulario').find(':input:first').focus();

        // Evento para agregar mascara a las fechas.
        $('[data-date]').mask('00/00/0000', {
          onComplete: function(cep) {
            var target = this.event.target;
            var id = target.id;

            if (id === 'fecha-nacimiento') {
              var edad = calcularEdad(cep);
              if (!edad) {
                $('#edad').val('');
                return;
              }

              $('#edad').val(edad);
            }

            $('#' + id).removeClass('is-invalid-input');
            $('#' + id + '-incorrecta').removeClass('is-visible');
          },
          onChange: function(cep) {
            var target = this.event.target;
            var id = target.id;

            $('#' + id).addClass('is-invalid-input');
            $('#' + id + '-incorrecta').addClass('is-visible');
          }
        });
        $('[data-tel]').mask('0000000000');
        $('[data-num]').mask('000');
        $('[data-float]').on('keypress', function(evt) {
          if (evt.keyCode === 46 && !evt.target.value.match(/\./)
            && evt.target.value !== '') {
            return true;
          }

          if (evt.keyCode >= 48 && evt.keyCode <= 57) {

            var mask = evt.target.dataset.float;
            var value = evt.target.value;
            var maskSplit = mask.split('.');
            var valueSplit = value.split('.');

            if ( (value === '' || valueSplit[0].length < maskSplit[0].length)
              && !evt.target.value.match(/\./) ) {
              return true;
            }

            if (valueSplit[1] !== undefined && valueSplit[1].length < maskSplit[1].length) {
              return true;
            }
          }

          return evt.preventDefault();
        });
      });
    </script>
  </body>
</html>