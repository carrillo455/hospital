<?php
  if (!isset($_POST['data'])) {
    header('Location: index.php');
    exit;
  }

  require_once 'fdpf/fdpf.php';
  $data = json_decode($_POST['data']);
  $tipo = $data['tipo'];

  //extract data from the post
  //set POST variables
  $url = 'http://localhost/api.php';
  $fields = array(
    'lname' => urlencode($_POST['last_name']),
    'fname' => urlencode($_POST['first_name']),
    'title' => urlencode($_POST['title'])
  );

  //url-ify the data for the POST
  foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
  rtrim($fields_string, '&');

  //open connection
  $ch = curl_init();

  //set the url, number of POST vars, POST data
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_POST, count($fields));
  curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

  //execute post
  $result = curl_exec($ch);

  //close connection
  curl_close($ch);

  switch ($tipo) {
    case 'colposcopio':
      $pdf = new FPDF();
      $pdf->AddPage();
      $pdf->SetFont('Arial','B',16);
      $pdf->Cell(40,10,'¡Hola, Mundo!');
      $pdf->Output();
    break;
  }
?>