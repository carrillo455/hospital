<header>
  <div class="title-bar" data-responsive-toggle="menu" data-hide-for="medium">
    <button class="menu-icon" type="button" data-toggle="menu"></button>
    <div class="title-bar-title">Hospital Ángeles</div>
  </div>

  <div class="top-bar" id="menu">
    <div class="top-bar-left">
      <ul class="dropdown menu" data-dropdown-menu>
        <li class="menu-text show-for-medium">Hospital Ángeles</li>
        <li><a href="index.php">Consulta</a></li>
        <li><a href="kardex.php">Kardex</a></li>
        <li>
          <a href="#">Pacientes</a>
          <ul class="menu vertical">
            <li><a href="guardar.php">Guardar</a></li>
            <li><a href="consultar.php">Consultar</a></li>
          </ul>
        </li>
        <!-- <li><a href="reportes.php">Reportes</a></li> -->
      </ul>
    </div>
    <div class="top-bar-right">
      <ul class="menu">
        <li>
          <button id="salir" type="button" class="button">Cerrar Sesión</button>
        </li>
      </ul>
    </div>
  </div>
</header>