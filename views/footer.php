<div id="loading" class="tiny reveal" data-reveal
  data-close-on-click="false" data-close-on-esc="false">
  <p class="lead">Espera un momento porfavor...</p>
</div>

<div id="mensaje" class="tiny reveal" data-reveal>
  <br>
  <p class="lead"></p>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
  <button class="small button float-right" data-close>CERRAR</button>
</div>

<div id="confirmar" class="tiny reveal" data-reveal data-close-on-click="false">
  <br>
  <p class="lead"></p>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
  <div class="button-group float-right">
    <button class="small secondary button" data-close>CANCELAR</button>
    <button class="small button" data-accept>ACEPTAR</button>
  </div>
</div>