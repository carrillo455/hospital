<?php
  session_start();
  if (!isset($_SESSION['usuario'])) {
    header('Location: ../index.php');
  } else {
    $claves = $_SESSION['usuario']['claves'];
    if (!in_array('SUD01', $claves) && !in_array('ADM03', $claves)) {
      header('Location: ../index.php');
    }
  }
?>
<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema Administrativo de Información Médica v1.0.0</title>
    <link rel="icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/foundation.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="../css/app.css">
  </head>
  <body>
    <?php require_once 'header.php'; ?>

    <main>
      <div class="medium callout">
        <div class="row">
          <div class="large-12 columns">
            <h4 class="text-center">CONSULTAR INFORMACION DE LOS PACIENTES</h4>
          </div>
        </div>

        <div class="row">
          <div class="large-12 columns">
            <fieldset>
              <legend><strong>Columnas visibles</strong>
                <a id="toggle-columns" href="#toggle-columns" data-text="Mostrar">Esconder</a>
              </legend>
              <div class="row" data-columns>
                <div class="large-12 columns">
                  <input id="checkbox-todos" type="checkbox" data-all="true" checked>
                  <label for="checkbox-todos">Todos</label>
                </div>
              </div>
            </fieldset>
          </div>
        </div>

        <table id="dt-pacientes" class="dataTable" data-init="false">
          <thead></thead>
        </table>
      </div>
    </main>

    <?php require_once 'footer.php'; ?>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.min.js"></script>
    <script src="../js/vendor/jquery-ui.min.js"></script>
    <script src="../js/vendor/jquery.dataTables.min.js"></script>
    <script src="../js/vendor/dataTables.buttons.min.js"></script>
    <script src="../js/vendor/dataTables.buttons.flash.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/editar-paciente.js"></script>
    <script>
      $(document).ready(function() {
        var columns = [
          {'title': '#', 'className': 'text-right', 'width': '2.5%'},
          {'title': '', 'className': 'td-id', 'visible': false},
          {'title': 'EXPEDIENTE', 'className': 'text-center', 'width': '5%'},
          {'title': 'FECHA INGRESO', 'className': 'text-center', 'width': '5%'},
          {'title': 'NOMBRE', 'width': '5%'},
          {'title': 'FECHA NACIMIENTO', 'className': 'text-center', 'width': '5%'},
          {'title': 'SEXO', 'className': 'text-center', 'width': '5%'},
          {'title': 'PESO', 'className': 'text-center', 'width': '5%'},
          {'title': 'ESTATURA', 'className': 'text-center', 'width': '5%'},
          {'title': 'TELEFONO', 'className': 'text-center', 'width': '5%'},
          {'title': 'CELULAR', 'className': 'text-center', 'width': '5%'},
          {'title': 'EMAIL', 'width': '5%'},
          {'title': 'OCUPACION', 'width': '5%'},
          {'title': 'SANGUINEO', 'className': 'text-center', 'width': '5%'},
          {'title': 'PADECIMIENTO', 'width': '5%'},
          {'title': 'EXPLORACION', 'width': '5%'},
          {'title': 'TA', 'width': '5%'},
          {'title': 'FR', 'width': '5%'},
          {'title': 'FC', 'width': '5%'},
          {'title': 'MENARCA', 'width': '5%'},
          {'title': 'GESTA', 'width': '5%'},
          {'title': 'FUM', 'width': '5%'},
          {'title': 'IVSA', 'width': '5%'},
          {'title': 'PARA', 'width': '5%'},
          {'title': 'ABORTOS', 'className': 'text-center', 'width': '5%'},
          {'title': 'CESAREAS', 'className': 'text-center', 'width': '5%'},
          {'title': 'P. SEXUALES', 'width': '5%'},
          {'title': 'OTRAS ACTIVIDADES', 'width': '5%'},
          {'title': 'OBSERVACIONES', 'width': '5%'},
          {'title': 'ANTECEDENTES FAMILIARES', 'width': '5%'},
          {'title': 'ANTECEDENTES PATOLOGICOS', 'width': '5%'},
          {'title': 'ANTECEDENTES QUIRURGICOS', 'width': '5%'},
          {'title': 'ANTECEDENTES GINECO OBTETRICOS', 'width': '5%'},
          {'title': 'ALERGIAS', 'width': '5%'},
          {'title': 'ADICCIONES', 'width': '5%'},
          {'title': 'VACUNAS', 'width': '5%'},
          {'title': 'ACCIONES', 'width': '5%'}
        ];
        var table = $('#dt-pacientes').dataTable( {
          'language': {
            'url': '../json/datatables.spanish.lang.json'
          },
          'autoWidth': false,
          'scrollX': true,
          'pageLength': 25,
          'processing': true,
          'serverSide': true,
          'ajax': '../php/scripts/server_processing.php?o=pacientes',
          'columns': columns,
          'order': [],
          "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "Todos"] ],
          'initComplete': function( settings, json ) {
            var api = this.api();
            table.attr('data-init', true);

            // Creacion de filtro de columnas visibles.
            (function() {
              // $(api.table().header().children).children('th')
              $.each(columns, function(index, column) {
                if (column.visible === false) return;
                var checkbox = document.createElement('input');
                var label = document.createElement('label');
                var div = document.createElement('div');
                checkbox.type = 'checkbox';
                checkbox.id = 'column-' + index;
                checkbox.checked = true;
                checkbox.dataset.column = index;
                label.setAttribute('for', checkbox.id);
                label.textContent = column.title;
                label.style.width = '75%';
                div.className = 'large-3 medium-4 small-6 columns';
                if (index === (columns.length - 1)) {
                  div.classList.add('end');
                }
                div.appendChild(checkbox);
                div.appendChild(label);
                $('[data-columns]').append(div);
              });

              $('[data-columns] input[type="checkbox"]').on('change',
              function(e) {
                if (this.dataset.all) {
                  var todosIsChecked = this.checked;
                  $.each($('[data-columns] input[type="checkbox"]'),
                    function(index, checkbox) {
                      if (checkbox.dataset.all) return;
                      var column = table.DataTable().column( checkbox.dataset.column );
                      column.visible( todosIsChecked );
                      checkbox.checked = todosIsChecked;
                    });
                  return;
                }

                // Get the column API object
                var column = table.DataTable().column( this.dataset.column );

                // Toggle the visibility
                column.visible( ! column.visible() );
              });
            })();
          },
          'drawCallback': function( settings ) {
            var api = this.api();
            var start = settings._iDisplayStart;

            // Crear un consecutivo en la primera columna.
            api.column(0).nodes().each( function (cell, i) {
              start += 1;
              cell.innerHTML = start;
            });

            // Darle funcionalidad al dropdown proveniente del server-side.
            table.find('.dropdown').foundation();
          },
          'destroy': true,
          // 'dom': 'B<\"clear\">lfrtip',
          // 'buttons': [{
          //   extend: 'excel',
          //   text: 'Exportar Excel',
          //   exportOptions: {
          //     modifier: {
          //       search: 'none'
          //     },
          //     columns: ':visible'
          //   }
          // },
          // {
          //   extend: 'csv',
          //   text: 'Exportar CSV',
          //   exportOptions: {
          //     modifier: {
          //       search: 'none'
          //     },
          //     columns: ':visible'
          //   }
          // }]
        });

        $('#toggle-columns').on('click', function(e) {
          e.preventDefault();

          var toggleText = this.dataset.text;
          this.dataset.text = this.textContent;
          this.textContent = toggleText;
          return $('[data-columns]').toggleClass('hide');
        });

        table.on('click', '[data-action]', function() {
          var action = this.dataset.action;
          table.data('target', this);

          switch (action) {
            case 'editar-paciente':
              editarPaciente(table);
            break;

            case 'eliminar-paciente':
              eliminarPaciente(table);
            break;
          }

          return;
        });
      });
    </script>
  </body>
</html>