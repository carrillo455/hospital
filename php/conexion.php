<?php
  // Estas validaciones son para no tener que modificar
  // el archivo de conexion y asi trabajar en localhost u online.
  $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off'
    ? 'https://'
    : 'http://';
  // Cuando trabajas en localhost...
  $host = $protocol . $_SERVER['HTTP_HOST'] . '/hospital';
  $mysql = array(
    'host' => 'localhost',
    'db' => 'hospital',
    'user' => 'root',
    'pass' => '4lt4m1r4*'
  );

  // Cuando es online...
  if ( $_SERVER['HTTP_HOST'] !== 'localhost'
      && $_SERVER['HTTP_HOST'] !== 'localhost:8080'
      && strpos($_SERVER['HTTP_HOST'], '192.168.') === FALSE ) {
    $host = $protocol . $_SERVER['HTTP_HOST'];
    $mysql['host'] = 'localhost';
    $mysql['db'] = 'hospital';
    $mysql['user'] = 'root';
    $mysql['pass'] = '';
  }

  $pdo = new PDO("mysql:host=" . $mysql['host'] . ";dbname=" . $mysql['db'] .
    ";charset=utf8", $mysql['user'], $mysql['pass']);
?>