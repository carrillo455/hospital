<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

$option = $_GET['o'];

switch ($option) {
  case 'colposcopio':
    // DB table to use
    $table = 'dt_colposcopio';
    /*ALTER VIEW dt_colposcopio AS
    SELECT 1 AS consecutivo, c.colposcopio_id AS id, p.expediente,
      DATE_FORMAT(c.fecha, '%d/%m/%Y') AS fecha,
      CONCAT(p.nombre, ' ', p.apellido_paterno, ' ', p.apellido_materno) AS nombre,
      DATE_FORMAT(p.fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento,
      c.procedimiento, i.nombre AS instrumento,
      c.motivo_estudio AS motivoEstudio, IF(c.anestesia=1,'SI','NO') AS anestesia,
      IF(CHAR_LENGTH(c.hallazgos) > 52,
        CONCAT(SUBSTRING(c.hallazgos, 1, 52),'...'), c.hallazgos) AS hallazgos,
      IF(CHAR_LENGTH(c.impresion_diagnostica) > 52,
        CONCAT(SUBSTRING(c.impresion_diagnostica, 1, 52),'...'),
        c.impresion_diagnostica) AS impresionDiagnostica,
      IF(CHAR_LENGTH(c.plan) > 52, CONCAT(SUBSTRING(c.plan, 1, 52),'...'), c.plan) AS plan,
      CONCAT('<ul class="dropdown menu navigation" data-dropdown-menu data-closing-time=50>',
        '<li>',
          '<a href="#acciones">Da clic para...</a>',
          '<ul class="menu">',
            '<li><a href="#reporte" data-action="ver-reporte">VER REPORTE</a></li>',
            '<li><a href="#status" data-action="ver-imagenes">VER IMAGENES</a></li>',
          '</ul>',
        '</li>'
      '</ul>') AS acciones
    FROM pacientes p
    INNER JOIN colposcopio c ON c.pacientes_id = p.pacientes_id
    INNER JOIN instrumentos i ON i.instrumentos_id = c.instrumentos_id
    WHERE p.borrado = 0
    ORDER BY p.pacientes_id;*/

    $primaryKey = 'id';
    $columns = array(
      array( 'db' => 'consecutivo', 'dt' => 0 ),
      array( 'db' => 'id', 'dt' => 1 ),
      array( 'db' => 'expediente', 'dt' => 2 ),
      array( 'db' => 'fecha', 'dt' => 3 ),
      array( 'db' => 'nombre', 'dt' => 4 ),
      array( 'db' => 'fechaNacimiento', 'dt' => 5 ),
      array( 'db' => 'procedimiento', 'dt' => 6 ),
      array( 'db' => 'instrumento', 'dt' => 7 ),
      array( 'db' => 'motivoEstudio', 'dt' => 8 ),
      array( 'db' => 'anestesia', 'dt' => 9 ),
      array( 'db' => 'hallazgos', 'dt' => 10 ),
      array( 'db' => 'impresionDiagnostica', 'dt' => 11 ),
      array( 'db' => 'plan', 'dt' => 12 ),
      array( 'db' => 'acciones', 'dt' => 13 )
    );
  break;

  case 'pacientes':
    $table = 'dt_pacientes';

    $primaryKey = 'id';
    $columns = array(
      array('db' => 'consecutivo', 'dt' => 0),
      array('db' => 'id', 'dt' => 1),
      array('db' => 'expediente', 'dt' => 2),
      array('db' => 'fechaIngreso', 'dt' => 3),
      array('db' => 'nombre', 'dt' => 4),
      array('db' => 'fechaNacimiento', 'dt' => 5),
      array('db' => 'sexo', 'dt' => 6),
      array('db' => 'peso', 'dt' => 7),
      array('db' => 'estatura', 'dt' => 8),
      array('db' => 'telefono', 'dt' => 9),
      array('db' => 'celular', 'dt' => 10),
      array('db' => 'email', 'dt' => 11),
      array('db' => 'nombreOcupacion', 'dt' => 12),
      array('db' => 'nombreSanguineoTipo', 'dt' => 13),
      array('db' => 'padecimientoActual', 'dt' => 14),
      array('db' => 'exploracionFisica', 'dt' => 15),
      array('db' => 'ta', 'dt' => 16),
      array('db' => 'fr', 'dt' => 17),
      array('db' => 'fc', 'dt' => 18),
      array('db' => 'menarca', 'dt' => 19),
      array('db' => 'gesta', 'dt' => 20),
      array('db' => 'fum', 'dt' => 21),
      array('db' => 'ivsa', 'dt' => 22),
      array('db' => 'para', 'dt' => 23),
      array('db' => 'abortos', 'dt' => 24),
      array('db' => 'cesareas', 'dt' => 25),
      array('db' => 'pSexuales', 'dt' => 26),
      array('db' => 'otrasActividades', 'dt' => 27),
      array('db' => 'observaciones', 'dt' => 28),
      array('db' => 'antecedentesFamiliares', 'dt' => 29),
      array('db' => 'antecedentesPatologicos', 'dt' => 30),
      array('db' => 'antecedentesQuirurgicos', 'dt' => 31),
      array('db' => 'antecedentesGinecoobstetricos', 'dt' => 32),
      array('db' => 'alergias', 'dt' => 33),
      array('db' => 'adicciones', 'dt' => 34),
      array('db' => 'vacunas', 'dt' => 35),
      array('db' => 'acciones', 'dt' => 36)
    );
  break;
}

// SQL server connection information
require( '../conexion.php' );
$sql_details = array(
  'user' => $mysql['user'],
  'pass' => $mysql['pass'],
  'db'   => $mysql['db'],
  'host' => $mysql['host']
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );

echo json_encode(
  SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);

