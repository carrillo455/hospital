<?php
  // Validar si ya se hizo 'session_start' en algun otro archivo y evitar el error.
  if (!session_id()) {
    session_start();
  }

  // Mandar llamar el archivo de conexion.
  require_once 'conexion.php';

  // Recibir la accion desde el lado cliente.
  $accion = $_POST['accion'];

  switch ($accion) {
    case 'log-in':
      $username = $_POST['username'];
      $password = $_POST['password'];

      $query = $pdo->prepare("SELECT u.usuarios_id AS id, u.username,
          un.usuarios_niveles_id AS id_nivel, un.nombre AS nombre_nivel
          FROM usuarios u
          INNER JOIN usuarios_niveles un
            ON un.usuarios_niveles_id = u.usuarios_niveles_id
          WHERE CAST(username AS BINARY) = :username
            AND CAST(password AS BINARY) = :password
            AND u.borrado = 0
            AND un.borrado = 0");
      $query->execute(array('username' => $username, 'password' => $password));
      $usuario = $query->fetch(PDO::FETCH_ASSOC);

      if ($usuario) {
        $_SESSION['usuario']['id'] = $usuario['id'];
        $_SESSION['usuario']['username'] = $usuario['username'];
        $_SESSION['usuario']['nivel'] = array();
        $_SESSION['usuario']['nivel']['id'] = $usuario['id_nivel'];
        $_SESSION['usuario']['nivel']['nombre'] = $usuario['nombre_nivel'];

        $query = $pdo->query("SELECT clave
          FROM usuarios_accesos ua
          INNER JOIN usuarios_permisos up
            ON up.usuarios_accesos_id = ua.usuarios_accesos_id
          WHERE up.borrado = 0");

        $claves = $query->fetchAll(PDO::FETCH_COLUMN);
        $_SESSION['usuario']['claves'] = $claves;

        echo json_encode(array('status' => 'OK'));
      } else {
        echo json_encode(array('status' => 'WRONG'));
      }
    break;

    case 'log-out':
      session_destroy();
      session_unset();

      echo json_encode(array('status' => 'OK'));
    break;

    case 'check':
      $status = isset($_SESSION['usuario']) ? 'OK' : 'BYE';
      echo json_encode(array('status' => $status));
    break;

    case 'obtener-adicciones':
      $query = $pdo->query("SELECT a.adicciones_id AS id, a.nombre AS nombre
          FROM adicciones a
          WHERE borrado = 0");

      $adicciones = $query->fetchAll(PDO::FETCH_ASSOC);

      if(!$adicciones) {
        echo json_encode(array(
          'status' => 'ERROR OBTENER ADICCIONES',
          'msg' => 'Lo sentimos, no fue posible obtener información escencial'.
            ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      echo json_encode(array(
        'status' => 'OK',
        'data' => $adicciones
        )
      );
    break;

    case 'obtener-alergias':
      $query = $pdo->query("SELECT a.alergias_id AS id, a.nombre AS nombre
          FROM alergias a
          WHERE borrado = 0");

      $alergias = $query->fetchAll(PDO::FETCH_ASSOC);

      if(!$alergias) {
        echo json_encode(array(
          'status' => 'ERROR OBTENER ALERGIAS',
          'msg' => 'Lo sentimos, no fue posible obtener información escencial'.
            ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      echo json_encode(array(
        'status' => 'OK',
        'data' => $alergias
        )
      );
    break;

    case 'obtener-cantidad-pacientes':
      $query = $pdo->query("SELECT COUNT(*) AS cantidad
        FROM pacientes
        WHERE borrado = 0");
      $total = $query->fetchColumn();

      if (!$total) {
        echo json_encode(array(
          'status' => 'ERROR OBTENER CANTIDAD PACIENTES',
          'msg' => 'Lo sentimos, no fue posible obtener información esencial' .
            ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      echo json_encode(array(
          'status' => 'OK',
          'data' => array(
            'total' => $total,
            'totalStatus' => 0
          )
        )
      );
    break;

    case 'obtener-expediente':
      $query = $pdo->query("SELECT
        IF(
          (SELECT MAX(expediente) FROM pacientes WHERE borrado = 0) IS NULL,
          LPAD(1, 5, '0'),
          LPAD((SELECT MAX(expediente)+1 FROM pacientes WHERE borrado = 0), 5, '0')
        )");
      $expediente = $query->fetchColumn();

      if (!$expediente) {
        echo json_encode(array(
          'status' => 'ERROR OBTENER EXPEDIENTE',
          'msg' => 'Lo sentimos, no fue posible obtener información esencial' .
            ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      echo json_encode(array(
          'status' => 'OK',
          'data' => $expediente
        )
      );
    break;

    case 'obtener-imagenes':
      $colposcopio_id = $_POST['id'];

      $query = $pdo->prepare("SELECT colposcopio_imagenes_id AS id, url
        FROM colposcopio_imagenes
        WHERE colposcopio_id = :colposcopio_id");
      $ok = $query->execute(array('colposcopio_id' => $colposcopio_id));
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER IMAGENES',
            'msg' => 'Lo sentimos, algo sucedió al obtener la información'.
              ' Por favor, vuelve a intentarlo.'
          )
        );
        exit;
      }

      $imagenes = $query->fetchAll(PDO::FETCH_ASSOC);
      $msg = 'No hay imágenes.';
      if (count($imagenes) > 0) { $msg = ''; }
      foreach ($imagenes as $index => $imagen) {
        $msg .= '<img class="thumbnail" src="' . $imagen['url'] . '">';
      }

      echo json_encode(array(
          'status' => 'OK',
          'data' => $imagenes,
          'msg' => $msg
        )
      );
    break;

    case 'obtener-instrumentos':
      $query = $pdo->query("SELECT instrumentos_id AS id, nombre
        FROM instrumentos
        WHERE borrado = 0");
      $instrumentos = $query->fetchAll(PDO::FETCH_ASSOC);

      if (!$instrumentos) {
        echo json_encode(array(
          'status' => 'ERROR OBTENER INSTRUMENTOS',
          'msg' => 'Lo sentimos, no fue posible obtener información esencial' .
            ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      echo json_encode(array(
          'status' => 'OK',
          'data' => $instrumentos
        )
      );
    break;

    case 'obtener-ocupaciones':
      $term = isset($_POST['term']) ? $_POST['term'] : false;

      if ($term) {
        $query = $pdo->query("SELECT ocupaciones_id AS id, nombre AS label
          FROM ocupaciones
          WHERE nombre LIKE '%$term%'
            AND borrado = 0");
        // Traer mas informacion...
        // , c.procedimiento, c.instrumentos_id AS instrumento,
        //   c.motivo_estudio AS motivoEstudio, IF(c.anestesia=1,'si','no') AS anestesia, c.hallazgos,
        //   c.impresion_diagnostica AS impresionDiagnostica,
        //   (SELECT GROUP_CONCAT(url SEPARATOR ',') FROM colposcopio_imagenes
        //     WHERE colposcopio_id = c.colposcopio_id AND borrado = 0) AS imgs
        //  FROM pacientes p
        //  LEFT JOIN colposcopio c ON c.pacientes_id = p.pacientes_id
      } else  {
        $query = $pdo->query("SELECT ocupaciones_id AS id, nombre
          FROM ocupaciones
          WHERE borrado = 0");
      }

      $ocupaciones = $query->fetchAll(PDO::FETCH_ASSOC);
      if (!$ocupaciones && !is_numeric(count($ocupaciones)) ) {
        echo json_encode(
          array(
            'status' => 'ERROR OBTENER OCUPACIONES',
            'msg' => 'Lo sentimos, no fue posible obtener información esencial' .
              ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      echo json_encode(
        array(
          'status' => 'OK',
          'data' => $ocupaciones
        )
      );
    break;

    case 'obtener-paciente':
      $pacientes_id = $_POST['id'];

      $query = $pdo->prepare("SELECT p.pacientes_id AS id, p.expediente,
        DATE_FORMAT(p.fecha_ingreso, '%d/%m/%Y') AS fechaIngreso,
        p.nombre, p.apellido_paterno AS apellidoPaterno, p.apellido_materno AS apellidoMaterno,
        DATE_FORMAT(p.fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento, p.sexo,
        p.peso, p.estatura, p.telefono_1 AS telefono, p.celular, p.email,
        p.ocupaciones_id idOcupacion, o.nombre AS nombreOcupacion,
        st.sanguineos_tipos_id AS tipoSanguineo,
        p.padecimiento_actual AS padecimientoActual, p.exploracion_fisica AS exploracionFisica,
        p.t_a AS ta, p.f_r AS fr, p.f_c AS fc, p.menarca, p.gesta, p.fum, p.ivsa, p.para,
        p.abortos, p.cesareas, p.poses_sexuales AS pSexuales,
        p.otras_actividades AS otrasActividades, p.observaciones,
        p.antecedentes_familiares AS antecedentesFamiliares,
        p.antecedentes_patologicos AS antecedentesPatologicos,
        p.antecedentes_quirurgicos AS antecedentesQuirurgicos,
        p.antecedentes_ginecoobstetricos AS antecedentesGinecoobstetricos
      FROM pacientes p
      LEFT JOIN ocupaciones o ON p.ocupaciones_id = o.ocupaciones_id
      LEFT JOIN sanguineos_tipos st ON p.sanguineos_tipos_id = st.sanguineos_tipos_id
      WHERE p.pacientes_id = :pacientes_id AND p.borrado = 0");

      $ok = $query->execute(array('pacientes_id' => $pacientes_id));
      if (!$ok) {
        echo json_encode(
          array(
            'status' => 'ERROR OBTENER PACIENTE!',
            'msg' => 'Lo sentimos, no fue posible obtener información esencial' .
              ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $base = $query->fetch(PDO::FETCH_ASSOC);
      $_SESSION['paciente'] = array('id' => $base['id']);

      // Obtener alergias.
      $query = $pdo->query("SELECT a.alergias_id AS id,
        IF(pa.tiene = 1, 'si', 'no') AS tiene, pa.observaciones
        FROM alergias a
        INNER JOIN pacientes_alergias pa ON pa.alergias_id = a.alergias_id
        WHERE pa.borrado = 0 AND pa.pacientes_id = " . $base['id']);
      $alergias = $query ? $query->fetchAll(PDO::FETCH_ASSOC) : array();

      // Obtener adicciones.
      $query = $pdo->query("SELECT a.adicciones_id AS id,
        IF(pa.tiene = 1, 'si', 'no') AS tiene, pa.observaciones
        FROM adicciones a
        INNER JOIN pacientes_adicciones pa ON pa.adicciones_id = a.adicciones_id
        WHERE pa.borrado = 0 AND pa.pacientes_id = " . $base['id']);
      $adicciones = $query ? $query->fetchAll(PDO::FETCH_ASSOC) : array();

      // Obtener vacunas.
      $query = $pdo->query("SELECT v.vacunas_id AS id,
        IF(pv.tiene = 1, 'si', 'no') AS tiene, pv.observaciones
        FROM vacunas v
        INNER JOIN pacientes_vacunas pv ON pv.vacunas_id = v.vacunas_id
        WHERE pv.borrado = 0 AND pv.pacientes_id = " . $base['id']);
      $vacunas = $query ? $query->fetchAll(PDO::FETCH_ASSOC) : array();

      echo json_encode(
        array(
          'status' => 'OK',
          'data' => array(
            'base' => $base,
            'alergias' => $alergias,
            'adicciones' => $adicciones,
            'vacunas' => $vacunas
          )
        )
      );
    break;

    case 'obtener-pacientes':
      $term = isset($_POST['term']) ? $_POST['term'] : false;

      if ($term) {
        $query = $pdo->query("SELECT p.pacientes_id AS id, p.expediente,
          CONCAT(p.nombre, ' ', p.apellido_paterno, ' ', p.apellido_materno) AS label,
          p.nombre, p.apellido_paterno AS apellidoPaterno, p.apellido_materno AS apellidoMaterno,
          DATE_FORMAT(p.fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento
          FROM pacientes p
          WHERE (p.nombre LIKE '%$term%'
              OR p.apellido_paterno LIKE '%$term%'
              OR p.apellido_materno LIKE '%$term%'
              OR CONCAT(p.nombre, ' ', p.apellido_paterno, ' ', p.apellido_materno) LIKE '%$term%'
              OR p.expediente LIKE '%$term%'
            )
            AND p.borrado = 0");
        // Traer mas informacion...
        // , c.procedimiento, c.instrumentos_id AS instrumento,
        //   c.motivo_estudio AS motivoEstudio, IF(c.anestesia=1,'si','no') AS anestesia, c.hallazgos,
        //   c.impresion_diagnostica AS impresionDiagnostica,
        //   (SELECT GROUP_CONCAT(url SEPARATOR ',') FROM colposcopio_imagenes
        //     WHERE colposcopio_id = c.colposcopio_id AND borrado = 0) AS imgs
        //  FROM pacientes p
        //  LEFT JOIN colposcopio c ON c.pacientes_id = p.pacientes_id
      } else  {
        $query = $pdo->query("SELECT pacientes_id AS id, expediente,
          nombre, apellido_paterno AS apellidoPaterno, apellido_materno AS apellidoMaterno
          FROM pacientes
          WHERE borrado = 0");
      }

      $pacientes = $query->fetchAll(PDO::FETCH_ASSOC);
      if (!$pacientes && !is_numeric(count($pacientes)) ) {
        echo json_encode(
          array(
            'status' => 'ERROR OBTENER PACIENTES',
            'msg' => 'Lo sentimos, no fue posible obtener información esencial' .
              ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $_SESSION['paciente'] = array('id' => array());
      foreach ($pacientes as $key => $paciente) {
        array_push($_SESSION['paciente']['id'], $paciente['id']);
      }

      echo json_encode(
        array(
          'status' => 'OK',
          'data' => $pacientes
        )
      );
    break;

    case 'obtener-reporte':
      $id = $_POST['id'];
      $tipo = $_POST['tipo'];

      switch ($tipo) {
        case 'colposcopio':
          $imgs = isset($_POST['imgs']) ? implode(',', $_POST['imgs']) : '';
          $query = $pdo->prepare("SELECT 'Reporte de Colposcopia' AS titulo,
            p.expediente, DATE_FORMAT(c.timestamp, '%d/%m/%Y %H:%i') AS fecha,
            CONCAT(p.nombre, ' ', p.apellido_paterno, ' ', p.apellido_materno) AS nombre,
            FLOOR(DATEDIFF (NOW(), p.fecha_nacimiento)/365.25) AS edad,
            DATE_FORMAT(p.fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento,
            c.procedimiento, i.nombre AS instrumento,
            c.motivo_estudio AS motivoEstudio, IF(c.anestesia=1,'SI','NO') AS anestesia,
            c.hallazgos, c.impresion_diagnostica AS impresionDiagnostica, c.plan,
            GROUP_CONCAT(ci.url SEPARATOR ',') AS imagenes
          FROM pacientes p
          INNER JOIN colposcopio c ON c.pacientes_id = p.pacientes_id
          INNER JOIN instrumentos i ON i.instrumentos_id = c.instrumentos_id
          LEFT JOIN (
              SELECT * FROM colposcopio_imagenes
              WHERE FIND_IN_SET(colposcopio_imagenes_id, :imgs) > 0
            ) ci ON ci.colposcopio_id = c.colposcopio_id
          WHERE p.borrado = 0
            AND c.colposcopio_id = :id");
          $ok = $query->execute(array('imgs' => $imgs, 'id' => $id));

          if (!$ok) {
            echo json_encode(array(
                'status' => 'ERROR OBTENER REPORTE COLPOSCOPIO',
                'msg' => 'Lo sentimos, sucedió algo. Por favor, vuelve a intentar'.
                  ' a generar el reporte.'
              )
            );
            exit;
          }

          require_once 'fpdf/fpdf.php';
          $data = $query->fetch(PDO::FETCH_ASSOC);

          foreach ($data as $key => $value) {
            $data[$key] = utf8_decode($value);
          }

          // Inicializar.
          $pdf = new FPDF('P','mm','Letter');
          $pdf->AddPage();

          // Titulo.
          $pdf->SetY(25);
          $pdf->SetFont('Times','B',18);
          $pdf->Cell(0,0,strtoupper($data['titulo']),0,0,'C');

          // Primer Renglon.
          $pdf->SetY(40);
          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Nombre:')+1,10,'Nombre:',0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->Cell($pdf->GetPageWidth()/3,10,$data['nombre'],0,0,'L');

          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Edad:')+1,10,'Edad:',0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->Cell($pdf->GetPageWidth()/10,10,$data['edad'],0,0,'L');

          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Fecha:')+1,10,'Fecha:',0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->Cell($pdf->GetPageWidth()/7,10,$data['fecha'],0,0,'L');

          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Anestesia:')+1,10,'Anestesia:',0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->Cell($pdf->GetPageWidth()/20,10,$data['anestesia'],0,0,'L');

          // Segundo Renglon.
          $pdf->SetY(50);
          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Instrumento:')+1,10,'Instrumento:',0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->Cell($pdf->GetPageWidth()/4.5,10,$data['instrumento'],0,0,'L');

          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Motivo de Estudio::')+1,10,'Motivo de Estudio:',0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->Cell($pdf->GetPageWidth()/6,10,$data['motivoEstudio'],0,0,'L');

          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Procedimiento:')+1,10,'Procedimiento:',0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->Cell($pdf->GetPageWidth()/7.5,10,$data['procedimiento'],0,0,'L');

          // Salto de línea.
          $pdf->Ln();

          // Inserción de imágenes.
          $imagenes = explode(',', $data['imagenes']);
          $img_x = 110;
          $img_y = 65;
          foreach ($imagenes as $key => $url) {
            $index = (int) $key;
            if ($index%2 === 0) {
              $pdf->Image($url,null,$img_y,90,60);
            } else {
              $pdf->Image($url,$img_x,$img_y,90,60);
              if (isset($imagenes[$index+1])) {
                $img_y *= 2;
              }
            }
          }

          // Cuarto renglon.
          $y = $img_y + 5;
          $pdf->Ln($y);

          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Hallazgos:')+1,5,'Hallazgos:',0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->MultiCell(0,5,$data['hallazgos'],0,'L');

          $pdf->Ln(5);

          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Impresion Diagnostica:')+1,5,
            utf8_decode('Impresión Diagnostica:'),0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->MultiCell(0,5,$data['impresionDiagnostica'],0,'L');

          $pdf->Ln(5);

          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetStringWidth('Plan:')+1,5,
            utf8_decode('Plan:'),0,0,'L');
          $pdf->SetFont('Times','',10);
          $pdf->MultiCell(0,5,$data['plan'],0,'L');

          // Ultimo renglon.
          $pdf->SetXY($pdf->GetPageWidth()/1.75,$pdf->GetPageHeight() - 35);
          $pdf->SetFont('Times','B',10);
          $pdf->Cell($pdf->GetPageWidth()/3,10,'Firma','T',0,'C');

          // Guardar.
          $pdf->Output('F', '../temp/reporte-colposcopio.pdf');

          echo json_encode(array(
              'status' => 'OK',
              'data' => '../temp/reporte-colposcopio.pdf'
            )
          );
        break;
      }
    break;

    case 'obtener-sanguineos-tipos':
      $query = $pdo->query("SELECT s.sanguineos_tipos_id AS id, s.nombre AS nombre
          FROM sanguineos_tipos s
          WHERE borrado = 0");

      $sanguineos_tipos = $query->fetchAll(PDO::FETCH_ASSOC);

      if(!$sanguineos_tipos) {
        echo json_encode(array(
          'status' => 'ERROR OBTENER SANGUINEOS TIPOS',
          'msg' => 'Lo sentimos, no fue posible obtener información escencial'.
            ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      echo json_encode(array(
        'status' => 'OK',
        'data' => $sanguineos_tipos
        )
      );
    break;

    case 'obtener-vacunas':
      $query = $pdo->query("SELECT v.vacunas_id AS id, v.nombre AS nombre
        FROM vacunas v
        WHERE borrado = 0");

      $vacunas = $query->fetchAll(PDO::FETCH_ASSOC);

      if(!$vacunas) {
        echo json_encode(array(
          'status' => 'ERROR OBTENER VACUNAS',
          'msg' => 'Lo sentimos, no fue posible obtener información escencial'.
            ' para la captura de la consulta. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      echo json_encode(array(
        'status' => 'OK',
        'data' => $vacunas
        )
      );
    break;

    case 'guardar-colposcopio':
      $pacientes_id = empty($_POST['id-paciente'])
        ? false
        : (
          !in_array($_POST['id-paciente'], $_SESSION['paciente']['id'])
          ? false
          : $_POST['id-paciente']
        );
      // $expediente = $_POST['expediente'];
      // $fecha = $_POST['fecha'];
      $nombre = $_POST['nombre'];
      $apellido_paterno = $_POST['apellido-paterno'];
      $apellido_materno = $_POST['apellido-materno'];
      $fecha_nacimiento = empty($_POST['fecha-nacimiento'])
        ? null
        : implode('-', array_reverse(explode('/', $_POST['fecha-nacimiento'])));
      $procedimiento = $_POST['procedimiento'];
      $instrumentos_id = $_POST['instrumento'];
      $motivo_estudio = $_POST['motivo-estudio'];
      $anestesia = $_POST['anestesia'] === 'si' ? 1 : 0;
      $hallazgos = $_POST['hallazgos'];
      $impresion_diagnostica = $_POST['impresion-diagnostica'];
      $plan = $_POST['plan'];
      $imgs = isset($_POST['imgs']) ? $_POST['imgs'] : array();
      $limite_kb = 10000; // Limite en KB.
      $permitidos = array( // Formatos permitidos.
        "jpg" => "image/jpg",
        "jpeg" => "image/jpeg",
        "gif" => "image/gif",
        "png" => "image/png"
      );
      $path = '../photos/';
      $usuarios_id = $_SESSION['usuario']['id'];

      $pdo->beginTransaction();

      // Paciente.
      if (!$pacientes_id) {
        $insert = $pdo->prepare("INSERT INTO pacientes
          (expediente, fecha_ingreso, nombre, apellido_paterno, apellido_materno,
          fecha_nacimiento, usuarios_id)
          SELECT IF( MAX(expediente) IS NULL,
              LPAD(1, 5, '0'),
              LPAD(MAX(expediente) + 1, 5, '0')
            ), NOW(), :nombre, :apellido_paterno, :apellido_materno,
            :fecha_nacimiento, :usuarios_id
          FROM pacientes
          WHERE borrado = 0");
        $ok_paciente = $insert->execute(array(
            // 'expediente' => $expediente,
            'nombre' => $nombre,
            'apellido_paterno' => $apellido_paterno,
            'apellido_materno' => $apellido_materno,
            'fecha_nacimiento' => $fecha_nacimiento,
            'usuarios_id' => $usuarios_id
          )
        );

        if (!$ok_paciente) {
          $pdo->rollBack();
          echo json_encode(array(
              'status' => 'ERROR GUARDAR PACIENTE',
              'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                ' Por favor, vuelve a intentarlo.'
            )
          );
          exit;
        }

        $pacientes_id = $pdo->lastInsertId();

      }

      // Obtener el expediente del paciente.
      $query = $pdo->prepare("SELECT expediente
        FROM pacientes
        WHERE pacientes_id = :pacientes_id");
      $ok = $query->execute(array('pacientes_id' => $pacientes_id));
      $expediente = $query->fetchColumn();
      $path .= $expediente . '/';

      // Colposcopio.
      $insert = $pdo->prepare("INSERT INTO colposcopio
        (pacientes_id, fecha, procedimiento, instrumentos_id, motivo_estudio,
        anestesia, hallazgos, impresion_diagnostica, plan, usuarios_id)
        VALUES (:pacientes_id, NOW(), :procedimiento, :instrumentos_id, :motivo_estudio,
        :anestesia, :hallazgos, :impresion_diagnostica, :plan, :usuarios_id)");
      $ok_colposcopio = $insert->execute(array(
          'pacientes_id' => $pacientes_id,
          // 'fecha' => $fecha,
          'procedimiento' => $procedimiento,
          'instrumentos_id' => $instrumentos_id,
          'motivo_estudio' => $motivo_estudio,
          'anestesia' => $anestesia,
          'hallazgos' => $hallazgos,
          'impresion_diagnostica' => $impresion_diagnostica,
          'plan' => $plan,
          'usuarios_id' => $usuarios_id
        )
      );

      if (!$ok_colposcopio) {
        $pdo->rollBack();
        echo json_encode(array(
            'status' => 'ERROR GUARDAR COLPOSCOPIO',
            'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
              ' Por favor, vuelve a intentarlo.'
          )
        );
        exit;
      }

      $colposcopio_id = $pdo->lastInsertId();

      foreach ($imgs as $index => $dataUri) {
        $array_img = explode('base64,', $dataUri);
        $img = end($array_img);
        $encoded_data = str_replace(' ', '+', $img);
        $decoded_data = base64_decode($encoded_data);
        $size = strlen($decoded_data);

        foreach ($permitidos as $extension => $formato) {
          if (strpos($array_img[0], $formato) !== FALSE) {
            $extension = $extension;
            $formato = $formato;
            break 1;
          }
        }

        if (isset($extension)) {
          if ($size <= $limite_kb * 1024) {
            $file = uniqid() . '.' . $extension;

            if (!file_exists($path)) {
              mkdir($path, 0777, true);
            }

            $target = $path . $file;
            $saved = file_put_contents($target, $decoded_data, FILE_USE_INCLUDE_PATH);
          }
        }

        if ($saved) {
          $insert = $pdo->prepare("INSERT INTO colposcopio_imagenes
            (colposcopio_id, nombre, url, formato, usuarios_id)
            VALUES (:colposcopio_id, :nombre, :url, :formato, :usuarios_id)");
          $ok = $insert->execute(array(
              'colposcopio_id' => $colposcopio_id,
              'nombre' => $file,
              'url' => $host . '/photos/' . $expediente . '/' . $file,
              'formato' => $formato,
              'usuarios_id' => $usuarios_id
            )
          );

          if (!$ok) {
            $pdo->rollBack();
            echo json_encode(array(
                'status' => 'ERROR GUARDAR IMG ' . $index,
                'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                  ' Por favor, vuelve a intentarlo.'
              )
            );
            exit;
          }
        }
      }

      $pdo->commit();
      echo json_encode(array(
          'status' => 'OK',
          'data' => $colposcopio_id,
          'msg' => 'La información de la consulta fue guardada correctamente.'
        )
      );
    break;

    case 'guardar-paciente':
      $pacientes_id = empty($_POST['id-paciente'])
        ? false
        : (
          $_SESSION['paciente']['id'] !== $_POST['id-paciente']
          ? false
          : $_POST['id-paciente']
        );

      $fecha_ingreso = !empty($_POST['fecha-ingreso'])
        ? implode('-', array_reverse(explode('/', $_POST['fecha-ingreso'])))
        : 'NULL';
      $nombre = $_POST['nombre'];
      $apellido_paterno = $_POST['apellido-paterno'];
      $apellido_materno = $_POST['apellido-materno'];

      $fecha_nacimiento = !empty($_POST['fecha-nacimiento'])
        ? implode('-', array_reverse(explode('/', $_POST['fecha-nacimiento'])))
        : 'NULL';

      $sexo = isset($_POST['sexo'])
        ? $_POST['sexo']
        : 'NULL';
      $peso = !empty($_POST['peso'])
        ? $_POST['peso']
        : 'NULL';
      $estatura = !empty($_POST['estatura'])
        ? $_POST['estatura']
        : 'NULL';
      $telefono = $_POST['telefono'];
      $celular = $_POST['celular'];
      $email = $_POST['email'];
      $nombre_ocupacion = !empty($_POST['nombre-ocupacion'])
        ? $_POST['nombre-ocupacion']
        : 'NULL';
      $ocupaciones_id = !empty($_POST['id-ocupacion'])
        ? $_POST['id-ocupacion']
        : 'NULL';
      $sanguineos_tipos_id = isset($_POST['tipo-sanguineo'])
        ? $_POST['tipo-sanguineo']
        : 'NULL';
      $padecimiento_actual = $_POST['padecimiento-actual'];
      $exploracion_fisica = $_POST['exploracion-fisica'];
      $t_a = $_POST['t-a'];
      $f_r = $_POST['f-r'];
      $f_c = $_POST['f-c'];
      $menarca = $_POST['menarca'];
      $gesta = $_POST['gesta'];
      $fum = $_POST['fum'];
      $ivsa = $_POST['ivsa'];
      $para = $_POST['para'];
      $abortos = !empty($_POST['abortos'])
        ? $_POST['abortos']
        : 'NULL';
      $cesareas = !empty($_POST['cesareas'])
        ? $_POST['cesareas']
        : 'NULL';
      $poses_sexuales = $_POST['p-sexuales'];
      $otras_actividades = $_POST['otras-actividades'];
      $observaciones = $_POST['observaciones'];

      // Antecedentes.
      $antecedentes_familiares = $_POST['antecedentes-familiares'];
      $antecedentes_patologicos = $_POST['antecedentes-patologicos'];
      $antecedentes_quirurgicos = $_POST['antecedentes-quirurgicos'];
      $antecedentes_ginecoobstetricos = $_POST['antecedentes-ginecoobstetricos'];

      // Alergías.
      $alergias_ids = $_POST['alergias-ids'];
      $alergias_tiene = $_POST['alergias-tiene'];
      $alergias_observaciones = $_POST['alergias-observaciones'];

      // Adicciones.
      $adicciones_ids = $_POST['adicciones-ids'];
      $adicciones_tiene = $_POST['adicciones-tiene'];
      $adicciones_observaciones = $_POST['adicciones-observaciones'];

      // Vacunas.
      $vacunas_ids = $_POST['vacunas-ids'];
      $vacunas_tiene = $_POST['vacunas-tiene'];
      $vacunas_observaciones = $_POST['vacunas-observaciones'];

      $usuarios_id = $_SESSION['usuario']['id'];

      // Empezamos el proceso de insercion.
      $pdo->beginTransaction();

      // Insertar la ocupación en caso de ser nueva.
      if ($ocupaciones_id === 'NULL') {
        $query = $pdo->prepare("SELECT MAX(ocupaciones_id)
          FROM ocupaciones
          WHERE nombre = NULLIF(:nombre, 'NULL')");
        $ok = $query->execute(array('nombre' => $nombre_ocupacion));
        if (!$ok) {
          $pdo->rollBack();
          echo json_encode(array(
              'status' => 'ERROR OBTENER OCUPACION',
              'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                ' Por favor, vuelve a intentarlo.'
            )
          );
          exit;
        }

        $ocupaciones_id = $query->fetchColumn();
        if (!$ocupaciones_id) {
          $insert = $pdo->prepare("INSERT INTO ocupaciones
            (nombre) VALUES (NULLIF(:nombre, 'NULL'))");
          $ok = $insert->execute(array('nombre' => $nombre_ocupacion));
          if (!$ok) {
            $pdo->rollBack();
            echo json_encode(array(
                'status' => 'ERROR GUARDAR OCUPACION',
                'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                  ' Por favor, vuelve a intentarlo.'
              )
            );
            exit;
          }

          $ocupaciones_id = $pdo->lastInsertId();
        }
      }

      // Validar si se esta editando o insertando paciente.
      $pacientes_values = array(
        'fecha_ingreso' => $fecha_ingreso,
        'nombre' => $nombre,
        'apellido_paterno' => $apellido_paterno,
        'apellido_materno' => $apellido_materno,
        'fecha_nacimiento' => $fecha_nacimiento,
        'peso' => $peso,
        'estatura' => $estatura,
        'sexo' => $sexo,
        'telefono_1' => $telefono,
        'celular' => $celular,
        'email' => $email,
        'ocupaciones_id' => $ocupaciones_id,
        'sanguineos_tipos_id' => $sanguineos_tipos_id,
        'padecimiento_actual' => $padecimiento_actual,
        'exploracion_fisica' => $exploracion_fisica,
        't_a' => $t_a,
        'f_r' => $f_r,
        'f_c' => $f_c,
        'menarca' => $menarca,
        'gesta' => $gesta,
        'fum' => $fum,
        'ivsa' => $ivsa,
        'para' => $para,
        'abortos' => $abortos,
        'cesareas' => $cesareas,
        'poses_sexuales' => $poses_sexuales,
        'otras_actividades' => $otras_actividades,
        'observaciones' => $observaciones,
        'antecedentes_familiares' => $antecedentes_familiares,
        'antecedentes_patologicos' => $antecedentes_patologicos,
        'antecedentes_quirurgicos' => $antecedentes_quirurgicos,
        'antecedentes_ginecoobstetricos' => $antecedentes_ginecoobstetricos
      );

      if ($pacientes_id) {
        $statement = $pdo->prepare("UPDATE pacientes
          SET fecha_ingreso = NULLIF(:fecha_ingreso, 'NULL'),
            nombre = :nombre,
            apellido_paterno = :apellido_paterno,
            apellido_materno = :apellido_materno,
            fecha_nacimiento = NULLIF(:fecha_nacimiento, 'NULL'),
            peso = NULLIF(:peso, 'NULL'),
            estatura = NULLIF(:estatura, 'NULL'),
            sexo = NULLIF(:sexo, 'NULL'),
            telefono_1 = :telefono_1,
            celular = :celular,
            email = :email,
            ocupaciones_id = NULLIF(:ocupaciones_id, 'NULL'),
            sanguineos_tipos_id = NULLIF(:sanguineos_tipos_id, 'NULL'),
            padecimiento_actual = :padecimiento_actual,
            exploracion_fisica = :exploracion_fisica,
            t_a = :t_a,
            f_r = :f_r,
            f_c = :f_c,
            menarca = :menarca,
            gesta = :gesta,
            fum = :fum,
            ivsa = :ivsa,
            para = :para,
            abortos = NULLIF(:abortos, 'NULL'),
            cesareas = NULLIF(:cesareas, 'NULL'),
            poses_sexuales = :poses_sexuales,
            otras_actividades = :otras_actividades,
            observaciones = :observaciones,
            antecedentes_familiares = :antecedentes_familiares,
            antecedentes_patologicos = :antecedentes_patologicos,
            antecedentes_quirurgicos = :antecedentes_quirurgicos,
            antecedentes_ginecoobstetricos = :antecedentes_ginecoobstetricos
          WHERE pacientes_id = :pacientes_id");
        $pacientes_values['pacientes_id'] = $pacientes_id;
      } else {
        // Insertar paciente.
        $statement = $pdo->prepare("INSERT INTO pacientes
          (expediente, fecha_ingreso, nombre, apellido_paterno, apellido_materno,
          fecha_nacimiento, peso, estatura, sexo, telefono_1, celular,
          email, ocupaciones_id, sanguineos_tipos_id,
          padecimiento_actual, exploracion_fisica, t_a, f_r, f_c,
          menarca, gesta, fum, ivsa, para, abortos,
          cesareas, poses_sexuales, otras_actividades,
          observaciones, antecedentes_familiares, antecedentes_patologicos,
          antecedentes_quirurgicos, antecedentes_ginecoobstetricos, usuarios_id)
          SELECT IF( MAX(expediente) IS NULL,
                LPAD(1, 5, '0'),
                LPAD(MAX(expediente) + 1, 5, '0')
              ), :fecha_ingreso, :nombre, :apellido_paterno, :apellido_materno,
            NULLIF(:fecha_nacimiento, 'NULL'), NULLIF(:peso, 'NULL'),
            NULLIF(:estatura, 'NULL'), NULLIF(:sexo, 'NULL'), :telefono_1, :celular,
            :email, NULLIF(:ocupaciones_id, 'NULL'), NULLIF(:sanguineos_tipos_id, 'NULL'),
            :padecimiento_actual, :exploracion_fisica, :t_a, :f_r, :f_c,
            :menarca, :gesta, :fum, :ivsa, :para, NULLIF(:abortos, 'NULL'),
            NULLIF(:cesareas, 'NULL'), :poses_sexuales, :otras_actividades,
            :observaciones, :antecedentes_familiares, :antecedentes_patologicos,
            :antecedentes_quirurgicos, :antecedentes_ginecoobstetricos, :usuarios_id
          FROM pacientes
          WHERE borrado = 0");
        $pacientes_values['usuarios_id'] = $usuarios_id;
      }

      $ok_paciente = $statement->execute($pacientes_values);

      if (!$ok_paciente) {
        $pdo->rollBack();
        echo json_encode(array(
            'status' => 'ERROR GUARDAR PACIENTE',
            'error' => "UPDATE pacientes SET fecha_ingreso = NULLIF('$fecha_ingreso', 'NULL'), nombre = '$nombre', apellido_paterno = '$apellido_paterno', apellido_materno = '$apellido_materno', fecha_nacimiento = NULLIF('$fecha_nacimiento', 'NULL'), peso = NULLIF('$peso', 'NULL'), estatura = NULLIF('$estatura', 'NULL'), sexo = NULLIF('$sexo', 'NULL'), telefono_1 = '$telefono', celular = '$celular', email = '$email', ocupaciones_id = '$ocupaciones_id', sanguineos_tipos_id = NULLIF('$sanguineos_tipos_id', 'NULL'), padecimiento_actual = '$padecimiento_actual', exploracion_fisica = '$exploracion_fisica', t_a = '$t_a', f_r = '$f_r', f_c = '$f_c', menarca = '$menarca', gesta = '$gesta', fum = '$fum', ivsa = '$ivsa', para = '$para', abortos = NULLIF('$abortos', 'NULL'), cesareas = NULLIF('$cesareas', 'NULL'), poses_sexuales = '$poses_sexuales', otras_actividades = '$otras_actividades', observaciones = '$observaciones', antecedentes_familiares = '$antecedentes_familiares', antecedentes_patologicos = '$antecedentes_patologicos', antecedentes_quirurgicos = '$antecedentes_quirurgicos', antecedentes_ginecoobstetricos = '$antecedentes_ginecoobstetricos' WHERE pacientes_id = '$pacientes_id'",
            'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
              ' Por favor, vuelve a intentarlo.'
          )
        );
        exit;
      }

      // Si hubo cambios o insercion, agregar registro al LOG.
      if ($statement->rowCount() > 0) {
        $pacientes_id = $pacientes_id ? $pacientes_id : $pdo->lastInsertId();
      }

      // Inserción de las alergias.
      foreach ($alergias_ids as $index => $alergias_id) {
        // Buscar si ya esta insertada la alergia.
        $query = $pdo->prepare("SELECT id
          FROM pacientes_alergias
          WHERE pacientes_id = :pacientes_id
            AND alergias_id = :alergias_id");
        $ok = $query->execute(array(
            'pacientes_id' => $pacientes_id,
            'alergias_id' => $alergias_id
          )
        );

        if (!$ok) {
          $pdo->rollBack();
          echo json_encode(array(
              'status' => 'ERROR OBTENER ALERGIA!',
              'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                ' Por favor, vuelve a intentarlo.'
            )
          );
          exit;
        }

        // Obtener la alergia.
        $alergia = $query->fetch(PDO::FETCH_ASSOC);

        $alergia_values = array(
          'pacientes_id' => $pacientes_id,
          'alergias_id' => $alergias_id,
          'tiene' => $alergias_tiene[$index] === 'si' ? 1 : 0,
          'observaciones' => $alergias_observaciones[$index]
        );

        if ($alergia) {
          $statement = $pdo->prepare("UPDATE pacientes_alergias
            SET pacientes_id = :pacientes_id,
              alergias_id = :alergias_id,
              tiene = :tiene,
              observaciones = :observaciones
            WHERE id = :id");
          $alergia_values['id'] = $alergia['id'];
        } else {
          $statement = $pdo->prepare("INSERT INTO pacientes_alergias
            (pacientes_id, alergias_id, tiene, observaciones)
            VALUES (:pacientes_id, :alergias_id, :tiene, :observaciones)");
        }

        $ok = $statement->execute($alergia_values);

        if (!$ok) {
          $pdo->rollBack();
          echo json_encode(array(
              'status' => 'ERROR GUARDAR ALERGIAS',
              'query' => "INSERT INTO pacientes_alergias (pacientes_id, alergias_id, tiene, observaciones) VALUES ('$pacientes_id', '$alergias_id', '$alergias_tiene[$index]', '$alergias_observaciones[$index]')",
              'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                ' Por favor, vuelve a intentarlo.'
            )
          );
          exit;
        }
      }

      // Inserción de las adicciones.
      foreach ($adicciones_ids as $index => $adicciones_id) {
        // if ( (int) $adicciones_id <= 0 ) continue;
        // Buscar si ya esta insertada la adiccion.
        $query = $pdo->prepare("SELECT id
          FROM pacientes_adicciones
          WHERE pacientes_id = :pacientes_id
            AND adicciones_id = :adicciones_id");
        $ok = $query->execute(array(
            'pacientes_id' => $pacientes_id,
            'adicciones_id' => $adicciones_id
          )
        );

        if (!$ok) {
          $pdo->rollBack();
          echo json_encode(array(
              'status' => 'ERROR OBTENER ADICCION!',
              'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                ' Por favor, vuelve a intentarlo.'
            )
          );
          exit;
        }

        // Obtener la adiccion.
        $adiccion = $query->fetch(PDO::FETCH_ASSOC);

        $adiccion_values = array(
          'pacientes_id' => $pacientes_id,
          'adicciones_id' => $adicciones_id,
          'tiene' => $adicciones_tiene[$index] === 'si' ? 1 : 0,
          'observaciones' => $adicciones_observaciones[$index]
        );

        if ($adiccion) {
          $statement = $pdo->prepare("UPDATE pacientes_adicciones
            SET pacientes_id = :pacientes_id,
              adicciones_id = :adicciones_id,
              tiene = :tiene,
              observaciones = :observaciones
            WHERE id = :id");
          $adiccion_values['id'] = $adiccion['id'];
        } else {
          $statement = $pdo->prepare("INSERT INTO pacientes_adicciones
            (pacientes_id, adicciones_id, tiene, observaciones)
            VALUES (:pacientes_id, :adicciones_id, :tiene, :observaciones)");
        }

        $ok = $statement->execute($adiccion_values);

        if (!$ok) {
          $pdo->rollBack();
          echo json_encode(array(
              'status' => 'ERROR GUARDAR ADICCIONES',
              'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                ' Por favor, vuelve a intentarlo.'
            )
          );
          exit;
        }
      }

        // Inserción de las vacunas.
      foreach ($vacunas_ids as $index => $vacunas_id) {
        // if ( (int) $vacunas_id <= 0 ) continue;

        // Buscar si ya esta insertada la vacuna.
        $query = $pdo->prepare("SELECT id
          FROM pacientes_vacunas
          WHERE pacientes_id = :pacientes_id
            AND vacunas_id = :vacunas_id");
        $ok = $query->execute(array(
            'pacientes_id' => $pacientes_id,
            'vacunas_id' => $vacunas_id
          )
        );

        if (!$ok) {
          $pdo->rollBack();
          echo json_encode(array(
              'status' => 'ERROR OBTENER VACUNA!',
              'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                ' Por favor, vuelve a intentarlo.'
            )
          );
          exit;
        }

        // Obtener la vacuna.
        $vacuna = $query->fetch(PDO::FETCH_ASSOC);

        $vacuna_values = array(
          'pacientes_id' => $pacientes_id,
          'vacunas_id' => $vacunas_id,
          'tiene' => $vacunas_tiene[$index] === 'si' ? 1 : 0,
          'observaciones' => $vacunas_observaciones[$index]
        );

        if ($vacuna) {
          $statement = $pdo->prepare("UPDATE pacientes_vacunas
            SET pacientes_id = :pacientes_id,
              vacunas_id = :vacunas_id,
              tiene = :tiene,
              observaciones = :observaciones
            WHERE id = :id");
          $vacuna_values['id'] = $vacuna['id'];
        } else {
          $statement = $pdo->prepare("INSERT INTO pacientes_vacunas
            (pacientes_id, vacunas_id, tiene, observaciones)
            VALUES (:pacientes_id, :vacunas_id, :tiene, :observaciones)");
        }

        $ok = $statement->execute($vacuna_values);

        if (!$ok) {
          $pdo->rollBack();
          echo json_encode(array(
              'status' => 'ERROR GUARDAR VACUNAS',
              'msg' => 'Lo sentimos, algo sucedió al intentar guardar la información.'.
                ' Por favor, vuelve a intentarlo.'
            )
          );
          exit;
        }
      }

      $pdo->commit();
      echo json_encode(array(
          'status' => 'OK',
          'msg' => 'El paciente fue guardado correctamente.'
        )
      );
    break;
  }
?>